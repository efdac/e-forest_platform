/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/
package fr.ifn.eforest.harmonization.business;

import fr.ifn.eforest.harmonization.AbstractEFDACTest;
import fr.ifn.eforest.harmonization.business.HarmonizationService;
import fr.ifn.eforest.harmonization.database.harmonizeddata.HarmonisationProcessDAO;
import fr.ifn.eforest.harmonization.database.harmonizeddata.HarmonizedDataDAO;

//
// Note : In order to use this Test Class correctly under Eclipse, you need to change the working directory to
// ${workspace_loc:EFDAC - Framework Contract for forest data and services/service_integration}
//

/**
 * Test cases for the Harmonization service.
 */
public class HarmonizationServiceTest extends AbstractEFDACTest {

	// The services
	private HarmonizationService harmonizationService = new HarmonizationService();

	// The DAOs
	private HarmonizedDataDAO harmonizedDataDAO = new HarmonizedDataDAO();
	private HarmonisationProcessDAO harmonisationProcessDAO = new HarmonisationProcessDAO();

	/**
	 * Constructor
	 * 
	 * @param name
	 */
	public HarmonizationServiceTest(String name) {
		super(name);
	}

	/**
	 * Test the data submission function.
	 */
	public void testHarmonizeData() throws Exception {

		// Parameters
		String countryCode = "66";

		String requestId = "WP3_REQUEST";

		Integer processId = null;

		try {

			//
			// Launch the harmonization process
			//
			processId = harmonizationService.harmonizeData(requestId, countryCode);

			// Check that we have some data in the harmonized tables
			assertEquals(7, harmonizedDataDAO.countData("harmonized_location", countryCode, requestId));
			assertEquals(1, harmonizedDataDAO.countData("harmonized_plot_data", countryCode, requestId));
			assertEquals(3, harmonizedDataDAO.countData("harmonized_species_data", countryCode, requestId));

		} catch (Exception e) {
			logger.error(e);
			assertTrue(false);
		} finally {

			// Delete the data from the harmonized tables
			logger.debug("");
			logger.debug("Removing test data");
			logger.debug("");

			// Remove the inserted data
			harmonizedDataDAO.deleteHarmonizedData("harmonized_species_data", countryCode, requestId);
			harmonizedDataDAO.deleteHarmonizedData("harmonized_plot_data", countryCode, requestId);
			harmonizedDataDAO.deleteHarmonizedData("harmonized_location", countryCode, requestId);

			// Delete the harmonization log
			if (processId != null) {
				harmonisationProcessDAO.deleteHarmonizationProcess(processId);
			}

		}

	}
}
