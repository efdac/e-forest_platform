/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/
CREATE SCHEMA raw_data;

SET SEARCH_PATH = raw_data, public;



/*==============================================================*/
/* Sequence : SUBMISSION_ID                                     */
/*==============================================================*/
CREATE SEQUENCE submission_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
  
/*==============================================================*/
/* Table : SUBMISSION                                           */
/*==============================================================*/
create table SUBMISSION (
SUBMISSION_ID        INT4                 not null default nextval('submission_id_seq'),
TYPE          		 VARCHAR(36)          not null,
STEP		 		 VARCHAR(36)          null,
STATUS    			 VARCHAR(36)          null,
COUNTRY_CODE         VARCHAR(36)          not null,
_CREATIONDT          DATE                 null DEFAULT current_timestamp,
_VALIDATIONDT        DATE                 null DEFAULT current_timestamp,
constraint PK_SUBMISSION primary key (SUBMISSION_ID)
);

COMMENT ON COLUMN SUBMISSION.SUBMISSION_ID IS 'The identifier of the submission';
COMMENT ON COLUMN SUBMISSION.TYPE IS 'The type of the submission (LOCATION, STRATA or DATA)';
COMMENT ON COLUMN SUBMISSION.STEP IS 'The step of the submission (INIT, INSERTED, CHECKED, VALIDATED or CANCELLED)';
COMMENT ON COLUMN SUBMISSION.STATUS IS 'The status of the submission (OK, RUNNING, WARNING, ERROR or CRASH)';
COMMENT ON COLUMN SUBMISSION.COUNTRY_CODE IS 'The country code';
COMMENT ON COLUMN SUBMISSION._CREATIONDT IS 'The date of creation of the submission';
COMMENT ON COLUMN SUBMISSION._VALIDATIONDT IS 'The date of validation of the submission';



/*==============================================================*/
/* Table : DATA_SUBMISSION                                      */
/*==============================================================*/
create table DATA_SUBMISSION (
SUBMISSION_ID        INT4                 not null,
REQUEST_ID           VARCHAR(36)          not null,
COMMENT              VARCHAR(255)         null,
USER_LOGIN           VARCHAR(50)          null,
constraint PK_DATA_SUBMISSION primary key (SUBMISSION_ID, REQUEST_ID)
);

COMMENT ON COLUMN DATA_SUBMISSION.SUBMISSION_ID IS 'The identifier of the submission';
COMMENT ON COLUMN DATA_SUBMISSION.REQUEST_ID IS 'The identifier of the dataset';
COMMENT ON COLUMN DATA_SUBMISSION.COMMENT IS 'A comment';
COMMENT ON COLUMN DATA_SUBMISSION.USER_LOGIN IS 'The user who has done the submission';

/*==============================================================*/
/* Table : STRATA_SUBMISSION                               */
/*==============================================================*/
create table STRATA_SUBMISSION (
SUBMISSION_ID        INT4                 not null,
constraint PK_STRATA_SUBMISSION primary key (SUBMISSION_ID)
);

COMMENT ON COLUMN STRATA_SUBMISSION.SUBMISSION_ID IS 'The identifier of the submission';

/*==============================================================*/
/* Table : LOCATION_SUBMISSION                                  */
/*==============================================================*/
create table LOCATION_SUBMISSION (
SUBMISSION_ID        INT4                 not null,
constraint PK_LOCATION_SUBMISSION primary key (SUBMISSION_ID)
);

COMMENT ON COLUMN LOCATION_SUBMISSION.SUBMISSION_ID IS 'The identifier of the submission';

/*==============================================================*/
/* Table : SUBMISSION_FILE                                      */
/*==============================================================*/
create table SUBMISSION_FILE (
SUBMISSION_ID        INT4                 not null,
FILE_TYPE            VARCHAR(36)          not null,
FILE_NAME            VARCHAR(4000)         not null,
NB_LINE              INT4                 null,
constraint PK_SUBMISSION_FILE primary key (SUBMISSION_ID, FILE_TYPE)
);

COMMENT ON COLUMN SUBMISSION_FILE.SUBMISSION_ID IS 'The identifier of the submission';
COMMENT ON COLUMN SUBMISSION_FILE.FILE_TYPE IS 'The type of the file (LOCATION, PLOT, SPECIES, ...)';
COMMENT ON COLUMN SUBMISSION_FILE.FILE_NAME IS 'The name of the file';
COMMENT ON COLUMN SUBMISSION_FILE.NB_LINE IS 'The number of lines of data';


/*==============================================================*/
/* Table : LOCATION                                             */
/*==============================================================*/
create table LOCATION (
SUBMISSION_ID        INT4                 not null,
PLOT_CODE            VARCHAR(36)          not null,
CLUSTER_CODE         VARCHAR(36)          not null,
COUNTRY_CODE         VARCHAR(36)          not null,
LAT                  FLOAT8               null,
LONG                 FLOAT8               null,
IS_PLOT_COORDINATES_DEGRADED   CHAR(1)	  not null,
CELL_ID_NUTS0		 VARCHAR(36)          null,
CELL_ID_100          VARCHAR(36)          null,
CELL_ID_50           VARCHAR(36)          null,
CELL_ID				 VARCHAR(36)          null,
COMMENT              VARCHAR(255)         null,
LINE_NUMBER			 INTEGER			  null,
constraint PK_LOCATION primary key (SUBMISSION_ID, PLOT_CODE),
UNIQUE (COUNTRY_CODE, PLOT_CODE)
);

-- Ajout de la colonne point PostGIS
SELECT AddGeometryColumn('raw_data','location','the_geom',4326,'POINT',2);
		
-- Spatial Index on the_geom 
CREATE INDEX IX_LOCATION_SPATIAL_INDEX ON raw_data.location USING GIST
            ( the_geom GIST_GEOMETRY_OPS );
            
COMMENT ON COLUMN LOCATION.SUBMISSION_ID IS 'The identifier of the submission';
COMMENT ON COLUMN LOCATION.PLOT_CODE IS 'The identifier of the plot for the country';
COMMENT ON COLUMN LOCATION.CLUSTER_CODE IS 'The identifier of the cluster';
COMMENT ON COLUMN LOCATION.COUNTRY_CODE IS 'The country code';
COMMENT ON COLUMN LOCATION.LAT IS 'The latitude in DMS coordinates';
COMMENT ON COLUMN LOCATION.LONG IS 'The longitude in DMS coordinates';
COMMENT ON COLUMN LOCATION.IS_PLOT_COORDINATES_DEGRADED IS 'Indicate if the position of the plot has already been blurred';
COMMENT ON COLUMN LOCATION.CELL_ID_NUTS0 IS 'The identifier of NUTS0 cell containing this plot';
COMMENT ON COLUMN LOCATION.CELL_ID_100 IS 'The identifier of INSPIRE 100x100 cell containing this plot';
COMMENT ON COLUMN LOCATION.CELL_ID_50 IS 'The identifier of INSPIRE 50x50 cell containing this plot';
COMMENT ON COLUMN LOCATION.CELL_ID IS 'The identifier of INSPIRE 1x1 cell containing this plot';
COMMENT ON COLUMN LOCATION.COMMENT IS 'A comment';
COMMENT ON COLUMN LOCATION.LINE_NUMBER IS 'The line number where the data can be found in the original CSV file';
COMMENT ON COLUMN LOCATION.THE_GEOM IS 'The geometry corresponding to this plot location';
            
/*========================================================================*/
/*	Add a trigger to fill the the_geom column of the location table       */
/*========================================================================*/
CREATE OR REPLACE FUNCTION raw_data.geomfromcoordinate() RETURNS "trigger" AS
$BODY$
BEGIN
    NEW.the_geom = public.GeometryFromText('POINT(' || NEW.LONG || ' ' || NEW.LAT || ')', 4326);
    RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
  
CREATE TRIGGER geom_trigger
  BEFORE INSERT OR UPDATE
  ON raw_data.LOCATION
  FOR EACH ROW
  EXECUTE PROCEDURE raw_data.geomfromcoordinate();
  


/*========================================================================*/
/*	Add a function to fill the CELL_ID columns of the location table       */
/*========================================================================*/
CREATE OR REPLACE FUNCTION raw_data.calculatecellid(submissionId integer) returns void AS
$$	
	-- NUTS 0 (country code)
	UPDATE location
	SET    CELL_ID_NUTS0 = foo.CELL_ID
	FROM (
	       SELECT submission_id, plot_code, group_mode.dst_code as CELL_ID
	       FROM   location
	       LEFT JOIN metadata.group_mode on (group_mode.dst_unit = 'NUTS_COUNTRY_CODE' AND group_mode.src_code = location.country_code)
	       WHERE location.submission_id = $1	       
	) foo
	WHERE location.submission_id = foo.submission_id
	AND location.plot_code = foo.plot_code;
	
	
	-- 100 km x 100 km
    UPDATE location
	SET    CELL_ID_100 = foo.CELL_ID
	FROM (
		SELECT submission_id, plot_code, cell_id
		FROM location
		JOIN mapping.grid_eu25_100k ON (ST_Transform(location.the_geom,3035) && grid_eu25_100k.the_geom AND ST_Intersects(ST_Transform(location.the_geom,3035), grid_eu25_100k.the_geom))
		WHERE location.submission_id = $1	     
	)foo
	WHERE location.submission_id = foo.submission_id
	AND location.plot_code = foo.plot_code;
	
	-- 50 km x 50 km
	UPDATE location
	SET    CELL_ID_50 = foo.CELL_ID
	FROM (
		SELECT submission_id, plot_code, cell_id
		FROM location
		JOIN mapping.grid_eu25_50k ON (ST_Transform(location.the_geom,3035) && grid_eu25_50k.the_geom AND ST_Intersects(ST_Transform(location.the_geom,3035), grid_eu25_50k.the_geom))
		WHERE location.submission_id = $1	     
	)foo
	WHERE location.submission_id = foo.submission_id
	AND location.plot_code = foo.plot_code;
	
	
$$
LANGUAGE SQL;
  


/*==============================================================*/
/* Table : PLOT_DATA                                            */
/*==============================================================*/
create table PLOT_DATA (
SUBMISSION_ID        INT4                 not null,
STRATUM_CODE         VARCHAR(36)          null,
COUNTRY_CODE         VARCHAR(36)          not null,
PLOT_CODE            VARCHAR(36)          not null,
CYCLE	             VARCHAR(36)          not null,
REF_YEAR_BEGIN       INTEGER	          not null,
REF_YEAR_END	     INTEGER	          not null,
INV_DATE             DATE                 null,
STATISTICAL_WEIGHT   FLOAT8               null,
DOMAIN_FOREST        VARCHAR(36)			  null,
DOMAIN_BASAL_AREA    VARCHAR(36)			  null,
IS_FOREST_PLOT		 CHAR(1)	          null,
IS_PARTITIONNING_PLOT  CHAR(1)	          null,
COMMENT              VARCHAR(1000)        null,
LINE_NUMBER			 INTEGER			  null,
constraint PK_PLOT_DATA primary key (SUBMISSION_ID, COUNTRY_CODE, PLOT_CODE, CYCLE)
);

COMMENT ON COLUMN PLOT_DATA.SUBMISSION_ID IS 'The identifier of the submission';
COMMENT ON COLUMN PLOT_DATA.STRATUM_CODE IS 'The identifier of the stratum';
COMMENT ON COLUMN PLOT_DATA.COUNTRY_CODE IS 'The country code';
COMMENT ON COLUMN PLOT_DATA.PLOT_CODE IS 'The plot code';
COMMENT ON COLUMN PLOT_DATA.CYCLE IS 'The cycle of inventory';
COMMENT ON COLUMN PLOT_DATA.REF_YEAR_BEGIN IS 'The year of beginning of inventory';
COMMENT ON COLUMN PLOT_DATA.REF_YEAR_END IS 'The year of end of inventory';
COMMENT ON COLUMN PLOT_DATA.INV_DATE IS 'The date of inventory of this plot';
COMMENT ON COLUMN PLOT_DATA.STATISTICAL_WEIGHT IS 'The statistical weight of the plot';
COMMENT ON COLUMN PLOT_DATA.DOMAIN_FOREST IS 'Indicate if the plot is in the forest domain (0, 1 or -1)';
COMMENT ON COLUMN PLOT_DATA.DOMAIN_BASAL_AREA IS 'Indicate if the plot is in the basal area domain (0, 1 or -1)';
COMMENT ON COLUMN PLOT_DATA.IS_PARTITIONNING_PLOT IS 'Indicate if the plot was a partitionning plot (0 or 1)';
COMMENT ON COLUMN PLOT_DATA.COMMENT IS 'A comment';
COMMENT ON COLUMN PLOT_DATA.LINE_NUMBER IS 'The line number where the data can be found in the original CSV file';


/*==============================================================*/
/* Table : SPECIES_DATA                                         */
/*==============================================================*/
create table SPECIES_DATA (
SUBMISSION_ID        INT4                 not null,
COUNTRY_CODE         VARCHAR(36)          not null,
PLOT_CODE            VARCHAR(36)          not null,
CYCLE	             VARCHAR(36)          not null,
SPECIES_CODE         VARCHAR(36)          not null,
DBH_CLASS            VARCHAR(36)          null,
NATIONAL_SPECIES_CODE         VARCHAR(36)          null,
BASAL_AREA			 FLOAT8	              null,
COMMENT              VARCHAR(255)         null,
LINE_NUMBER			 INTEGER			  null,
constraint PK_SPECIES_DATA primary key (SUBMISSION_ID, COUNTRY_CODE, PLOT_CODE, CYCLE, DBH_CLASS, SPECIES_CODE)
);

COMMENT ON COLUMN SPECIES_DATA.SUBMISSION_ID IS 'The identifier of the submission';
COMMENT ON COLUMN SPECIES_DATA.COUNTRY_CODE IS 'The country code';
COMMENT ON COLUMN SPECIES_DATA.PLOT_CODE IS 'The plot code';
COMMENT ON COLUMN SPECIES_DATA.CYCLE IS 'The cycle of inventory';
COMMENT ON COLUMN SPECIES_DATA.SPECIES_CODE IS 'The eforest species code';
COMMENT ON COLUMN SPECIES_DATA.DBH_CLASS IS 'The class of diameter (1 or 2)';
COMMENT ON COLUMN SPECIES_DATA.NATIONAL_SPECIES_CODE IS 'The original species code (for the country)';
COMMENT ON COLUMN SPECIES_DATA.BASAL_AREA IS 'The basal area';
COMMENT ON COLUMN SPECIES_DATA.COMMENT IS 'A comment';
COMMENT ON COLUMN SPECIES_DATA.LINE_NUMBER IS 'The line number where the data can be found in the original CSV file';

/*==============================================================*/
/* Table : STRATA                                          */
/*==============================================================*/
create table STRATA (
SUBMISSION_ID        INT4                 not null,
COUNTRY_CODE         VARCHAR(36)          not null,
STRATUM_CODE         VARCHAR(36)          not null,
TOTAL_STRATUM_AREA   FLOAT8               not null,
COMMENT              VARCHAR(255)         null,
constraint PK_STRATA primary key (COUNTRY_CODE, STRATUM_CODE)
);

COMMENT ON COLUMN STRATA.SUBMISSION_ID IS 'The identifier of the submission';
COMMENT ON COLUMN STRATA.COUNTRY_CODE IS 'The country code';
COMMENT ON COLUMN STRATA.STRATUM_CODE IS 'The stratum code';
COMMENT ON COLUMN STRATA.TOTAL_STRATUM_AREA IS 'The area of the stratum';
COMMENT ON COLUMN STRATA.COMMENT IS 'A comment';


/*==============================================================*/
/* Table : CHECK_ERROR                                          */
/*==============================================================*/
create table CHECK_ERROR (
CHECK_ERROR_ID       serial               not null,
CHECK_ID             INT4                 not null,
SUBMISSION_ID        INT4                 not null,
LINE_NUMBER          INT4                 not null,
SRC_FORMAT           VARCHAR(36)          null,
SRC_DATA             VARCHAR(36)          null,
COUNTRY_CODE         VARCHAR(36)          null,
PLOT_CODE            VARCHAR(36)          null,
FOUND_VALUE          VARCHAR(255)         null,
EXPECTED_VALUE       VARCHAR(255)         null,
ERROR_MESSAGE        VARCHAR(4000)        null,
_CREATIONDT          DATE                 null  DEFAULT current_timestamp,
constraint PK_CHECK_ERROR primary key (CHECK_ID, SUBMISSION_ID, CHECK_ERROR_ID)
);

COMMENT ON COLUMN CHECK_ERROR.CHECK_ERROR_ID IS 'The identifier of the check error';
COMMENT ON COLUMN CHECK_ERROR.CHECK_ID IS 'The identifier of the check';
COMMENT ON COLUMN CHECK_ERROR.SUBMISSION_ID IS 'The identifier of the submission';
COMMENT ON COLUMN CHECK_ERROR.LINE_NUMBER IS 'The line number where the data can be found in the original CSV file';
COMMENT ON COLUMN CHECK_ERROR.SRC_FORMAT IS 'The logical name of the source format (file type)';
COMMENT ON COLUMN CHECK_ERROR.SRC_DATA IS 'The logical name of the source data (file column)';
COMMENT ON COLUMN CHECK_ERROR.COUNTRY_CODE IS 'The country code';
COMMENT ON COLUMN CHECK_ERROR.PLOT_CODE IS 'The plot code';
COMMENT ON COLUMN CHECK_ERROR.FOUND_VALUE IS 'The value that was given';
COMMENT ON COLUMN CHECK_ERROR.EXPECTED_VALUE IS 'The value that was expected';
COMMENT ON COLUMN CHECK_ERROR.ERROR_MESSAGE IS 'The error message';
COMMENT ON COLUMN CHECK_ERROR._CREATIONDT IS 'The identifier of the check error';


/*========================================================================*/
/*	Add a function to fill the CELL_ID columns of the location table       */
/*========================================================================*/
CREATE OR REPLACE FUNCTION raw_data.calculatecellid(submissionId integer) returns void AS
$$	
	-- NUTS 0 (country code)
	UPDATE 	location
	SET    	CELL_ID_NUTS0 = group_mode.dst_code
	FROM 	metadata.group_mode 
	WHERE   group_mode.dst_unit = 'NUTS_COUNTRY_CODE' 
	AND     group_mode.src_code = location.country_code
	AND 	location.submission_id = $1;
		
	-- 100 km x 100 km
	UPDATE 	location
	SET    	CELL_ID_100 = grid_eu25_100k.CELL_ID
	FROM 	mapping.grid_eu25_100k
	WHERE   ST_Transform(location.the_geom,3035) && grid_eu25_100k.the_geom 
	AND 	ST_Intersects(ST_Transform(location.the_geom,3035), grid_eu25_100k.the_geom)
	AND 	location.submission_id = $1;
	
	-- 50 km x 50 km
	UPDATE 	location
	SET    	CELL_ID_50 = grid_eu25_50k.CELL_ID
	FROM 	mapping.grid_eu25_50k
	WHERE   ST_Transform(location.the_geom,3035) && grid_eu25_50k.the_geom 
	AND 	ST_Intersects(ST_Transform(location.the_geom,3035), grid_eu25_50k.the_geom)
	AND 	location.submission_id = $1;
	
	-- 1 km x 1 km
	UPDATE 	location
	SET    	CELL_ID = grid_eu25_1k.CELL_ID
	FROM 	mapping.grid_eu25_1k
	WHERE   ST_Transform(location.the_geom,3035) && grid_eu25_1k.the_geom 
	AND 	ST_Intersects(ST_Transform(location.the_geom,3035), grid_eu25_1k.the_geom)
	AND 	location.submission_id = $1;
	
	
$$
LANGUAGE SQL;
  
   
/*==============================================================*/
/* Table : CLUSTER                                              */
/*==============================================================*/
CREATE TABLE CLUSTER (
REQUEST_ID 		character varying(36) 	NOT NULL,
COUNTRY_CODE 	character varying(36) 	NOT NULL,
STRATUM_CODE 	character varying(36)	NOT NULL,
CLUSTER_CODE 	character varying(36)	NOT NULL,
STATISTICAL_WEIGHT double precision     NOT NULL,
NO_PLOT_IN_CLUSTER bigint,
constraint PK_CLUSTER primary key (REQUEST_ID, COUNTRY_CODE, STRATUM_CODE, CLUSTER_CODE, STATISTICAL_WEIGHT)
);

COMMENT ON COLUMN CLUSTER.REQUEST_ID IS 'The dataset identifier';
COMMENT ON COLUMN CLUSTER.COUNTRY_CODE IS 'The country code';
COMMENT ON COLUMN CLUSTER.STRATUM_CODE IS 'The stratum code';
COMMENT ON COLUMN CLUSTER.CLUSTER_CODE IS 'The cluster code';
COMMENT ON COLUMN CLUSTER.STATISTICAL_WEIGHT IS 'The value of the statistical weight inside the country';
COMMENT ON COLUMN CLUSTER.NO_PLOT_IN_CLUSTER IS 'The number of plots inside the cluster';

/*==============================================================*/
/* Table : FOREST_MAP_VALIDATION                                */
/*==============================================================*/
CREATE TABLE FOREST_MAP_VALIDATION (
SUBMISSION_ID        	INT4                NOT null,
COUNTRY_CODE         	VARCHAR(36)         NOT null,
PLOT_CODE 		 		VARCHAR(36)  		NOT NULL,
CYCLE			 		VARCHAR(36)  		NOT NULL,
INV_DATE			 	DATE		 		NOT NULL,
NUTS_CODE			 	VARCHAR(36) 		NULL,
DOMAIN_FOREST		 	VARCHAR(36) 		NULL,
IS_FOREST_PLOT		 	VARCHAR(2) 	 		NULL,
FOREST_TYPE				VARCHAR(36) 		NULL,
CROWN_COVER				VARCHAR(36) 		NULL,
YOUNG_STAND				VARCHAR(36) 		NULL,
BOUNDARY				VARCHAR(36) 		NULL,
ELEVATION				FLOAT		 		NULL,
SLOPE					FLOAT		 		NULL,
ASPECT					VARCHAR(36)	 		NULL,
CENTER					VARCHAR(36) 	 		NULL,
A11						VARCHAR(36) 	 		NULL,
A12						VARCHAR(36) 	 		NULL,
A13						VARCHAR(36) 	 		NULL,
A14						VARCHAR(36) 	 		NULL,
A15						VARCHAR(36) 	 		NULL,
A21						VARCHAR(36) 	 		NULL,
A22						VARCHAR(36) 	 		NULL,
A23						VARCHAR(36) 	 		NULL,
A24						VARCHAR(36) 	 		NULL,
A25						VARCHAR(36) 	 		NULL,
A31						VARCHAR(36) 	 		NULL,
A32						VARCHAR(36) 	 		NULL,
A34						VARCHAR(36) 	 		NULL,
A35						VARCHAR(36) 	 		NULL,
A41						VARCHAR(36) 	 		NULL,
A42						VARCHAR(36) 	 		NULL,
A43						VARCHAR(36) 	 		NULL,
A44						VARCHAR(36) 	 		NULL,
A45						VARCHAR(36) 	 		NULL,
A51						VARCHAR(36) 	 		NULL,
A52						VARCHAR(36) 	 		NULL,
A53						VARCHAR(36) 	 		NULL,
A54						VARCHAR(36) 	 		NULL,
A55						VARCHAR(36) 	 		NULL,
COMMENT					TEXT		 		NULL,
constraint PK_FOREST_MAP_VALIDATION primary key (SUBMISSION_ID, COUNTRY_CODE, PLOT_CODE, CYCLE)
);

COMMENT ON COLUMN FOREST_MAP_VALIDATION.SUBMISSION_ID IS 'The identifier of the submission';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.COUNTRY_CODE IS 'The country code';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.PLOT_CODE IS 'The plot code';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.CYCLE IS 'The cycle of inventory';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.INV_DATE IS 'The date when the field plot has been assessed';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.NUTS_CODE IS 'NUTS 3 code';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.DOMAIN_FOREST IS 'Indicates whether the plot belongs to the domain in which the forest area (is_forest_plot) is defined';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.IS_FOREST_PLOT IS 'Tells if the plot corresponds to a forest plot or not';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.FOREST_TYPE IS 'Indicates whether the plot is a coniferous, broadleaved, or mixed plot';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.CROWN_COVER IS 'Indicates whether the plot belongs to a closed or open forest ';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.YOUNG_STAND IS 'Indicates whether the forest in the plot is classified in regeneration/young stand';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.BOUNDARY IS 'Indicates whether the plot is crossed by a boundary';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.ELEVATION IS 'Elevation of the NFI plot in meters';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.SLOPE IS 'Slope of the NFI plot in percent';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.ASPECT IS 'Aspect of the NFI plot in cardinal points';
COMMENT ON COLUMN FOREST_MAP_VALIDATION.CENTER IS 'The extracted pixel value for the center cell';



ALTER TABLE raw_data.forest_map_validation
ADD CONSTRAINT fk_forest_map_validation FOREIGN KEY (plot_code, country_code)
      REFERENCES raw_data."location" (plot_code, country_code) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;
      
      
      
      
      
/*==============================================================*/
/* Table : STRATA_INFO                                          */
/*==============================================================*/   
-- DROP TABLE raw_data.plot_surface;
CREATE TABLE raw_data.strata_info
(
  country_code character varying(36), -- The country code
  stratum_code character varying(36), -- The stratum code
  stratum_surface_area double precision,
  nominal_cluster_size integer,
  total_plots integer,
  weighted_sum_nb_plots double precision,
  be double precision
)
WITH (
  OIDS=FALSE
);

ALTER TABLE raw_data.strata_info
ADD CONSTRAINT pk_strata_info primary key (country_code, stratum_code);   
      
      
      


/* Non créé sinon on ne peut plus mettre à jour les checks
alter table CHECK_ERROR
   add constraint FK_CHECK_ER_ASSOCIATI_CHECKS foreign key (CHECK_ID)
      references metadata.CHECKS (CHECK_ID)
      on delete restrict on update restrict;
*/

alter table LOCATION
   add constraint FK_LOCATION_ASSOCIATI_LOCATION foreign key (SUBMISSION_ID)
      references LOCATION_SUBMISSION (SUBMISSION_ID)
      on delete restrict on update restrict;
      
alter table PLOT_DATA
   add constraint FK_PLOT_DATA_ASSOCIATE_LOCATION foreign key (PLOT_CODE, COUNTRY_CODE)
      references LOCATION (PLOT_CODE, COUNTRY_CODE)
      on delete restrict on update restrict;
     
alter table PLOT_DATA
   add constraint FK_PLOT_DATA_ASSOCIATE_STRATA foreign key (STRATUM_CODE, COUNTRY_CODE)
      references STRATA (STRATUM_CODE, COUNTRY_CODE)
      on delete restrict on update restrict;


      /*
alter table DATA_SUBMISSION
   add constraint FK_SUBMISSI_COMPOSED__JRC_REQU foreign key (REQUEST_ID)
      references metadata.JRC_REQUEST (REQUEST_ID)
      on delete restrict on update restrict;
      */

      
alter table SPECIES_DATA
   add constraint FK_SPECIES_ASSOCIATE_PLOT_DAT foreign key (SUBMISSION_ID, COUNTRY_CODE, PLOT_CODE, CYCLE)
      references PLOT_DATA (SUBMISSION_ID, COUNTRY_CODE, PLOT_CODE, CYCLE)
      on delete restrict on update restrict;     
      
      
-- Ajout d'indexes
CREATE INDEX LOCATION_PLOT_CODE_IDX ON location (country_code, plot_code);
CREATE INDEX PLOT_DATA_PLOT_CODE_IDX ON plot_data (country_code, plot_code);
CREATE INDEX SPECIES_DATA_PLOT_CODE_IDX ON species_data (country_code, plot_code);
CREATE INDEX standardized_cluster_IDX ON standardized_cluster (country_code, cluster_code);





GRANT ALL ON SCHEMA raw_data TO eforest;
GRANT ALL ON TABLE FOREST_MAP_VALIDATION TO eforest;


