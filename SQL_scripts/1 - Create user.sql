/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

-- Crée un user de connexion eforest / eforest
CREATE ROLE eforest LOGIN
  ENCRYPTED PASSWORD 'md50c9861e20d394c9b183b1c45c5c9098c'
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE;
  
ALTER ROLE eforest SET search_path=public, website, metadata, mapping, raw_data, harmonized_data, aggregated_data;


GRANT ALL ON SCHEMA website TO "eforest";
GRANT ALL ON SCHEMA metadata TO "eforest";
GRANT ALL ON SCHEMA raw_data TO "eforest";
GRANT ALL ON SCHEMA harmonized_data TO "eforest";
GRANT ALL ON SCHEMA aggregated_data TO "eforest";
