/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/
set search_path = metadata;

--
-- Remove integrity contraints
--

alter table FILE_FORMAT drop constraint FK_FILE_FOR_HERITAGE__FORMAT;
alter table FORM_FORMAT drop constraint FK_FORM_FOR_HERITAGE__FORMAT;
alter table TABLE_FORMAT drop constraint FK_TABLE_FO_HERITAGE__FORMAT;
alter table FILE_FIELD drop constraint FK_FILE_FIE_HERITAGE__FIELD;
alter table FORM_FIELD drop constraint FK_FORM_FIE_HERITAGE__FIELD;            
alter table TABLE_FIELD drop constraint FK_TABLE_FI_HERITAGE__FIELD;
alter table COMPLEMENTARY_FILE_FIELD drop constraint FK_COMPLEMENTARY_FILE_FIELD_HERITAGE_FIELD;
alter table COMPLEMENTARY_FORM_FIELD drop constraint FK_COMPLEMENTARY_FORM_FIELD_HERITAGE_FIELD;
alter table COMPLEMENTARY_TABLE_FIELD drop constraint FK_COMPLEMENTARY_TABLE_FIELD_HERITAGE_FIELD;
alter table DATASET_FIELDS drop constraint FK_DATASET_FIELDS_DATASET;
alter table DATASET_FIELDS drop constraint FK_DATASET_FIELDS_FIELD;
alter table DATASET_FILES drop constraint FK_DATASET_FILES_FORMAT;




COPY unit from '/var/tmp/efdac_init_sql/metadata/unit.csv' with delimiter ';' null '';
COPY data from '/var/tmp/efdac_init_sql/metadata/data.csv' with delimiter ';' null '';
--COPY range from '/var/tmp/efdac_init_sql/metadata/range.csv' with delimiter ';' null '';
COPY mode from '/var/tmp/efdac_init_sql/metadata/mode.csv' with delimiter ';' null '';

COPY form_format from '/var/tmp/efdac_init_sql/metadata/form_format.csv' with delimiter ';' null '';
COPY table_format from '/var/tmp/efdac_init_sql/metadata/table_format.csv' with delimiter ';' null '';
COPY file_format from '/var/tmp/efdac_init_sql/metadata/file_format.csv' with delimiter ';' null '';

-- Fill the parent table
INSERT INTO format (format, type)
SELECT format, 'FILE'
FROM   file_format
WHERE format not in (SELECT format FROM format);

INSERT INTO format (format, type)
SELECT format, 'TABLE'
FROM   table_format
WHERE format not in (SELECT format FROM format);

INSERT INTO format (format, type)
SELECT format, 'FORM'
FROM   form_format
WHERE format not in (SELECT format FROM format);

COPY form_field from '/var/tmp/efdac_init_sql/metadata/form_field.csv' with delimiter ';' null '';
COPY file_field from '/var/tmp/efdac_init_sql/metadata/file_field.csv' with delimiter ';' null '';
COPY table_field from '/var/tmp/efdac_init_sql/metadata/table_field.csv' with delimiter ';' null '';

-- TODO : Uncomment when we have some complementary data
--COPY complement_field from '/var/tmp/efdac_init_sql/complement_field.csv' with delimiter ';' null '';

-- Fill the parent table
INSERT INTO field (data, format, type)
SELECT data, format, 'FILE'
FROM   file_field
WHERE data||format not in (SELECT data||format FROM field);

INSERT INTO field (data, format, type)
SELECT data, format, 'TABLE'
FROM   table_field
WHERE data||format not in (SELECT data||format FROM field);

INSERT INTO field (data, format, type)
SELECT data, format, 'FORM'
FROM   form_field
WHERE data||format not in (SELECT data||format FROM field);


COPY field_mapping from '/var/tmp/efdac_init_sql/metadata/field_mapping.csv' with delimiter ';' null '';


COPY dataset from '/var/tmp/efdac_init_sql/metadata/dataset.csv' with delimiter ';' null '';
COPY dataset_fields from '/var/tmp/efdac_init_sql/metadata/dataset_fields.csv' with delimiter ';' null '';
COPY dataset_files from '/var/tmp/efdac_init_sql/metadata/dataset_files.csv' with delimiter ';' null '';

COPY table_tree from '/var/tmp/efdac_init_sql/metadata/table_tree.csv' with delimiter ';' null '';

--
-- Restore Integrity contraints
--

alter table FILE_FIELD
   add constraint FK_FILE_FIE_HERITAGE__FIELD foreign key (DATA, FORMAT)
      references metadata.FIELD (DATA, FORMAT)
      on delete restrict on update restrict;
      
alter table COMPLEMENTARY_FILE_FIELD
   add constraint FK_COMPLEMENTARY_FILE_FIELD_HERITAGE_FIELD foreign key (DATA, FORMAT)
      references FIELD (DATA, FORMAT)
      on delete restrict on update restrict;

alter table FILE_FORMAT
   add constraint FK_FILE_FOR_HERITAGE__FORMAT foreign key (FORMAT)
      references metadata.FORMAT (FORMAT)
      on delete restrict on update restrict;

alter table FORM_FIELD
   add constraint FK_FORM_FIE_HERITAGE__FIELD foreign key (DATA, FORMAT)
      references metadata.FIELD (DATA, FORMAT)
      on delete restrict on update restrict;

alter table COMPLEMENTARY_FORM_FIELD
   add constraint FK_COMPLEMENTARY_FORM_FIELD_HERITAGE_FIELD foreign key (DATA, FORMAT)
      references FIELD (DATA, FORMAT)
      on delete restrict on update restrict;      
      
alter table FORM_FORMAT
   add constraint FK_FORM_FOR_HERITAGE__FORMAT foreign key (FORMAT)
      references metadata.FORMAT (FORMAT)
      on delete restrict on update restrict;

alter table TABLE_FIELD
   add constraint FK_TABLE_FI_HERITAGE__FIELD foreign key (DATA, FORMAT)
      references FIELD (DATA, FORMAT)
      on delete restrict on update restrict;
      
alter table COMPLEMENTARY_TABLE_FIELD
   add constraint FK_COMPLEMENTARY_TABLE_FIELD_HERITAGE_FIELD foreign key (DATA, FORMAT)
      references FIELD (DATA, FORMAT)
      on delete restrict on update restrict;

alter table TABLE_FORMAT
   add constraint FK_TABLE_FO_HERITAGE__FORMAT foreign key (FORMAT)
      references FORMAT (FORMAT)
      on delete restrict on update restrict;

alter table DATASET_FIELDS
   add constraint FK_DATASET_FIELDS_DATASET foreign key (DATASET_ID)
      references DATASET (DATASET_ID)
      on delete restrict on update restrict;
      
alter table DATASET_FIELDS
   add constraint FK_DATASET_FIELDS_FIELD foreign key (FORMAT, DATA)
      references FIELD (FORMAT, DATA)
      on delete restrict on update restrict;
      
alter table DATASET_FILES
   add constraint FK_DATASET_FILES_FORMAT foreign key (FORMAT)
      references FILE_FORMAT (FORMAT)
      on delete restrict on update restrict;


--
-- Consistency checks
--

-- Units of type CODE should have an entry in the CODE table
SELECT UNIT, 'This unit of type CODE is not described in the MODE table'
FROM unit
WHERE TYPE = 'CODE'
AND unit not in (SELECT UNIT FROM MODE WHERE MODE.UNIT=UNIT)
UNION
-- Units of type RANGE should have an entry in the RANGE table
SELECT UNIT, 'This unit of type RANGE is not described in the RANGE table'
FROM unit
WHERE TYPE = 'RANGE'
AND unit not in (SELECT UNIT FROM RANGE WHERE RANGE.UNIT=UNIT)
UNION
-- File fields should have a FILE mapping
SELECT format||'_'||data, 'This file field has no FILE mapping defined'
FROM file_field
WHERE format||'_'||data NOT IN (
	SELECT (src_format||'_'||src_data )
	FROM field_mapping
	WHERE mapping_type = 'FILE'
	)
UNION
-- Form fields should have a FORM mapping
SELECT format||'_'||data, 'This form field has no FORM mapping defined'
FROM form_field
WHERE format||'_'||data NOT IN (
	SELECT (src_format||'_'||src_data )
	FROM field_mapping
	WHERE mapping_type = 'FORM'
	)
UNION
-- Raw data field should be mapped with harmonized fields
SELECT format||'_'||data, 'This raw_data table field is not mapped with an harmonized field'
FROM table_field
JOIN table_format using (format)
WHERE schema_code = 'RAW_DATA'
AND is_column_oriented = '0' -- We ignore complementary variables
AND data <> 'SUBMISSION_ID'
AND data <> 'LINE_NUMBER'
AND format||'_'||data NOT IN (
	SELECT (src_format||'_'||src_data )
	FROM field_mapping
	WHERE mapping_type = 'HARMONIZE'
	)
UNION
-- Raw data field should be mapped with harmonized fields
SELECT format||'_'||data, 'This harmonized_data table field is not used by a mapping'
FROM table_field
JOIN table_format using (format)
WHERE schema_code = 'HARMONIZED_DATA'
AND column_name <> 'REQUEST_ID'
AND format||'_'||data NOT IN (
	SELECT (dst_format||'_'||dst_data )
	FROM field_mapping
	WHERE mapping_type = 'HARMONIZE'
	)
UNION
-- the REQUEST_ID field is mandatory for harmonized data tables
SELECT format, 'This harmonized table format is missing the REQUEST_ID field'
FROM table_format 
WHERE schema_code = 'HARMONIZED_DATA'
AND NOT EXISTS (SELECT * FROM table_field WHERE table_format.format = table_field.format AND table_field.data='REQUEST_ID')
UNION
-- the SUBMISSION_ID field is mandatory for raw data tables
SELECT format, 'This raw table format is missing the SUBMISSION_ID field'
FROM table_format 
WHERE schema_code = 'RAW_DATA'
AND NOT EXISTS (SELECT * FROM table_field WHERE table_format.format = table_field.format AND table_field.data='SUBMISSION_ID')
UNION
-- the unit type in not consistent with the form field input type
SELECT form_field.format || '_' || form_field.data, 'The form field input type (' || input_type || ') is not consistent with the unit type (' || type || ')'
FROM form_field 
LEFT JOIN data using (data)
LEFT JOIN unit using (unit)
WHERE (input_type = 'NUMERIC' AND type NOT IN ('NUMERIC', 'RANGE','INTEGER'))
OR (input_type = 'DATE' AND type <> 'DATE')
OR (input_type = 'SELECT' AND type <> 'CODE')
OR (input_type = 'TEXT' AND type <> 'STRING')
OR (input_type = 'CHECKBOX' AND type <> 'BOOLEAN')

