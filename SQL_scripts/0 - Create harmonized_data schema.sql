/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/
CREATE SCHEMA harmonized_data;

SET SEARCH_PATH = harmonized_data, public;

 
  
/*==============================================================*/
/* Table : HARMONIZATION_PROCESS                                */
/*==============================================================*/
create table HARMONIZATION_PROCESS (
HARMONIZATION_PROCESS_ID         serial,
REQUEST_ID           			 VARCHAR(36)          not null,
COUNTRY_CODE	          		 VARCHAR(36)          not null,
HARMONIZATION_STATUS   			 VARCHAR(36)          null,
_CREATIONDT          DATE                 null DEFAULT current_timestamp,
constraint PK_HARMONIZATION_PROCESS primary key (HARMONIZATION_PROCESS_ID)
);

COMMENT ON COLUMN HARMONIZATION_PROCESS.HARMONIZATION_PROCESS_ID IS 'The identifier of the harmonization process';
COMMENT ON COLUMN HARMONIZATION_PROCESS.REQUEST_ID IS 'The identifier of the dataset';
COMMENT ON COLUMN HARMONIZATION_PROCESS.COUNTRY_CODE IS 'The identifier of the country';
COMMENT ON COLUMN HARMONIZATION_PROCESS.HARMONIZATION_STATUS IS 'The status of the harmonization process';
COMMENT ON COLUMN HARMONIZATION_PROCESS._CREATIONDT IS 'The date of launch of the process';


/*==============================================================*/
/* Table : HARMONIZATION_PROCESS_SUBMISSIONS                                */
/*==============================================================*/
create table HARMONIZATION_PROCESS_SUBMISSIONS (
HARMONIZATION_PROCESS_ID         INTEGER,
RAW_DATA_SUBMISSION_ID 			 INTEGER,
constraint PK_HARMONIZATION_PROCESS_SUBMISSIONS primary key (HARMONIZATION_PROCESS_ID, RAW_DATA_SUBMISSION_ID)
);

COMMENT ON COLUMN HARMONIZATION_PROCESS_SUBMISSIONS.HARMONIZATION_PROCESS_ID IS 'The identifier of the harmonization process';
COMMENT ON COLUMN HARMONIZATION_PROCESS_SUBMISSIONS.RAW_DATA_SUBMISSION_ID IS 'The identifier of the submission of raw data corresponding';

alter table HARMONIZATION_PROCESS_SUBMISSIONS
   add constraint FK_HARMONIZATION_PROCESS_SUBMISSIONS_ASSOCIATE_PROCESS foreign key (HARMONIZATION_PROCESS_ID)
      references HARMONIZATION_PROCESS (HARMONIZATION_PROCESS_ID)
      on delete restrict on update restrict;

/*==============================================================*/
/* Table : HARMONIZED_LOCATION                                  */
/*==============================================================*/
create table HARMONIZED_LOCATION (
REQUEST_ID           			 VARCHAR(36)          not null,
COUNTRY_CODE         VARCHAR(36)          not null,
PLOT_CODE            VARCHAR(36)          not null,
CLUSTER_CODE         VARCHAR(36)          not null,
LAT                  FLOAT8               null,
LONG                 FLOAT8               null,
IS_PLOT_COORDINATES_DEGRADED   CHAR(1)	  null,
CELL_ID_NUTS0		 VARCHAR(36)          null,
CELL_ID_100          VARCHAR(36)          null,
CELL_ID_50           VARCHAR(36)          null,
CELL_ID				 VARCHAR(36)          null,
COMMENT              VARCHAR(255)         null,
constraint PK_HARMONIZED_LOCATION primary key (REQUEST_ID, COUNTRY_CODE, PLOT_CODE)
);



-- Ajout de la colonne point PostGIS
SELECT AddGeometryColumn('harmonized_data','harmonized_location','the_geom',3035,'POINT',2);
		
-- Spatial Index on the_geom 
CREATE INDEX IX_HARMONIZED_LOCATION_SPATIAL_INDEX ON harmonized_data.harmonized_location USING GIST ( the_geom GIST_GEOMETRY_OPS );

/*========================================================================*/
/*	Add a trigger to fill the the_geom column of the location table       */
/*========================================================================*/
CREATE OR REPLACE FUNCTION harmonized_data.geomfromcoordinate() RETURNS "trigger" AS
$BODY$
BEGIN
    NEW.the_geom = public.ST_Transform(public.GeometryFromText('POINT(' || NEW.LONG || ' ' || NEW.LAT || ')', 4326), 3035);
    RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER geom_trigger
  BEFORE INSERT 
  ON harmonized_data.HARMONIZED_LOCATION
  FOR EACH ROW
  EXECUTE PROCEDURE harmonized_data.geomfromcoordinate();
    
  

/*========================================================================*/
/*	Add a function to fill the CELL_ID columns of the location table       */
/*========================================================================*/
CREATE OR REPLACE FUNCTION harmonized_data.calculatecellid(requestId varchar, country_code varchar) returns void AS
$$	
	-- NUTS 0 (country code)
	UPDATE 	HARMONIZED_LOCATION
	SET    	CELL_ID_NUTS0 = group_mode.dst_code
	FROM 	metadata.group_mode 
	WHERE   group_mode.dst_unit = 'NUTS_COUNTRY_CODE' 
	AND     group_mode.src_code = HARMONIZED_LOCATION.country_code
	AND 	HARMONIZED_LOCATION.request_id = $1
	AND		HARMONIZED_LOCATION.country_code = $2;
		
	-- 100 km x 100 km
	UPDATE 	HARMONIZED_LOCATION
	SET    	CELL_ID_100 = grid_eu25_100k.CELL_ID
	FROM 	mapping.grid_eu25_100k
	WHERE   HARMONIZED_LOCATION.the_geom && grid_eu25_100k.the_geom 
	AND 	ST_Intersects(HARMONIZED_LOCATION.the_geom, grid_eu25_100k.the_geom)
	AND 	HARMONIZED_LOCATION.request_id = $1
	AND		HARMONIZED_LOCATION.country_code = $2;
	
	-- 50 km x 50 km
	UPDATE 	HARMONIZED_LOCATION
	SET    	CELL_ID_50 = grid_eu25_50k.CELL_ID
	FROM 	mapping.grid_eu25_50k
	WHERE   HARMONIZED_LOCATION.the_geom && grid_eu25_50k.the_geom 
	AND 	ST_Intersects(HARMONIZED_LOCATION.the_geom, grid_eu25_50k.the_geom)
	AND 	HARMONIZED_LOCATION.request_id = $1
	AND		HARMONIZED_LOCATION.country_code = $2;
	
	-- 1 km x 1 km
	UPDATE 	HARMONIZED_LOCATION
	SET    	CELL_ID = grid_eu25_1k.CELL_ID
	FROM 	mapping.grid_eu25_1k
	WHERE   HARMONIZED_LOCATION.the_geom && grid_eu25_1k.the_geom 
	AND 	ST_Intersects(HARMONIZED_LOCATION.the_geom, grid_eu25_1k.the_geom)
	AND 	HARMONIZED_LOCATION.request_id = $1
	AND		HARMONIZED_LOCATION.country_code = $2;	
	
$$
LANGUAGE SQL;
  
  
  
/*==============================================================*/
/* Table : HARMONIZED_PLOT_DATA                                 */
/*==============================================================*/
create table HARMONIZED_PLOT_DATA (
REQUEST_ID           VARCHAR(36)          not null,
COUNTRY_CODE         VARCHAR(36)          not null,
STRATUM_CODE         VARCHAR(36)          null,
PLOT_CODE            VARCHAR(36)          not null,
CYCLE	             VARCHAR(36)          not null,
REF_YEAR_BEGIN       INTEGER	          not null,
REF_YEAR_END	     INTEGER	          not null,
INV_DATE             DATE                 null,
IS_FOREST_PLOT		 CHAR(1)	          null,
IS_PARTITIONNING_PLOT  CHAR(1)	          null,
COMMENT              VARCHAR(1000)         null,
constraint PK_HARMONIZED_PLOT_DATA primary key (REQUEST_ID, COUNTRY_CODE, PLOT_CODE, CYCLE)
);


alter table HARMONIZED_PLOT_DATA
   add constraint FK_HARMONIZED_PLOT_DATA_ASSOCIATE_LOCATION foreign key (REQUEST_ID, PLOT_CODE, COUNTRY_CODE)
      references HARMONIZED_LOCATION (REQUEST_ID, PLOT_CODE, COUNTRY_CODE)
      on delete restrict on update restrict;
      
      
CREATE INDEX HARMONIZED_PLOT_DATA_COUNTRY_IDX ON HARMONIZED_PLOT_DATA (COUNTRY_CODE);
      

/*==============================================================*/
/* Table : HARMONIZED_SPECIES_DATA                              */
/*==============================================================*/
create table HARMONIZED_SPECIES_DATA (
REQUEST_ID        	 VARCHAR(36)          not null,
COUNTRY_CODE         VARCHAR(36)          not null,
PLOT_CODE            VARCHAR(36)          not null,
CYCLE	             VARCHAR(36)          not null,
SPECIES_CODE         VARCHAR(36)          not null,
DBH_CLASS            VARCHAR(36)          null,
NATIONAL_SPECIES_CODE         VARCHAR(36)          null,
COMMENT              VARCHAR(255)         null,
constraint PK_HARMONIZED_SPECIES_DATA primary key (REQUEST_ID, COUNTRY_CODE, PLOT_CODE, CYCLE, DBH_CLASS, SPECIES_CODE)
);


      
alter table HARMONIZED_SPECIES_DATA
   add constraint FK_HARMONIZED_SPECIES_ASSOCIATE_PLOT_DAT foreign key (REQUEST_ID, COUNTRY_CODE, PLOT_CODE, CYCLE)
      references HARMONIZED_PLOT_DATA (REQUEST_ID, COUNTRY_CODE, PLOT_CODE, CYCLE)
      on delete restrict on update restrict;     
      


SET SEARCH_PATH = harmonized_data, public;

/*==============================================================*/
/* Table : HARMONIZED_FOREST_MAP_VALIDATION                     */
/*==============================================================*/
--DROP TABLE HARMONIZED_FOREST_MAP_VALIDATION;
CREATE TABLE HARMONIZED_FOREST_MAP_VALIDATION (
REQUEST_ID        	 	VARCHAR(36)          not null,
COUNTRY_CODE         	VARCHAR(36)          not null,
PLOT_CODE 		 		VARCHAR(36)          not null,
PLOT_NUMBER 		 	serial,
CYCLE			 		VARCHAR(36)  		NOT NULL,
INV_DATE			 	DATE		 		NOT NULL,
NUTS_CODE			 	VARCHAR(36) 		NULL,
DOMAIN_FOREST		 	VARCHAR(36) 		NULL,
IS_FOREST_PLOT		 	VARCHAR(2) 	 		NULL,
FOREST_TYPE				VARCHAR(36) 		NULL,
CROWN_COVER				VARCHAR(36) 		NULL,
YOUNG_STAND				VARCHAR(36) 		NULL,
BOUNDARY				VARCHAR(36) 		NULL,
ELEVATION				FLOAT		 		NULL,
SLOPE					FLOAT		 		NULL,
ASPECT					VARCHAR(36)	 		NULL,
CENTER					VARCHAR(36) 	 		NULL,
A11						VARCHAR(36) 	 		NULL,
A12						VARCHAR(36) 	 		NULL,
A13						VARCHAR(36) 	 		NULL,
A14						VARCHAR(36) 	 		NULL,
A15						VARCHAR(36) 	 		NULL,
A21						VARCHAR(36) 	 		NULL,
A22						VARCHAR(36) 	 		NULL,
A23						VARCHAR(36) 	 		NULL,
A24						VARCHAR(36) 	 		NULL,
A25						VARCHAR(36) 	 		NULL,
A31						VARCHAR(36) 	 		NULL,
A32						VARCHAR(36) 	 		NULL,
A34						VARCHAR(36) 	 		NULL,
A35						VARCHAR(36) 	 		NULL,
A41						VARCHAR(36) 	 		NULL,
A42						VARCHAR(36) 	 		NULL,
A43						VARCHAR(36) 	 		NULL,
A44						VARCHAR(36) 	 		NULL,
A45						VARCHAR(36) 	 		NULL,
A51						VARCHAR(36) 	 		NULL,
A52						VARCHAR(36) 	 		NULL,
A53						VARCHAR(36) 	 		NULL,
A54						VARCHAR(36) 	 		NULL,
A55						VARCHAR(36) 	 		NULL,
COMMENT					TEXT		 		NULL,
constraint PK_HARMONIZED_FOREST_MAP_VALIDATION primary key (REQUEST_ID, COUNTRY_CODE, PLOT_CODE, CYCLE)
);

COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.REQUEST_ID IS 'The request identifier';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.COUNTRY_CODE IS 'The country code';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.PLOT_CODE IS 'The plot identifier';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.PLOT_NUMBER IS 'The plot identifier (serial)';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.CYCLE IS 'The cycle of inventory';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.INV_DATE IS 'The date when the field plot has been assessed';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.NUTS_CODE IS 'NUTS 3 code';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.DOMAIN_FOREST IS 'Indicates whether the plot belongs to the domain in which the forest area (is_forest_plot) is defined';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.IS_FOREST_PLOT IS 'Tells if the plot corresponds to a forest plot or not';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.FOREST_TYPE IS 'Indicates whether the plot is a coniferous, broadleaved, or mixed plot';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.CROWN_COVER IS 'Indicates whether the plot belongs to a closed or open forest ';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.YOUNG_STAND IS 'Indicates whether the forest in the plot is classified in regeneration/young stand';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.BOUNDARY IS 'Indicates whether the plot is crossed by a boundary';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.ELEVATION IS 'Elevation of the NFI plot in meters';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.SLOPE IS 'Slope of the NFI plot in percent';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.ASPECT IS 'Aspect of the NFI plot in cardinal points';
COMMENT ON COLUMN HARMONIZED_FOREST_MAP_VALIDATION.CENTER IS 'The extracted pixel value for the center cell';


      




-- Indexes
CREATE INDEX HARMONIZED_LOCATION_PLOT_CODE_IDX ON harmonized_location (request_id, country_code, plot_code);
CREATE INDEX HARMONIZED_PLOT_DATA_PLOT_CODE_IDX ON harmonized_plot_data (request_id, country_code, plot_code);
CREATE INDEX HARMONIZED_SPECIES_DATA_PLOT_CODE_IDX ON harmonized_species_data (request_id, country_code, plot_code);

      