
		Docs.classData ={"id":"apidocs","iconCls":"icon-docs","text":"API Documentation","singleClickExpand":true,"children":[
                {"id":"pkg-Ext","text":"Ext","iconCls":"icon-pkg","cls":"package","singleClickExpand":true, children:[
                {"id":"pkg-Ext.ux","text":"ux","iconCls":"icon-pkg","cls":"package","singleClickExpand":true, children:[
                {"id":"pkg-Ext.ux.form","text":"form","iconCls":"icon-pkg","cls":"package","singleClickExpand":true, children:[
                {"href":"output/Ext.ux.form.BasicCheckbox.html","text":"BasicCheckbox","id":"Ext.ux.form.BasicCheckbox","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Ext.ux.form.Checkbox.html","text":"Checkbox","id":"Ext.ux.form.Checkbox","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Ext.ux.form.CycleCheckbox.html","text":"CycleCheckbox","id":"Ext.ux.form.CycleCheckbox","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				]}
				]}
				]}
				,
                {"id":"pkg-Genapp","text":"Genapp","iconCls":"icon-pkg","cls":"package","singleClickExpand":true, children:[
                {"id":"pkg-Genapp.form","text":"form","iconCls":"icon-pkg","cls":"package","singleClickExpand":true, children:[
                {"href":"output/Genapp.form.DateRangeField.html","text":"DateRangeField","id":"Genapp.form.DateRangeField","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.form.GeometryField.html","text":"GeometryField","id":"Genapp.form.GeometryField","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.form.NumberRangeField.html","text":"NumberRangeField","id":"Genapp.form.NumberRangeField","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.form.TwinNumberField.html","text":"TwinNumberField","id":"Genapp.form.TwinNumberField","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				]}
				,
                {"id":"pkg-Genapp.menu","text":"menu","iconCls":"icon-pkg","cls":"package","singleClickExpand":true, children:[
                {"href":"output/Genapp.menu.DateRangeMenu.html","text":"DateRangeMenu","id":"Genapp.menu.DateRangeMenu","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.menu.NumberRangeMenu.html","text":"NumberRangeMenu","id":"Genapp.menu.NumberRangeMenu","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				]}
				,
                {"href":"output/Genapp.CardPanel.html","text":"CardPanel","id":"Genapp.CardPanel","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.ConsultationPanel.html","text":"ConsultationPanel","id":"Genapp.ConsultationPanel","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.DateRangePicker.html","text":"DateRangePicker","id":"Genapp.DateRangePicker","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.DetailsPanel.html","text":"DetailsPanel","id":"Genapp.DetailsPanel","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.FieldForm.html","text":"FieldForm","id":"Genapp.FieldForm","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.MapPanel.html","text":"MapPanel","id":"Genapp.MapPanel","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.NumberRangePicker.html","text":"NumberRangePicker","id":"Genapp.NumberRangePicker","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.PDFComponent.html","text":"PDFComponent","id":"Genapp.PDFComponent","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				,
                {"href":"output/Genapp.PredefinedRequestPanel.html","text":"PredefinedRequestPanel","id":"Genapp.PredefinedRequestPanel","isClass":true,"iconCls":"icon-cls","cls":"cls","leaf":true}
				]}
				]};
        Docs.icons = {
        
			"Ext.ux.form.BasicCheckbox":"icon-cls"
			,
			"Ext.ux.form.Checkbox":"icon-cls"
			,
			"Ext.ux.form.CycleCheckbox":"icon-cls"
			,
			"Genapp.form.DateRangeField":"icon-cls"
			,
			"Genapp.form.GeometryField":"icon-cls"
			,
			"Genapp.form.NumberRangeField":"icon-cls"
			,
			"Genapp.form.TwinNumberField":"icon-cls"
			,
			"Genapp.menu.DateRangeMenu":"icon-cls"
			,
			"Genapp.menu.NumberRangeMenu":"icon-cls"
			,
			"Genapp.CardPanel":"icon-cls"
			,
			"Genapp.ConsultationPanel":"icon-cls"
			,
			"Genapp.DateRangePicker":"icon-cls"
			,
			"Genapp.DetailsPanel":"icon-cls"
			,
			"Genapp.FieldForm":"icon-cls"
			,
			"Genapp.MapPanel":"icon-cls"
			,
			"Genapp.NumberRangePicker":"icon-cls"
			,
			"Genapp.PDFComponent":"icon-cls"
			,
			"Genapp.PredefinedRequestPanel":"icon-cls"
			};
    