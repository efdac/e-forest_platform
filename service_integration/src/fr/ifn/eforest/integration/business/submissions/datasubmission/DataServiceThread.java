/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.integration.business.submissions.datasubmission;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.ifn.eforest.common.business.AbstractThread;
import fr.ifn.eforest.common.business.ThreadLock;
import fr.ifn.eforest.integration.database.rawdata.SubmissionData;
import fr.ifn.eforest.integration.mail.EforestEmailer;

/**
 * Thread running the data importation.
 */
public class DataServiceThread extends AbstractThread {

	/**
	 * Local variables.
	 */
	private Integer submissionId;
	private String countryCode;
	private Map<String, String> requestParameters;

	/**
	 * The logger used to log the errors or several information.
	 * 
	 * @see org.apache.log4j.Logger
	 */
	protected final transient Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Emailer service.
	 */
	EforestEmailer eforestEmailer = new EforestEmailer();

	/**
	 * Constructs a DataServiceThread object.
	 * 
	 * @param submissionId
	 *            the identifier of the submission
	 * @param countryCode
	 *            the country code
	 * @param requestParameters
	 *            the map of static parameter values (the upload path, ...)
	 * @throws Exception
	 */
	public DataServiceThread(Integer submissionId, String countryCode, Map<String, String> requestParameters) throws Exception {

		this.submissionId = submissionId;
		this.countryCode = countryCode;
		this.requestParameters = requestParameters;

	}

	/**
	 * Launch in thread mode the check(s).
	 */
	public void run() {

		try {

			Date startDate = new Date();
			logger.debug("Start of the data upload process " + startDate + ".");

			// Submit the data
			DataService dataService = new DataService(this);
			SubmissionData submission = dataService.submitData(submissionId, countryCode, requestParameters);

			// Log the end the the request
			Date endDate = new Date();
			logger.debug("Data Upload process terminated successfully in " + (endDate.getTime() - startDate.getTime()) / 1000.00 + " sec.");

			// Send a email
			eforestEmailer.send("New data submission", submission);

		} finally {
			// Remove itself from the list of running checks
			ThreadLock.getInstance().releaseProcess("" + submissionId);

		}

	}

}
