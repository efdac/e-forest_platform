/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.integration.business;

import static fr.ifn.eforest.common.business.UnitTypes.*;
import static fr.ifn.eforest.common.business.checks.CheckCodes.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import fr.ifn.eforest.common.util.CSVFile;
import fr.ifn.eforest.common.util.InconsistentNumberOfColumns;
import fr.ifn.eforest.common.business.AbstractThread;
import fr.ifn.eforest.common.business.Data;
import fr.ifn.eforest.common.business.GenericMapper;
import fr.ifn.eforest.common.business.MappingTypes;
import fr.ifn.eforest.common.business.Schemas;
import fr.ifn.eforest.common.business.UnitTypes;
import fr.ifn.eforest.common.business.checks.CheckException;
import fr.ifn.eforest.common.database.GenericDAO;
import fr.ifn.eforest.common.database.GenericData;
import fr.ifn.eforest.common.database.metadata.FieldData;
import fr.ifn.eforest.common.database.metadata.MetadataDAO;
import fr.ifn.eforest.common.database.metadata.TableFieldData;
import fr.ifn.eforest.common.database.metadata.TableFormatData;
import fr.ifn.eforest.integration.database.rawdata.CheckErrorDAO;
import fr.ifn.eforest.integration.database.rawdata.SubmissionDAO;

/**
 * This service manages the integration process.
 */
public class IntegrationService extends GenericMapper {

	/**
	 * The logger used to log the errors or several information.
	 * 
	 * @see org.apache.log4j.Logger
	 */
	private final transient Logger logger = Logger.getLogger(this.getClass());

	/**
	 * The database accessors.
	 */
	private MetadataDAO metadataDAO = new MetadataDAO();
	private GenericDAO genericDAO = new GenericDAO();
	private CheckErrorDAO checkErrorDAO = new CheckErrorDAO();
	private SubmissionDAO submissionDAO = new SubmissionDAO();

	/**
	 * Insert a dataset coming from a CSV in database.
	 * 
	 * @param submissionId
	 *            the submission identifier
	 * @param countryCode
	 *            the country code
	 * @param filePath
	 *            the source data file path
	 * @param sourceFormat
	 *            the source format identifier
	 * @param requestParameters
	 *            the static values (COUNTRY_CODE, REQUEST_ID, ...)
	 * @param thread
	 *            the thread that is running the process (optional, this is too keep it informed of the progress)
	 */
	public boolean insertData(Integer submissionId, String countryCode, String filePath, String sourceFormat, Map<String, String> requestParameters,
			AbstractThread thread) throws Exception {

		logger.debug("insertData");
		boolean isInsertValid = true;
		CSVFile csvFile = null;
		try {

			// First get the description of the content of the CSV file
			List<FieldData> sourceFieldDescriptors = metadataDAO.getFileFields(sourceFormat);

			// Parse the CSV file and check the number of lines/columns

			try {
				csvFile = new CSVFile(filePath);
			} catch (InconsistentNumberOfColumns ince) {

				// The file number of columns changes from line to line
				CheckException e = new CheckException(WRONG_FIELD_NUMBER);
				e.setSourceFormat(sourceFormat);
				e.setSubmissionId(submissionId);
				e.setCountryCode(countryCode);
				throw e;
			}

			// Check that the file is not empty
			if (csvFile.getRowsCount() == 0) {
				CheckException e = new CheckException(EMPTY_FILE);
				e.setSourceFormat(sourceFormat);
				e.setSubmissionId(submissionId);
				e.setCountryCode(countryCode);
				throw e;
			}

			// check that the file as the expected number of columns
			if (csvFile.getColsCount() != sourceFieldDescriptors.size()) {
				CheckException e = new CheckException(WRONG_FIELD_NUMBER);
				e.setSourceFormat(sourceFormat);
				e.setSubmissionId(submissionId);
				e.setCountryCode(countryCode);
				throw e;
			}

			// Store the name and path of the file
			submissionDAO.addSubmissionFile(submissionId, sourceFormat, filePath, csvFile.getRowsCount());

			// Get the destination formats
			Map<String, TableFormatData> destFormatsMap = metadataDAO.getFormatMapping(sourceFormat, MappingTypes.FILE_MAPPING);

			// Prepare the storage of the description of the destination tables
			Map<String, List<TableFieldData>> tableFieldsMap = new HashMap<String, List<TableFieldData>>();

			// Get the description of the destination tables
			Iterator<TableFormatData> destFormatIter = destFormatsMap.values().iterator();
			while (destFormatIter.hasNext()) {
				TableFormatData destFormat = destFormatIter.next();

				// Get the list of fields for the table
				List<TableFieldData> destFieldDescriptors = metadataDAO.getTableFields(destFormat.getFormat(), countryCode);

				// Store in a map
				tableFieldsMap.put(destFormat.getFormat(), destFieldDescriptors);
			}

			// Get the field mapping
			// We create a map, giving for each field name a descriptor with the name of the destination table and column.
			Map<String, TableFieldData> mappedFieldDescriptors = metadataDAO.getFieldMapping(sourceFormat, MappingTypes.FILE_MAPPING);

			// Prepare the common destination fields for each table (indexed by destination format)
			Map<String, GenericData> commonFieldsMap = new HashMap<String, GenericData>();

			// We go thru the expected destination fields of each table
			destFormatIter = destFormatsMap.values().iterator();
			while (destFormatIter.hasNext()) {
				TableFormatData destFormat = destFormatIter.next();

				List<TableFieldData> destFieldDescriptors = tableFieldsMap.get(destFormat.getFormat());

				Iterator<TableFieldData> destDescriptorIter = destFieldDescriptors.iterator();
				while (destDescriptorIter.hasNext()) {
					TableFieldData destFieldDescriptor = destDescriptorIter.next();

					// If the field is not in the mapping
					TableFieldData destFound = mappedFieldDescriptors.get(destFieldDescriptor.getFieldName());
					if (destFound == null) {

						// We look in the request parameters for the missing field
						String value = requestParameters.get(destFieldDescriptor.getFieldName());
						if (value != null) {

							// We get the metadata for the fieldFieldData
							FieldData fieldData = metadataDAO.getFileField(destFieldDescriptor.getFieldName());

							// We add it to the common fields
							GenericData commonField = new GenericData();
							commonField.setFormat(destFieldDescriptor.getFieldName());
							commonField.setColumnName(destFieldDescriptor.getColumnName());
							commonField.setType(fieldData.getType());
							commonField.setValue(convertType(fieldData, value));
							commonFieldsMap.put(commonField.getFormat(), commonField);
						}
					}
				}

			}

			// Travel the content of the csv file line by line
			int row = 1;
			String[] csvLine = csvFile.readNextLine();
			while (csvLine != null) {

				try {

					if (thread != null) {
						thread.updateInfo("Inserting " + sourceFormat + " data", row, csvFile.getRowsCount());
					}

					// List of tables where to insert data
					Set<String> tablesContent = new TreeSet<String>();

					// Store each column in the destination table container
					for (int col = 0; col < csvFile.getColsCount(); col++) {

						// Get the value to insert
						String value = csvLine[col];

						// The value once transformed into an Object
						Object valueObj = null;

						// Get the field descriptor
						FieldData sourceFieldDescriptor = sourceFieldDescriptors.get(col);

						// Check the mask if available and the variable is not a date (date format is tested with a date format)
						if (sourceFieldDescriptor.getMask() != null && !sourceFieldDescriptor.getType().equalsIgnoreCase(DATE)) {
							try {
								checkMask(sourceFieldDescriptor.getMask(), value);
							} catch (CheckException e) {
								// Complete the description of the problem
								e.setSourceFormat(sourceFormat);
								e.setExpectedValue(sourceFieldDescriptor.getMask());
								e.setFoundValue(value);
								e.setSourceData(sourceFieldDescriptor.getData());
								e.setLineNumber(row + 1);
								e.setSubmissionId(submissionId);
								e.setCountryCode(countryCode);
								throw e;
							}
						}

						// Check and convert the type
						try {
							valueObj = convertType(sourceFieldDescriptor, value);
						} catch (CheckException e) {
							// Complete the description of the problem
							e.setSourceFormat(sourceFormat);
							e.setSourceData(sourceFieldDescriptor.getData());
							if (e.getExpectedValue() == null) {
								e.setExpectedValue(sourceFieldDescriptor.getType());
							}
							e.setFoundValue(value);
							e.setLineNumber(row + 1);
							e.setSubmissionId(submissionId);
							e.setCountryCode(countryCode);
							throw e;
						}

						// Get the mapped column destination
						TableFieldData mappedFieldDescriptor = mappedFieldDescriptors.get(sourceFieldDescriptor.getData());

						if (mappedFieldDescriptor == null) {
							CheckException e = new CheckException(NO_MAPPING);
							e.setSourceFormat(sourceFormat);
							e.setFoundValue(sourceFieldDescriptor.getData());
							e.setLineNumber(row + 1);
							e.setSubmissionId(submissionId);
							e.setCountryCode(countryCode);
							throw e;
						}

						// Create the descriptor of the data
						GenericData data = new GenericData();
						data.setFormat(mappedFieldDescriptor.getFormat());
						data.setColumnName(mappedFieldDescriptor.getColumnName());
						data.setType(sourceFieldDescriptor.getType());
						data.setValue(valueObj);

						// Store the descriptor in the common list
						commonFieldsMap.put(sourceFieldDescriptor.getData(), data);

						// Store the name of the table
						tablesContent.add(mappedFieldDescriptor.getFormat());

					}

					// Insert the content of the line in the destination tables
					Iterator<String> tablesIter = tablesContent.iterator();
					while (tablesIter.hasNext()) {
						String format = tablesIter.next();

						// Get the destination table name
						TableFormatData destTable = destFormatsMap.get(format);
						String tableName = destTable.getTableName();

						try {

							// Increment the line number
							GenericData lineNumber = new GenericData();
							lineNumber.setColumnName(Data.LINE_NUMBER);
							lineNumber.setType(UnitTypes.INTEGER);
							lineNumber.setFormat(Data.LINE_NUMBER);
							lineNumber.setValue(row + 1);
							commonFieldsMap.put(Data.LINE_NUMBER, lineNumber);

							// Insert a list of values in the destination table
							genericDAO.insertData(Schemas.RAW_DATA, tableName, tableFieldsMap.get(format), commonFieldsMap);
						} catch (CheckException e) {
							// Complete the description of the problem
							e.setSourceFormat(sourceFormat);
							e.setLineNumber(row + 1);
							e.setSubmissionId(submissionId);
							e.setCountryCode(countryCode);
							throw e;
						}

					}

				} catch (CheckException ce) {
					// Line-Level catch of checked exceptions
					isInsertValid = false;
					logger.error("CheckException", ce);

					// We store the check exception in database and continue to the next line
					checkErrorDAO.createCheckError(ce);

				}

				csvLine = csvFile.readNextLine();
				row++;

			}
		} catch (CheckException ce) {
			// File-Level catch of checked exceptions
			isInsertValid = false;
			ce.setSourceFormat(sourceFormat);
			ce.setSubmissionId(submissionId);
			logger.error("CheckException", ce);

			// Store the check exception in database
			checkErrorDAO.createCheckError(ce);

		} catch (Exception e) {
			// File-Level catch of unexpected exceptions
			isInsertValid = false;
			CheckException ce = new CheckException(UNEXPECTED_SQL_ERROR);
			ce.setSourceFormat(sourceFormat);
			ce.setSubmissionId(submissionId);
			logger.error("Unexpected Exception", e);

			// Store the check exception in database
			checkErrorDAO.createCheckError(ce);

		} finally {
			if (csvFile != null) {
				csvFile.close();
			}
		}

		return isInsertValid;

	}
}
