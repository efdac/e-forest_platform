/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.integration.business.checks;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import fr.ifn.eforest.integration.database.rawdata.DataSubmissionDAO;
import fr.ifn.eforest.integration.database.rawdata.DataSubmissionData;
import fr.ifn.eforest.integration.business.submissions.SubmissionStatus;
import fr.ifn.eforest.integration.business.submissions.SubmissionStep;
import fr.ifn.eforest.integration.business.submissions.SubmissionTypes;
import fr.ifn.eforest.common.database.metadata.CheckData;
import fr.ifn.eforest.common.database.metadata.ChecksDAO;
import fr.ifn.eforest.integration.database.rawdata.SubmissionDAO;
import fr.ifn.eforest.integration.database.rawdata.SubmissionData;
import fr.ifn.eforest.integration.database.rawdata.CheckErrorDAO;

/**
 * Service managing the checks.
 */
public class CheckService {

	/**
	 * The local logger.
	 */
	private final transient Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Declare the DAOs.
	 */
	private SubmissionDAO submissionDAO = new SubmissionDAO();
	private DataSubmissionDAO dataSubmissionDAO = new DataSubmissionDAO();
	private ChecksDAO checksDAO = new ChecksDAO();
	private CheckErrorDAO checkErrorDAO = new CheckErrorDAO();

	/**
	 * Get the status of the submission.
	 * 
	 * @param submissionId
	 *            the identifier of the submission
	 * @return the status of the submission
	 * @throws Exception
	 */
	public String checkSubmissionStatus(int submissionId) throws Exception {

		SubmissionData submission = submissionDAO.getSubmission(submissionId);

		return submission.getStatus();

	}

	/**
	 * Checks the submissions.
	 * 
	 * @param submissionId
	 *            the identifier of the submission
	 */
	public void runChecks(Integer submissionId) {

		List<CheckData> checksList;

		try {

			long startTime = new Date().getTime();
			logger.debug("Start checks ...");

			// Change the status of the submission
			submissionDAO.updateSubmissionStatus(submissionId, SubmissionStep.DATA_CHECKED, SubmissionStatus.RUNNING);

			// Get the list of checks to run
			SubmissionData submission = submissionDAO.getSubmission(submissionId);
			if (submission.getType().equalsIgnoreCase(SubmissionTypes.DATA)) {
				DataSubmissionData dataSubmission = dataSubmissionDAO.getSubmission(submissionId);
				logger.debug("Request ID : " + dataSubmission.getRequestId() + " Country code : " + submission.getCountryCode());
				checksList = checksDAO.getChecks(dataSubmission.getRequestId(), submission.getCountryCode());
			} else {
				checksList = checksDAO.getChecks(submission.getType(), submission.getCountryCode());
			}

			// Launchs the checks
			checksDAO.executeChecks(submissionId, checksList);

			long endTime = new Date().getTime();
			logger.debug("Total check time : " + (endTime - startTime) / 1000.00 + "s.");

			// Gets the number of errors and of warnings for the step(s)
			int nbrErrors = checkErrorDAO.countPerCheck(submissionId, CheckStep.CONFORMITY, CheckLevel.ERROR);
			int nbrWarnings = checkErrorDAO.countPerCheck(submissionId, CheckStep.CONFORMITY, CheckLevel.WARNING);

			logger.debug("Compliance check(s) ended with " + nbrErrors + " error(s) and " + nbrWarnings + " warning(s).");

			// Update the submission status
			if (nbrErrors == 0) {
				if (nbrWarnings == 0) {
					submissionDAO.updateSubmissionStatus(submissionId, SubmissionStep.DATA_CHECKED, SubmissionStatus.OK);
				} else {
					submissionDAO.updateSubmissionStatus(submissionId, SubmissionStep.DATA_CHECKED, SubmissionStatus.WARNING);
				}
			} else {
				submissionDAO.updateSubmissionStatus(submissionId, SubmissionStep.DATA_CHECKED, SubmissionStatus.ERROR);
			}

		} catch (Exception e) {
			// Update the status of the submission to "CRASH"
			try {
				// Set the submission status
				submissionDAO.updateSubmissionStatus(submissionId, SubmissionStep.DATA_CHECKED, SubmissionStatus.CRASH);

			} catch (Exception ignored) {
				logger.error("Error while updating submission status to CRASH", ignored);
			}
		}

	}
}
