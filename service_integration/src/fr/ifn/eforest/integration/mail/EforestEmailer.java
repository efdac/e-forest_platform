/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.integration.mail;

import javax.mail.Session;

import org.apache.log4j.Logger;

import fr.ifn.eforest.common.util.Emailer;
import fr.ifn.eforest.common.database.metadata.MetadataDAO;
import fr.ifn.eforest.integration.database.rawdata.SubmissionData;
import fr.ifn.eforest.common.database.website.ApplicationParametersDAO;

/**
 * Class used to send the emails.
 */
public class EforestEmailer {

	/**
	 * The local logger.
	 */
	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * The emailer.
	 */
	private Emailer emailer = new Emailer();
	private static final String FROM_NAME = "[Eforest]";

	/**
	 * The DAOs.
	 */
	private ApplicationParametersDAO parameterDAO = new ApplicationParametersDAO();
	private MetadataDAO metadataDAO = new MetadataDAO();

	/**
	 * Sends an email to the administrator.
	 * 
	 * @param title
	 *            The title of the mail
	 * @param submission
	 *            the submission object
	 */
	public void send(String title, SubmissionData submission) {
		try {
			String toMail = parameterDAO.getApplicationParameter("toMail");
			String fromMail = parameterDAO.getApplicationParameter("fromMail");

			StringBuffer message = new StringBuffer("A new submission has been done on the Eforest web site\n\n");
			message.append("The submission id is: " + submission.getSubmissionId() + "\n");

			try {

				message.append("\n\n");
				String country = metadataDAO.getMode("COUNTRY_CODE", submission.getCountryCode());
				message.append("Country : " + country + "\n");
				message.append("Type : " + submission.getType() + "\n");
				message.append("Step : " + submission.getStep() + "\n");
				message.append("Status : " + submission.getStatus() + "\n");

			} catch (Exception e) {
				logger.error("Error while getting submission information for the email " + e.getMessage());
			}

			Session session = emailer.getTomcatSession();
			emailer.sendEmail(session, fromMail, FROM_NAME, toMail, title, message.toString());

		} catch (Exception ignored) {
			// The mailer is used to send error alerts, it should never throw an exception
			logger.error("Error while sending the mail : " + ignored.getMessage());
		}
	}

}
