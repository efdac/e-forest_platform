/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.integration.database.rawdata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

/**
 * Data Access Object used to calculate the surface of extension of a plot in a strata.
 */
public class PlotSurfaceDAO {

	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Clean the table.
	 */
	private static final String DELETE_PLOT_SURFACE_STMT = "DELETE FROM strata_info WHERE country_code = ?";

	/**
	 * Calculate the cell identifiers for a plot location.
	 */
	private static final String CALCULATE_PLOT_SURFACE_STMT = "INSERT INTO strata_info (country_code, stratum_code, stratum_surface_area, nominal_cluster_size, total_plots, weighted_sum_nb_plots, be) "
			+ " SELECT 	country_code, "
			+ "	stratum_code, "
			+ "	total_stratum_area as stratum_surface_area, "
			+ "	nominal_cluster_size,  "
			+ "	total_plots, "
			+ "	weighted_sum_nb_plots, "
			+ "	total_stratum_area * nominal_cluster_size / weighted_sum_nb_plots as be "
			+ " from strata "
			+ " left join ( "
			+ "	select country_code, stratum_code, max(nb_plots) as nominal_cluster_size, sum(nb_plots) as total_plots, sum(cluster_weight) as weighted_sum_nb_plots "
			+ "	from ( " + //
			"		select country_code, stratum_code, cluster_code, count(*) as nb_plots, sum(statistical_weight) AS cluster_weight " + //
			"		from location  " + //
			"		left join plot_data using (plot_code, country_code) " + //
			"		WHERE country_code = ? " + //
			"		group by country_code, stratum_code, cluster_code " + //
			"	) as foo " + //
			"	group by country_code, stratum_code " + //
			" ) as bar using (country_code, stratum_code) " +
			" WHERE country_code = ?";

	/**
	 * Get a connexion to the database.
	 * 
	 * @return The <code>Connection</code>
	 * @throws NamingException
	 * @throws SQLException
	 */
	private Connection getConnection() throws NamingException, SQLException {

		Context initContext = new InitialContext();
		DataSource ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/rawdata");
		Connection cx = ds.getConnection();

		return cx;
	}

	/**
	 * Calculate the surface of extension of the plots.
	 * 
	 * @param countryCode
	 *            The country code
	 */
	public void calculatePlotSurface(String countryCode) throws Exception {

		Connection con = null;
		PreparedStatement ps = null;
		try {

			con = getConnection();

			// Clean the table
			ps = con.prepareStatement(DELETE_PLOT_SURFACE_STMT);
			ps.setString(1, countryCode);

			logger.trace(DELETE_PLOT_SURFACE_STMT);
			ps.execute();
			ps.close();

			// Calculate the plot surfaces
			ps = con.prepareStatement(CALCULATE_PLOT_SURFACE_STMT);
			ps.setString(1, countryCode);
			ps.setString(2, countryCode);

			logger.trace(CALCULATE_PLOT_SURFACE_STMT);
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

}
