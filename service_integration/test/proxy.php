<?
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/



// TODO Add a :8080 to the base URL in order to redirect to Tomcat 
$url = $_SERVER["REQUEST_URI"];

//$urllist = parse_url($url);

$params = substr($url, strpos($url, "proxy.php?url=") + strlen("proxy.php?url="));

$newuri = "http://localhost:8080/" . $params;

header("Content-Type: text/xml");

$host = 'localhost';
$port = '8080';
$connecttimeout = 30;
$servicetimeout = 30;

// Timeout du script
set_time_limit($servicetimeout + $connecttimeout);

//entête du post
$msg = "GET $newuri HTTP/1.0\r\n" .
$msg .= "Host: localhost\r\n";
$msg .= "Connection: Close\r\n\r\n";

$errno = null;
$errstr = '';

// open the connection
try {
	@ $sock = fsockopen($host, $port, $errno, $errstr, $connecttimeout);
} catch (Exception $e) {
	echo 'Connexion error : ' . $e;
	throw $e;
}

if (!$sock) {
	echo 'Connexion timeout';
}

//info sur la connexion
$infos = stream_get_meta_data($sock);

//envoi
fputs($sock, $msg . $data);

//reception de la reponse
//entete
$body = "";
stream_set_timeout($sock, $servicetimeout);
stream_set_blocking($sock, FALSE);
$info = stream_get_meta_data($sock);

while (!feof($sock) && !$info['timed_out']) {
	$body = $body.fgets($sock, 4096);
}

if ($info['timed_out']) {
	echo "TIMEOUT";
}

$body = substr($body, strpos($body, "Connection: close") + strlen("Connection: close"));

echo $body;

fclose($sock);