// Get the XMLHTTPRequest object
function getXMLHttp()
{
	
  var xmlHttp

  try
  {
    // Firefox, Opera 8.0+, Safari
    xmlHttp = new XMLHttpRequest();
  }
  catch(e)
  {
    // Internet Explorer
    try
    {
      xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX!")
        return false;
      }
    }
  }
  return xmlHttp;
} 

// Make a request
function MakeRequest(url, callback) {
	
  var xmlHttp = getXMLHttp();
  
  xmlHttp.onreadystatechange = function()
  {
    if(xmlHttp.readyState == 4)
    {
      callback(xmlHttp.responseText);
    }
  }

  xmlHttp.open("GET", url, true); 
  xmlHttp.send(null);
}

// Lauch the "getCountries" Request
function getCountries() {
	MakeRequest("proxy.php?url=EforestIntegrationService/MetadataServlet?action=GetCountries", getCountriesHandler);
}

// Handle the "getCountries" response
function getCountriesHandler(response) {

	eval("var countries = "+response);
	
	var content = "";
	for (var i in countries)
	{
		country = countries[i];
		content = content +  '<option value="' + country.mode + '">' + country.label + '</option>'; 
	}
	
	// Set the default country
	selectedCountry = countries[0].mode;
	
	document.getElementById("COUNTRY_CODE").innerHTML=content;	
}

// Lauch the "getJRCRequests" Request
function getJRCRequests() {
	MakeRequest("proxy.php?url=EforestIntegrationService/MetadataServlet?action=GetJRCRequests", getJRCRequestsHandler);
}

// Handle the "getJRCRequests" response
function getJRCRequestsHandler(response) {
		
	eval("var jrcRequests = "+response);
	
	var content = "";
	for (var i in jrcRequests)
	{
		request = jrcRequests[i];
		content = content +  '<option value="' + request.id + '">' + request.label + '</option>'; 
	}
	
	document.getElementById("REQUEST_ID").innerHTML=content;	
}



// Lauch the "getFileFormat" Request
function getFileFormat(countryCode, fileFormat, fieldName) {
	
	var url = "proxy.php?url=EforestIntegrationService/MetadataServlet?action=GetFileFields&COUNTRY_CODE="+countryCode+"&FILE_FORMAT="+fileFormat;
	var xmlHttp = getXMLHttp();
	  
	xmlHttp.onreadystatechange = function()
	{
	    if(xmlHttp.readyState == 4)
	    {
	    	eval("var fields = "+xmlHttp.responseText);
	    	getFileFormatHandler(fieldName, fields);
	    }
	}

	xmlHttp.open("GET", url, true); 
	xmlHttp.send(null);
}

// Handle the "getFileFormat" response
function getFileFormatHandler(fieldName, fields) {
		
	var content = "";
	for (var i in fields)
	{
		field = fields[i];
		content = content +  field.label+", "; 
	}
	content = content.substr(0, content.length-2);
	
	document.getElementById(fieldName).innerHTML=content;	
}

