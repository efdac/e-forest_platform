<head>
<!--
 Copyright 2008-2012 European Union

 Licensed under the EUPL, Version 1.1 or – as soon they
 will be approved by the European Commission - subsequent
 versions of the EUPL (the "Licence");
 You may not use this work except in compliance with the
 Licence.
 You may obtain a copy of the Licence at:

 http://joinup.ec.europa.eu/software/page/eupl/licence-eupl

 Unless required by applicable law or agreed to in
 writing, software distributed under the Licence is
 distributed on an "AS IS" basis,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied.
 See the Licence for the specific language governing
 permissions and limitations under the Licence.
-->

<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="ajax.js"></script>
<script type="text/javascript">
<!--
function init()
{
	getCountries();
}
-->
</script>
</head>

<body onload="init()">

<form name="createPlotLocationSubmissionForm" action='http://localhost:8080/EforestIntegrationService/PlotLocationServlet?action=NewLocationSubmission' method="post">


<p>Country Code :	
<select name="COUNTRY_CODE" ID="COUNTRY_CODE">
</select>
</p>				

<input type="submit" value="Submit">
							
</form>

</body>


