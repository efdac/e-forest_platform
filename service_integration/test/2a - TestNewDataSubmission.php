<head>
<!--
 Copyright 2008-2012 European Union

 Licensed under the EUPL, Version 1.1 or – as soon they
 will be approved by the European Commission - subsequent
 versions of the EUPL (the "Licence");
 You may not use this work except in compliance with the
 Licence.
 You may obtain a copy of the Licence at:

 http://joinup.ec.europa.eu/software/page/eupl/licence-eupl

 Unless required by applicable law or agreed to in
 writing, software distributed under the Licence is
 distributed on an "AS IS" basis,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied.
 See the Licence for the specific language governing
 permissions and limitations under the Licence.
-->

<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="ajax.js"></script>
<script type="text/javascript">
<!--
function init()
{
	getCountries();
	getJRCRequests();
}
-->
</script>
</head>



<body onload="init()">

<form name="envoiForm" action='http://localhost:8080/EforestIntegrationService/DataServlet?action=NewDataSubmission' method="post">


<p>Country Code :	
<select name="COUNTRY_CODE" id="COUNTRY_CODE">
</select>
</p>


<p>JRC Requests:	
<select name="DATASET_ID" id="DATASET_ID">
</select>
</p>
		

<p>User Login :	
<select name="USER_LOGIN">
	<option value="toto">toto</option>
</select>
</p>			

<p>Comment:	
<INPUT TYPE="text" name="COMMENT" id="COMMENT" Value="">
</p>							
		

<input type="submit" value="Submit">

							
</form>

</body>