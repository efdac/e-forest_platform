
<head>
<!--
 Copyright 2008-2012 European Union

 Licensed under the EUPL, Version 1.1 or – as soon they
 will be approved by the European Commission - subsequent
 versions of the EUPL (the "Licence");
 You may not use this work except in compliance with the
 Licence.
 You may obtain a copy of the Licence at:

 http://joinup.ec.europa.eu/software/page/eupl/licence-eupl

 Unless required by applicable law or agreed to in
 writing, software distributed under the Licence is
 distributed on an "AS IS" basis,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied.
 See the Licence for the specific language governing
 permissions and limitations under the Licence.
-->

<META http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<script type="text/javascript" src="ajax.js"></script>
<script type="text/javascript">
<!--

var selectedCountry = null;

// Initialise the input values
function init()
{
	getCountries();
	getFileFormat(selectedCountry, "WP3_PLOT_FILE", "PLOT_FILE_FIELDS");
	getFileFormat(selectedCountry, "WP3_SPECIES_FILE", "SPECIES_FILE_FIELDS");	
}

// Check that the file extension match what is expected
function checkExtension(inputtype, expected) {
  var ext = inputtype.value;
  ext = ext.substring(ext.lastIndexOf(".")+1,ext.length);
  ext = ext.toLowerCase();
  if(ext != expected) {
    alert('Vous avez sélectionné un fichier .'+ext+', merci de sélectionner un fichier .'+expected);
    return false; 
  } else {
    return true;
  } 
}

// Change the country
function updateCountry() {

	selectedCountry = document.getElementById("COUNTRY_CODE").value;
	
	getFileFormat(selectedCountry, "WP3_PLOT_FILE", "PLOT_FILE_FIELDS");
	getFileFormat(selectedCountry, "WP3_SPECIES_FILE", "SPECIES_FILE_FIELDS");	

}

-->
</script>
</head>



<body onload="init()">

<form name="envoiForm" action='http://localhost:8080/EforestIntegrationService/DataServlet?action=UploadData' method="post" enctype="multipart/form-data">

<h1>WP3_REQUEST</h1>
	
<p>Submission ID:	
<INPUT TYPE="text" name="SUBMISSION_ID" id="SUBMISSION_ID" Value="1">
</p>	

<p>Country Code :	
<select name="COUNTRY_CODE" id="COUNTRY_CODE" onChange="updateCountry()">
</select>
</p>


							
<p>Plot File: <input type='file' id='PLOT_FILE' name='PLOT_FILE'  maxlength='500' size='50' onChange="checkExtension(this, 'csv')" />
<br>
Expected format is: <span id="PLOT_FILE_FIELDS"></span>
</p>
			
<p>Species File: <input type='file' id='SPECIES_FILE' name='SPECIES_FILE'  maxlength='500' size='50' onChange="checkExtension(this, 'csv')" />
<br>
Expected format is: <span id="SPECIES_FILE_FIELDS"></span>
</p>
		

<input type="submit" value="Submit">

							
</form>

</body>