/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/
package fr.ifn.eforest.integration;

import fr.ifn.eforest.integration.business.DataServiceTest;
import fr.ifn.eforest.integration.business.GenericMapperTest;
import fr.ifn.eforest.integration.business.LocationServiceTest;
import fr.ifn.eforest.integration.business.StrataServiceTest;
import fr.ifn.eforest.integration.database.MetadataTest;
import junit.framework.Test;
import junit.framework.TestSuite;

//
//Note : In order to use this Test Class correctly under Eclipse, you need to change the working directory to
//${workspace_loc:EFDAC - Framework Contract for forest data and services/service_integration}
//

/**
 * Run all the available tests.
 */
public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for fr.ifn.efdac.integration");

		suite.addTestSuite(DataServiceTest.class);
		suite.addTestSuite(GenericMapperTest.class);
		suite.addTestSuite(LocationServiceTest.class);
		suite.addTestSuite(StrataServiceTest.class);
		suite.addTestSuite(MetadataTest.class);
		return suite;
	}

}
