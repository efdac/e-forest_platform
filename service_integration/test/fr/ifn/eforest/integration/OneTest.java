/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/
package fr.ifn.eforest.integration;

//import fr.ifn.eforest.integration.business.DataServiceTest;
import fr.ifn.eforest.integration.business.GenericMapperTest;
import fr.ifn.eforest.integration.business.DataServiceTest;
import fr.ifn.eforest.integration.business.LocationServiceTest;
import fr.ifn.eforest.integration.database.MetadataTest;

import junit.framework.Test;
import junit.framework.TestSuite;

public class OneTest {

	/**
	 * Methode de lancement des tests.
	 */
	public static Test suite() {

		TestSuite suite = new TestSuite();

		//suite.addTest(new LocationServiceTest("testSubmitPlotLocation"));
		suite.addTest(new DataServiceTest("testDataSubmission"));
		//suite.addTest(new MetadataTest("testGetTablesTree"));
		//suite.addTest(new GenericMapperTest("testGetSortedAncestors"));
		//suite.addTest(new HarmonizationServiceTest("testHarmonizeData"));

		return suite;
	}
}
