<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

// public/index.php
//
// Step 1: APPLICATION_PATH is a constant pointing to our
// application/subdirectory. We use this to add our "library" directory
// to the include_path, so that PHP can find our Zend Framework classes.
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application/'));
set_include_path(
    APPLICATION_PATH . '/../library' 
    . PATH_SEPARATOR . APPLICATION_PATH . '/config'
    . PATH_SEPARATOR . APPLICATION_PATH . '/classes'
    . PATH_SEPARATOR . APPLICATION_PATH . '/classes/harmonized_data'
    . PATH_SEPARATOR . APPLICATION_PATH . '/classes/mapping'
    . PATH_SEPARATOR . APPLICATION_PATH . '/classes/metadata'
    . PATH_SEPARATOR . APPLICATION_PATH . '/classes/raw_data'
    . PATH_SEPARATOR . APPLICATION_PATH . '/classes/website'
    . PATH_SEPARATOR . get_include_path()
);

// Step 2: AUTOLOADER - Set up autoloading.
// This is a nifty trick that allows ZF to load classes automatically so
// that you don't have to litter your code with 'include' or 'require'
// statements.
require_once "Zend/Loader/Autoloader.php";
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);


// Step 3: REQUIRE APPLICATION BOOTSTRAP: Perform application-specific setup
// This allows you to setup the MVC environment to utilize. Later you 
// can re-use this file for testing your applications.
// The try-catch block below demonstrates how to handle bootstrap 
// exceptions. In this application, if defined a different 
// APPLICATION_ENVIRONMENT other than 'production', we will output the 
// exception and stack trace to the screen to aid in fixing the issue
try {
    require '../application/bootstrap.php';
} catch (Exception $exception) {
    echo '<html><body><center>'
       . 'An exception occured while bootstrapping the application.';
    if (defined('APPLICATION_ENVIRONMENT')
        && APPLICATION_ENVIRONMENT != 'production'
    ) {
        echo '<br /><br />' . $exception->getMessage() . '<br />'
           . '<div align="left">Stack Trace:' 
           . '<pre>' . $exception->getTraceAsString() . '</pre></div>';
    }
    echo '</center></body></html>';
    exit(1);
}

// Step 4: DISPATCH:  Dispatch the request using the front controller.
// The front controller is a singleton, and should be setup by now. We 
// will grab an instance and call dispatch() on it, which dispatches the
// current request.
try{
    $front = Zend_Controller_Front::getInstance();
    $front->throwExceptions(true);
    $front->dispatch();
}catch(Exception $e){
    // Exceptions handling
    $log  = 'Error: ' . $e->getMessage()
          . "\nin file: " . $e->getFile()
          . "\non line: " . $e->getLine()
          . "\ntrace: " . print_r($e->getTrace(), true);
    $logger = Zend_Registry :: get("logger");
    if (isset($logger)) {
        $logger->err($log);   //change the level to EMG
    } elseif (true) {
        error_log($log);    //log to php error log
    }

    header(" ",true,500);
}