<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/





/**
 * This is the model for managing aggregation grids.
 * @package models
 */
class Model_Grids extends Zend_Db_Table_Abstract {

	var $logger;

	/**
	 * Initialisation
	 */
	public function init() {

		// Initialise the logger
		$this->logger = Zend_Registry::get("logger");
	}

	/**
	 * Get the list of available grids.
	 *
	 * @return Array[Grid]
	 */
	public function getGrids() {
		$db = $this->getAdapter();

		$req = " SELECT * "." FROM grid_definition "." ORDER BY position ";

		Zend_Registry::get("logger")->info('getGrids : '.$req);

		$select = $db->prepare($req);
		$select->execute();

		$result = array();
		foreach ($select->fetchAll() as $row) {
			$grid = new Grid();
			$grid->name = $row['grid_name'];
			$grid->label = $row['grid_label'];
			$grid->tableName = $row['grid_table'];
			$grid->locationColumn = $row['location_column'];
			$grid->aggregationLayerName = $row['aggregation_layer_name'];
			$result[] = $grid;
		}

		return $result;
	}

	/**
	 * Get the detail of one grid.
	 *
	 * @param String the grid name
	 * @return Grid
	 */
	public function getGrid($gridName) {
		$db = $this->getAdapter();

		Zend_Registry::get("logger")->debug('getGrid $gridName : '.$gridName);

		$req = " SELECT * "." FROM grid_definition "." WHERE grid_name = ? ";

		Zend_Registry::get("logger")->info('getGrid : '.$req);

		$select = $db->prepare($req);
		$select->execute(array($gridName));

		$row = $select->fetch();
		$grid = new Grid();
		$grid->name = $row['grid_name'];
		$grid->label = $row['grid_label'];
		$grid->tableName = $row['grid_table'];
		$grid->locationColumn = $row['location_column'];
		$grid->aggregationLayerName = $row['aggregation_layer_name'];

		return $grid;

	}
}
