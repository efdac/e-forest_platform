<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once 'mapping/Center.php';

/**
 * This is the model for managing countries bounding boxes.
 * @package models
 */
class Model_BoundingBox extends Zend_Db_Table_Abstract {

	var $logger;

	/**
	 * Initialisation
	 */
	public function init() {

		// Initialise the logger
		$this->logger = Zend_Registry::get("logger");
	}

	/**
	 * Get the center and defaut zoom level of the map for the country.
	 *
	 * @param String $codeCountry the code of the country or 999 for europe
	 * @return Center the center
	 */
	public function getCenter($codeCountry) {
		$db = $this->getAdapter();

		if (empty($codeCountry)) {
            $configuration = Zend_Registry::get("configuration");
            $codeCountry = $configuration->defaultCodeCountry;
		}

		$req = " SELECT (bb_xmin + bb_xmax) / 2 as x_center, (bb_ymin + bb_ymax) / 2 as y_center, zoom_level
			   	 FROM bounding_box
			   	 WHERE code_country = ?";

		Zend_Registry::get("logger")->info('getCenter : '.$req);

		$select = $db->prepare($req);
		$select->execute(array($codeCountry));

		$row = $select->fetch();
		$center = new Center();
		$center->x_center = $row['x_center'];
		$center->y_center = $row['y_center'];
		$center->defaultzoom = $row['zoom_level'];

		return $center;
	}

	/**
	 * Get NUTS code of a country.
	 *
	 * @param String $codeCountry the code of the country or 999 for europe
	 * @return String the NUTS code
	 */
	public function getNUTSCode($codeCountry) {
		$db = $this->getAdapter();

		$req = " SELECT nuts_code
			   	 FROM bounding_box
			   	 WHERE code_country = ?";

		Zend_Registry::get("logger")->info('getNUTSCode for '.$codeCountry.' : '.$req);

		$select = $db->prepare($req);
		$select->execute(array($codeCountry));

		$row = $select->fetch();

		return $row['nuts_code'];
	}

}
