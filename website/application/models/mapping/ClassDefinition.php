<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once 'mapping/ClassItem.php';

/**
 * This is the model for managing SLD classes.
 * @package models
 */
class Model_ClassDefinition extends Zend_Db_Table_Abstract {

	var $logger;

	/**
	 * Initialisation
	 */
	public function init() {

		// Initialise the logger
		$this->logger = Zend_Registry::get("logger");
	}

	/**
	 * Get the definition of a data classes for a vector layer.
	 *
	 * @param String $data the logical name of the data
	 * @return Array[ClassItem] The classes
	 */
	public function getClassDefinition($data) {
		$db = $this->getAdapter();

		
		$req = " SELECT *
			   	 FROM class_definition
			   	 WHERE data = ?
			   	 ORDER BY max_value";

		Zend_Registry::get("logger")->info('getClassDefinition : '.$req);

		$select = $db->prepare($req);
		$select->execute(array($data));

		$result = array();
		foreach ($select->fetchAll() as $row) {
			$class = new ClassItem();
			$class->minValue = $row['min_value'];
			$class->maxValue = $row['max_value'];
			$class->color = $row['color'];
			$class->label = $row['label'];
			$result[] = $class;
		}

		return $result;
	}

	/**
	 * Get the definition of a data classes for a raster layer.
	 * 
	 * Warning : There is no min_value, mapserv use the previous value. 
	 *
	 * @param String $data the logical name of the data
	 * @return Array[ClassItem] The classes
	 */
	public function getRasterClassDefinition($data) {
		$db = $this->getAdapter();

		
		$req = " SELECT *
			   	 FROM raster_class_definition
			   	 WHERE data = ?
			   	 ORDER BY value";

		Zend_Registry::get("logger")->info('getRasterClassDefinition : '.$req);

		$select = $db->prepare($req);
		$select->execute(array($data));

		$result = array();
		foreach ($select->fetchAll() as $row) {
			$class = new ClassItem();
			$class->maxValue = $row['value'];
			$class->color = $row['color'];
			$class->label = $row['label'];
			$result[] = $class;
		}

		return $result;
	}
	
	

}
