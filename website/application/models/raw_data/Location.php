<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




/**
 * This is a model allowing access to the locations.
 * @package models
 */
class Model_Location extends Zend_Db_Table_Abstract {

	var $logger;

	/**
	 * Initialisation
	 */
	public function init() {

		// Initialise the logger
		$this->logger = Zend_Registry::get("logger");
	}

	/**
	 * Get the number of plot locations per country.
	 *
	 * @return Array[countryCode => count]
	 */
	public function getLocationsPerCountry() {
		$db = $this->getAdapter();
		$req = " SELECT code, label, nb_locations "." FROM mode "." LEFT JOIN ( "."     SELECT country_code, count(*) as nb_locations "."     FROM  location "."     GROUP BY country_code "." ) as l ON (mode.code = l.country_code) "." WHERE unit = 'COUNTRY_CODE' "." ORDER BY position";

		$select = $db->prepare($req);

		$select->execute(array());

		Zend_Registry::get("logger")->info('getLocationsPerCountry : '.$req);

		$result = array();
		foreach ($select->fetchAll() as $row) {
			$result[$row['code']] = $row['nb_locations'];
		}

		return $result;

	}

}
