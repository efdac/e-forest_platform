<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/





/**
 * Represent a user.
 *
 * @package classes
 */
class User {

	/**
	 * The user login
	 */
	var $login;

	/**
	 * The user name
	 */
	var $username;

	/**
	 * The user password
	 */
	var $password;

	/**
	 * The country code (ex: "1" for France).
	 */
	var $countryCode;

	/**
	 * The country label (ex: France).
	 */
	var $countryLabel;

	/**
	 * Indicate if the user is active (1 for true, 0 for false)
	 */
	var $active;

	/**
	 * The user email adress.
	 */
	var $email;

	/**
	 * The code of the role.
	 */
	var $roleCode;

	/**
	 * The name of the role (used in the "user list" view").
	 */
	var $roleLabel;

}
