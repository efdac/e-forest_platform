<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once APPLICATION_PATH.'/classes/website/PredefinedField.php';


/**
 * Represent a predefined request.
 *
 * @package classes
 */
class PredefinedRequest {

	/**
	 * The request name.
	 */
	var $requestName;

	/**
	 * The request definition.
	 */
	var $definition;

	/**
	 * The request label.
	 */
	var $label;

	/**
	 * The date of creation of the request.
	 */
	var $date;

	/**
	 * The schema where the predefined request is executed (RAW or HARMONIZED).
	 */
	var $schemaCode;

	/**
	 * The dataset identifier.
	 */
	var $datasetID;

	/**
	 * The position of the request in its group.
	 */
	var $position;

	/**
	 * The logical name of the group of requests.
	 */
	var $groupName;

	/**
	 * The label of the group of requests.
	 */
	var $groupLabel;

	/**
	 * The position of the group of requests.
	 */
	var $groupPosition;

	/**
	 * The label of the dataset.
	 */
	var $datasetLabel;

	/**
	 * The list of result columns (array[PredefinedField]).
	 */
	var $resultsList = array();

	/**
	 * The list of criteria columns (array[PredefinedField]).
	 */
	var $criteriaList = array();

	/**
	 * Return a JSON representation of the object.
	 * This should correspond to the definition of the grid reader in PredefinedRequestPanel.js
	 *
	 * @return String
	 */
	function toJSON() {
		$json = '[';
		$json .= json_encode($this->requestName);
		$json .= ','.json_encode($this->label);
		$json .= ','.json_encode($this->definition);
		$json .= ','; // click
		$json .= ','.json_encode($this->date);
		$json .= ','; // criteria_hint
		$json .= ','.json_encode($this->position);
		$json .= ','.json_encode($this->groupName);
		$json .= ','.json_encode($this->groupLabel);
		$json .= ','.json_encode($this->groupPosition);
		$json .= ','.json_encode($this->datasetID);
		$json .= ']';

		return $json;
	}

}
