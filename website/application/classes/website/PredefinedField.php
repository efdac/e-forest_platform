<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/





/**
 * Represent a Predefined Field.
 *
 * @package classes
 */
class PredefinedField {

	/**
	 * The field identifier (data)
	 */
	var $data;

	/**
	 * The source format
	 */
	var $format;

	/**
	 * The value of the field
	 */
	var $value;

	/**
	 * Indicate if the value is fixed ("1") or if the user can select it ("0").
	 */
	var $fixed;

	/**
	 * The input type (SELECT, CHECKBOX, ...).
	 */
	var $inputType;

	/**
	 * The type of the data (CODE, STRING, RANGE, ...)
	 */
	var $type;

	/**
	 * The label.
	 */
	var $label;

	/**
	 * The definition.
	 */
	var $definition;

	/**
	 * Return a JSON representation of the object.
	 * This should correspond to the definition of the grid reader in PredefinedRequestPanel.js
	 *
	 * @return String
	 */
	function toJSON() {

		$json = json_encode($this->format."_".$this->data); // name
		$json .= ','.json_encode($this->format);
		$json .= ','.json_encode($this->data);
		$json .= ','.json_encode($this->value);
		$json .= ','.json_encode($this->fixed);
		$json .= ','.json_encode($this->inputType);
		$json .= ','.json_encode($this->type);
		$json .= ','.json_encode($this->label);
		$json .= ','.json_encode($this->definition);
				
		return $json;
	}

}
