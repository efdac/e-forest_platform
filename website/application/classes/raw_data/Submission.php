<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once 'raw_data/SubmissionFile.php';

/**
 * Represent a submission.
 *
 * A submission is a batch of files that is send to the E-forest server in order to be stored in database.
 *
 * @package classes
 */
class Submission {

	/**
	 * The submission identifier
	 */
	var $submissionId;

	/**
	 * The country code
	 */
	var $countryCode;

	/**
	 * The submission step
	 */
	var $step;

	/**
	 * The submission status
	 */
	var $status;

	/**
	 * The submission type
	 */
	var $type;	
		
	/**
	 * The submission date
	 */
	var $date;

	/**
	 * The files of the submission (array of SubmissionFile);
	 */
	var $files = array();

	/**
	 * Add a file to the list.
	 *
	 * @param SubmissionFile
	 */
	public function addFile($subFile) {
		$this->files[] = $subFile;
	}

}
