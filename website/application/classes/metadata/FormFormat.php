<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/





/**
 * Represent a Form Format (a logical bloc of the HTML Query page).
 * @package classes
 */
class FormFormat {

	/**
	 * The form identifier 
	 */
	var $format;
	
	/**
	 * The label of the form
	 */
	var $label;
	
	
	/**
	 * The definition of the form
	 */
	var $definition;
	
	/**
	 * Serialize the object as a JSON string
	 * 
	 * @return a JSON string
	 */
	public function toJSON() {
		return 'id:'.json_encode($this->format).',label:'.json_encode($this->label);
	}

}