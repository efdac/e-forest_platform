<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once 'metadata/Field.php';

/**
 * Represent a Field of a Database.
 * @package classes
 */
class TableField extends Field {


	/**
	 * The physical name of the column.
	 */
	var $columnName;

	/**
	 * The value of the field (OPTIONAL, only used for criterias).
	 */
	var $value;

	/**
	 * The name of the corresponding form field (used for column-oriented data).
	 */
	var $sourceFieldName;

	/**
	 * The name of the corresponding form (used for column-oriented data).
	 */
	var $sourceFormName;

	/**
	 * Link the field to its source table (used when building the SQL request).
	 * refer to a TableTreeData object.
	 */
	var $sourceTable;


}