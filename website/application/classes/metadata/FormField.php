<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once 'metadata/Field.php';

/**
 * Represent a Field of a Form.
 * @package classes
 */
class FormField extends Field {

	/**
	 * The input type of the field (SELECT, TEXT, ...)
	 */
	var $inputType;

	/**
	 * True if the field is a default criteria
	 */
	var $isDefaultCriteria;

	/**
	 * True if the field is a default result
	 */
	var $isDefaultResult;

	/**
     * default value for the criteria
     */
    var $defaultValue;

	/**
	 * Serialize the object as a JSON string
	 *
	 * @return JSON the form field descriptor
	 */
	public function toJSON() {
		$return = 'name:'.json_encode($this->format.'__'.$this->data).
                ',data:'.json_encode($this->data).
                ',format:'.json_encode($this->format).
                ',label:'.json_encode($this->label).
                ',inputType:'.json_encode($this->inputType).
                ',unit:'.json_encode($this->unit).
                ',type:'.json_encode($this->type).
                ',definition:'.json_encode($this->definition);
		return $return;
	}

	/**
	 * Serialize the criteria as a JSON string
	 *
	 * @return JSON the criteria field descriptor
	 */
	public function toCriteriaJSON() {
		$return = 'a:'.json_encode($this->format.'__'.$this->data).
                ',b:'.json_encode($this->label).
                ',c:'.json_encode($this->inputType).
                ',d:'.json_encode($this->type).
                ',e:'.json_encode($this->definition).
                ',f:'.$this->isDefaultCriteria.
                ',g:'.json_encode($this->defaultValue);
		return $return;
	}

	/**
	 * Serialize the result as a JSON string
	 *
	 * @return JSON the result field descriptor
	 */
	public function toResultJSON() {
		$return = 'a:'.json_encode($this->format.'__'.$this->data).
                ',b:'.json_encode($this->label).
                ',c:'.json_encode($this->definition).
                ',d:'.$this->isDefaultResult;
		return $return;
	}
}