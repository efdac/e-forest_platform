<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once 'metadata/Field.php';

/**
 * Represent a Field of a File.
 * @package classes
 */
class FileField extends Field {
    
	/**
     * Indicate if the field is mandatory
     * 1 for true
     * 0 for false
     */
    var $isMandatory;
    
    /**
     * The mask of the field
     */
    var $mask;
    
    /**
     * Serialize the object as a JSON string
     * 
     * @return a JSON string
     */
    public function toJSON() {
        return 'name:'.json_encode($this->format.'__'.$this->data).
                ',format:'.json_encode($this->format).
                ',label:'.json_encode($this->label).
                ',isMandatory:'.json_encode($this->isMandatory).
                ',definition:'.json_encode($this->definition).
                ',mask:'.json_encode($this->mask);
    }
}