<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/





/**
 * Represent a request format (an input file descriptor).
 * @package classes
 */
class RequestFormat {

	/**
	 * The type of the file
	 */
	var $fileType;
	
	/**
	 * The file identifier 
	 */
	var $format;
	
	/**
	 * The description of the file
	 */
	var $label;
	
	/**
	 * The path of the file (used during data upload) 
	 */
	var $filePath;

}