<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/





/**
 * Represent a map legend item.
 *
 * @package classes
 */
class LegendItem {

	/**
	 * The identifier of the item.
	 */
	var $itemId;

	/**
	 * The identifier of the parent of the item.
	 */
	var $parentId;

	/**
	 * Defines if the item is a layer (a leaf) or a node.
	 */
	var $isLayer;

	/**
	 * Defines if the item is checked by default.
	 */
	var $isChecked;

	/**
	 * Defines if the node is expended by default.
	 */
	var $isExpended;

	/**
	 * The label of the item.
	 */
	var $label;

	/**
	 * The logical name of the layer in OpenLayers.
	 */
	var $layerName;

	/**
	 * Defines if the item is hidden by default (value = 1)
	 */
	var $isHidden;

	/**
	 * Defines if the item is disabled (grayed) by default (value = 1)
	 */
	var $isDisabled;

}
