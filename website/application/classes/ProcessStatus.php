<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/





/**
 * Represent the status of a process (integration or harmonization service).
 *
 * @package classes
 */
class ProcessStatus {

	/**
	 * The status.
	 */
	var $status;

	/**
	 * The name of the current task.
	 */
	var $taskName;

	/**
	 * The position of the process in the current task.
	 */
	var $currentCount;

	/**
	 * The total count of items in the current task.
	 */
	var $totalCount;

}
