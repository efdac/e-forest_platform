<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once 'AbstractQueryController.php';

/**
 * QueryController is the controller that manages database query module.
 * @package controllers
 */
class QueryController extends AbstractQueryController {

	protected $schema = "RAW_DATA";

	/**
	 * Initialise the controler
	 */
	public function init() {
		parent::init();

		// Set the current module name
		$websiteSession = new Zend_Session_Namespace('website');
		$websiteSession->module = "query";
		$websiteSession->moduleLabel = "Query Data";
		$websiteSession->moduleURL = "query";

		$configuration = Zend_Registry::get("configuration");
		$this->visualisationSRS = $configuration->srs_visualisation;
		$this->databaseSRS = $configuration->srs_raw_data;
		$this->detailsLayers = $configuration->query_details_layers->toArray();
		
		// Init the activated layers
		$mappingSession = new Zend_Session_Namespace('mapping');
		$mappingSession->activatedLayers[] = 'all_locations';
		$mappingSession->activatedLayers[] = 'all_locations_country';

	}

	/**
	 * Check if the authorization is valid this controler.
	 */
	function preDispatch() {

		parent::preDispatch();

		$userSession = new Zend_Session_Namespace('user');
		$permissions = $userSession->permissions;
		if (empty($permissions) || !array_key_exists('DATA_QUERY', $permissions)) {
			$this->_redirector->gotoUrl('/');
		}
	}

	/**
	 * Return the logical name of the location table (the table containing the the_geom column).
	 *
	 * @return String the name of the table.
	 */
	protected function getLocationTable() {
		return "LOCATION_DATA";
	}

   /**
     * Return the logical name of the plot table (the table containing the plot data).
     *
     * @return String the plot table
     */
    protected function getPlotTable() {
        return "PLOT_DATA";
    }
}