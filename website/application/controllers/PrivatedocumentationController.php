<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

/**
 * Licensed under EUPL v1.1 (see http://ec.europa.eu/idabc/eupl).
 */
require_once 'AbstractEforestController.php';

/**
 * PrivateDocumentationController is the controller that manages the documentation only accessible to the framework members.
 * @package controllers
 */
class PrivateDocumentationController extends AbstractEforestController {

	protected $_redirector = null;

	/**
	 * Initialise the controler
	 */
	public function init() {
		parent::init();

		// Set the current module name
		$websiteSession = new Zend_Session_Namespace('website');
		$websiteSession->module = "privatedocumentation";
		$websiteSession->moduleLabel = "Private Documentation";
		$websiteSession->moduleURL = "privatedocumentation";

		// Load the redirector helper
		$this->_redirector = $this->_helper->getHelper('Redirector');

		// Initialise the models

	}

	/**
	 * Check if the authorization is valid this controler.
	 */
	function preDispatch() {

		parent::preDispatch();

		$userSession = new Zend_Session_Namespace('user');
		$permissions = $userSession->permissions;
		if (empty($permissions) || !array_key_exists('PRIVATE_DOCUMENTATION', $permissions)) {
			$this->_redirector->gotoUrl('/');
		}
	}

	/**
	 * The "index" action is the default action for all controllers.
	 */
	public function indexAction() {
		$this->logger->debug('Private Documentation index');

		$this->render('index');
	}

	/**
	 * The documents action.
	 */
	public function documentsAction() {
		$this->logger->debug('documents');

		$this->render('documents');
	}

	/**
	 * The fileformat action.
	 */
	public function fileformatAction() {
		$this->logger->debug('fileformat');

		$this->render('fileformat');
	}

	/**
	 * The manuals action.
	 */
	public function manualsAction() {
		$this->logger->debug('manuals');

		$this->render('manuals');
	}

	/**
	 * The faq action.
	 */
	public function faqAction() {
		$this->logger->debug('faq');

		$this->render('faq');
	}

	/**
	 * The meetings action.
	 */
	public function meetingsAction() {
		$this->logger->debug('meetings');

		$this->render('meetings');
	}

	/**
	 * The forestmap action.
	 */
	public function forestmapAction() {
		$this->logger->debug('forestmap');

		$this->render('forestmap');
	}
}
