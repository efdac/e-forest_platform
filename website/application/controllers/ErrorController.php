<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once 'AbstractEforestController.php';

/**
 * ErrorController
 * @package controllers
 */
class ErrorController extends AbstractEforestController {

	/**
	 * Initialise the controler
	 */
	public function init() {
		parent::init();

	}

	/**
	 * errorAction() is the action that will be called by the "ErrorHandler"
	 * plugin.  When an error/exception has been encountered
	 * in a ZF MVC application (assuming the ErrorHandler has not been disabled
	 * in your bootstrap) - the Errorhandler will set the next dispatchable
	 * action to come here.  This is the "default" module, "error" controller,
	 * specifically, the "error" action.  These options are configurable.
	 *
	 * @see http://framework.zend.com/manual/en/zend.controller.plugins.html
	 *
	 * @return void
	 */
	public function errorAction() {
		// Ensure the default view suffix is used so we always return good
		// content
		$this->_helper->viewRenderer->setViewSuffix('phtml');

		// Grab the error object from the request
		$errors = $this->_getParam('error_handler');

		// $errors will be an object set as a parameter of the request object,
		// type is a property
		switch ($errors->type) {
		case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
		case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

			// 404 error -- controller or action not found
			$this->getResponse()->setHttpResponseCode(404);
			$this->view->message = 'Page not found';
			break;
		default:
			// application error
			$this->getResponse()->setHttpResponseCode(500);
			$this->view->message = 'Application error';
			break;
		}

		// pass the environment to the view script so we can conditionally
		// display more/less information
		$this->view->env = $this->getInvokeArg('env');

		// pass the actual exception object to the view
		$this->view->exception = $errors->exception;
		$this->logger->err('Error : '.$errors->exception);

		// pass the request to the view
		$this->view->request = $errors->request;
	}
}
