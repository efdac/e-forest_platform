<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/





/**
 * IndexController is the default controller for this application
 *
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 * @package controllers
 */
class IndexController extends Zend_Controller_Action {

	/**
	 * Initialise the controler
	 */
	public function init() {
		parent::init();

		// Set the current module name
		$websiteSession = new Zend_Session_Namespace('website');
		$websiteSession->module = "";
	}

	/**
	 * The "index" action is the default action for all controllers. This
	 * will be the landing page of your application.
	 *
	 * Assuming the default route and default router, this action is dispatched
	 * via the following urls:
	 *   /
	 *   /index/
	 *   /index/index
	 *
	 * @return void
	 */
	public function indexAction() {
		/*
		 There is nothing inside this action, but it will still attempt to
		 render a view.  This is because by default, the front controller
		 uses the ViewRenderer action helper to handle auto rendering
		 In the MVC grand scheme of things, the ViewRenderer allows us to
		 draw the line between the C and V in the MVC.  Also note this action
		 helper is optional, but on default.
		 */
	}

	/**
	 * Show the welcome page for english users.
	 *
	 * @return void
	 */
	public function welcomeAction() {
		$this->render('welcome');
	}

	/**
	 * Show the welcome page for english users.
	 *
	 * @return void
	 */
	public function welcomeFrAction() {
		$this->render('welcome-fr');
	}
}
