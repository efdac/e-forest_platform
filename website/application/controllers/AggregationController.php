<?php
/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/




require_once 'AbstractAnalysisController.php';
require_once APPLICATION_PATH.'/models/metadata/Metadata.php';
require_once APPLICATION_PATH.'/models/mapping/Grids.php';
require_once APPLICATION_PATH.'/models/aggregation/Aggregation.php';
require_once APPLICATION_PATH.'/models/calculation_service/CalculationService.php';

define('OPERATION_LEVEL', 0);
define('DATASET_LEVEL', 1);
define('VARIABLE_LEVEL', 2);
define('CELL_LEVEL', 3);
define('FILTER_LEVEL', 4);


/**
 * AggregationController is the controller that manages the data aggregation process.
 * @package controllers
 */
class AggregationController extends AbstractAnalysisController {

	protected $_redirector = null;

	protected $operations = array();


	/**
	 * Initialise the controler
	 */
	public function init() {
		parent::init();

		// Set the current module name
		$websiteSession = new Zend_Session_Namespace('website');
		$websiteSession->module = "aggregation";
		$websiteSession->moduleLabel = "Data Aggregation";
		$websiteSession->moduleURL = "aggregation";

		// Load the redirector helper
		$this->_redirector = $this->_helper->getHelper('Redirector');

		// Initialise the model
		$this->metadataModel = new Model_Metadata();
		$this->gridsModel = new Model_Grids();
		$this->aggregationModel = new Model_Aggregation();
		$this->calculationServiceModel = new Model_CalculationService();

		// Initialise default values
		$this->operations['RATIO'] = 'Ratio';
		$this->operations['MEAN'] = 'Mean and total';
	}

	/**
	 * Check if the authorization is valid this controler.
	 */
	function preDispatch() {

		parent::preDispatch();
	}

	/**
	 * The "index" action is the default action for all controllers.
	 */
	public function indexAction() {
		$this->logger->debug('Data aggregation index');

		$userSession = new Zend_Session_Namespace('user');
		$permissions = $userSession->permissions;
		if (empty($permissions) || !array_key_exists('DATA_AGGREGATION', $permissions)) {
			$this->_redirector->gotoUrl('/');
		}

		$this->_resetSession();

		$this->showOperationsAction();
	}


	/**
	 * Reset the session parameters.
	 *
	 * @param Integer $level
	 */
	private function _resetSession($level = 0) {

		$aggregationSession = new Zend_Session_Namespace('aggregation');
		if ($level <= OPERATION_LEVEL) {
			$aggregationSession->__unset('operation');
		}

		if ($level <= DATASET_LEVEL) {
			$aggregationSession->__unset('datasetId');
		}
		if ($level <= VARIABLE_LEVEL) {
			$aggregationSession->__unset('variable'); // the selected variable (TableField)
			$aggregationSession->__unset('numerator');
			$aggregationSession->__unset('denominator');
		}
		if ($level <= CELL_LEVEL) {
			$aggregationSession->__unset('cell'); // the selected cell (name)
		}
		if ($level <= FILTER_LEVEL) {
			$aggregationSession->__unset('filters'); // filter (key + value)
			$aggregationSession->__unset('selectedFilters'); // names of the selected filters
		}

		// Recalculate the filter linked to the variable
		if ($level >= VARIABLE_LEVEL && $level <= FILTER_LEVEL) {
			$this->_addDomainFilter($aggregationSession->variable);
			$this->_addDomainFilter($aggregationSession->numerator);
			$this->_addDomainFilter($aggregationSession->denominator);
		}

	}

	/**
	 * Build and return the operation selection form.
	 */
	private function _getOperationForm() {

		$form = new Zend_Form();
		$form->setAction($this->baseUrl.'/aggregation/validate-operation');
		$form->setMethod('post');

		//
		// Add the dataset element
		//
		$operationElement = $form->createElement('select', 'OPERATION');
		$operationElement->setLabel('Operation');
		$operationElement->setRequired(true);

		$operationElement->addMultiOptions($this->operations);

		//
		// Add the submit element
		//
		$submitElement = $form->createElement('submit', 'submit');
		$submitElement->setLabel('Select');

		// Add elements to form:
		$form->addElement($operationElement);
		$form->addElement($submitElement);

		return $form;
	}


	/**
	 * Build and return the dataset selection form.
	 */
	private function _getDatasetForm() {

		$form = new Zend_Form();
		$form->setAction($this->baseUrl.'/aggregation/validate-dataset');
		$form->setMethod('post');

		//
		// Add the dataset element
		//
		$datasetElement = $form->createElement('select', 'DATASET_ID');
		$datasetElement->setLabel('Dataset');
		$datasetElement->setRequired(true);

		// TODO : Hardcoded value : Limited to the species dataset
		$datasets = array();
		$datasets['WP3_REQUEST'] = 'Basal Area by Species';

		//$requests = $this->metadataModel->getDatasets();

		$datasetElement->addMultiOptions($datasets);

		//
		// Add the submit element
		//
		$submitElement = $form->createElement('submit', 'submit');
		$submitElement->setLabel('Select');

		// Add elements to form:
		$form->addElement($datasetElement);
		$form->addElement($submitElement);

		return $form;
	}

	/**
	 * Build and return the filters form.
	 */
	private function _getFiltersForm($availableFilters) {

		$form = new Zend_Form();
		$form->setAction($this->baseUrl.'/aggregation/validate-filters');
		$form->setMethod('post');

		//
		// Add the submit element
		//
		$submitElement = $form->createElement('submit', 'submit');
		$submitElement->setLabel('Select');

		// HardCoded Value
		$speciesClass = new Field();
		$speciesClass->data = 'SPECIES_CLASS';
		$speciesClass->format = 'SPECIES_DATA';
		$speciesClass->label = 'Species class (Coniferous or Broadleaves)';
		$speciesClass->unit = 'SPECIES_CLASS';
		$speciesClass->type = 'CODE';

		$availableFilters[] = $speciesClass;

		// Add the filters
		foreach ($availableFilters as $availableFilter) {

			if (strpos($availableFilter->data, 'DOMAIN') === false) {
				$filterElement = $form->createElement('checkbox', $availableFilter->format.'__'.$availableFilter->data);
				$filterElement->setLabel($availableFilter->label);
				//$filterElement->setDescription($availableFilter->definition);
				$form->addElement($filterElement);
			}
		}



		// Add elements to form:
		$form->addElement($submitElement);

		return $form;
	}


	/**
	 * Build and return the filter values form.
	 */
	private function _getFilterValuesForm() {

		$form = new Zend_Form();
		$form->setAction($this->baseUrl.'/aggregation/validate-filters-values');
		$form->setMethod('post');

		// Get back parameters from the session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$selectedFilters = $aggregationSession->selectedFilters;

		// Add the filters values
		foreach ($selectedFilters as $selectedFilter) {

			// Split the variable name and format
			$split = explode("__", $selectedFilter);
			$filterFormat = $split[0];
			$filterData = $split[1];

			// HardCoded Value
			if ($filterData == 'SPECIES_CLASS') {
				$filterElement = $form->createElement('select', $selectedFilter);
				$filterElement->setLabel('Species class');
				$filterElement->setRequired(true);
				$values = $this->metadataModel->getModeFromUnit('SPECIES_CLASS');
				$filterElement->addMultiOptions($values);
			} else {

				// Get the field object
				$filter = $this->metadataModel->getTableField($filterFormat, $filterData);

				if ($filter->type == 'CODE') {
					$filterElement = $form->createElement('select', $selectedFilter);
					$filterElement->setLabel($filter->label);
					$filterElement->setRequired(true);
					$values = $this->metadataModel->getModeFromUnit($filter->unit);
					$filterElement->addMultiOptions($values);

				} else if ($filter->type == 'BOOLEAN') {
					$filterElement = $form->createElement('select', $selectedFilter);
					$filterElement->setLabel($filter->label);
					$filterElement->setRequired(true);
					$options = array("0" => "False", "1" => "True");
					$filterElement->addMultiOptions($options);
				}
			}
			// Get the list of available values for the filter
			$form->addElement($filterElement);

		}

		//
		// Add the submit element
		//
		$submitElement = $form->createElement('submit', 'submit');
		$submitElement->setLabel('Launch Calculation');

		// Add elements to form:
		$form->addElement($submitElement);

		return $form;
	}

	/**
	 * Build and return the cell selection form.
	 */
	private function _getCellsForm() {

		$form = new Zend_Form();
		$form->setAction($this->baseUrl.'/aggregation/validate-cell');

		$form->setMethod('post');

		//
		// Add the dataset element
		//
		$cellsElement = $form->createElement('select', 'CELL');
		$cellsElement->setLabel('Geographical domain');
		$cellsElement->setRequired(true);

		// Get the list of grids
		$grids = $this->gridsModel->getGrids();
		$gridsList = array();
		foreach ($grids as $grid) {
			$gridsList[$grid->name] = $grid->label;
		}

		$cellsElement->addMultiOptions($gridsList);

		//
		// Add the submit element
		//
		$submitElement = $form->createElement('submit', 'submit');
		$submitElement->setLabel('Select');

		// Add elements to form:
		$form->addElement($cellsElement);
		$form->addElement($submitElement);

		return $form;
	}

	/**
	 * Build and return the variable selection form.
	 */
	private function _getVariablesForm($variables) {

		$form = new Zend_Form();
		$form->setAction($this->baseUrl.'/aggregation/validate-variable');
		$form->setMethod('post');

		//
		// Add the dataset element
		//
		$variableElement = $form->createElement('select', 'VARIABLE');
		$variableElement->setLabel('Variable');
		$variableElement->setRequired(true);
		$variableElement->addMultiOptions($variables);

		//
		// Add the submit element
		//
		$submitElement = $form->createElement('submit', 'submit');
		$submitElement->setLabel('Select');

		// Add elements to form:
		$form->addElement($variableElement);
		$form->addElement($submitElement);

		return $form;
	}

	/**
	 * Build and return the variable selection form.
	 */
	private function _getVariablesRatioForm($variables) {

		$form = new Zend_Form();
		$form->setAction($this->baseUrl.'/aggregation/validate-variables-ratio');
		$form->setMethod('post');

		//
		// Add the numerator element
		//
		$numeratorElement = $form->createElement('select', 'NUMERATOR');
		$numeratorElement->setLabel('Numerator');
		$numeratorElement->setRequired(true);
		$numeratorElement->addMultiOptions($variables);

		//
		// Add the denominator element
		//
		$denominatorElement = $form->createElement('select', 'DENOMINATOR');
		$denominatorElement->setLabel('Denominator');
		$denominatorElement->setRequired(true);

		// Add hardcoded values for domains
		$variablesDenom = array();
		$variablesDenom["1"] = "Identity";
		$variablesDenom['PLOT_DATA__DOMAIN_FOREST'] = 'Domain forest';
		$variablesDenom['PLOT_DATA__DOMAIN_BASAL_AREA'] = 'Domain basal area';
		$denominatorElement->addMultiOptions($variablesDenom);

		//
		// Add the submit element
		//
		$submitElement = $form->createElement('submit', 'submit');
		$submitElement->setLabel('Select');

		// Add elements to form:
		$form->addElement($numeratorElement);
		$form->addElement($denominatorElement);
		$form->addElement($submitElement);

		return $form;
	}

	/**
	 * Display the available calculation operation.
	 *
	 * @return a View
	 */
	public function showOperationsAction() {
		$this->logger->debug('showOperationsAction');

		$this->view->form = $this->_getOperationForm();

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		$this->render('show-operations');
	}



	/**
	 * Display the available datasets.
	 *
	 * @return a View
	 */
	public function showDatasetsAction() {
		$this->logger->debug('showDatasetsAction');

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		$this->view->form = $this->_getDatasetForm();

		$this->render('show-datasets');
	}

	/**
	 * Store the selected dataset in session.
	 */
	public function validateDatasetAction() {
		$this->logger->debug('validateDatasetAction');


		// Check the validity of the POST
		if (!$this->getRequest()->isPost()) {
			$this->logger->debug('form is not a POST');
			return $this->_forward('index');
		}

		// Check the validity of the From
		$form = $this->_getDatasetForm();
		if (!$form->isValid($_POST)) {
			$this->logger->debug('form is not valid');
			$this->view->form = $form;
			return $this->render('show-datasets');
		}

		// Get the selected values
		$values = $form->getValues();
		$datasetId = $values['DATASET_ID'];

		// A bit of cleaning
		$this->_resetSession(DATASET_LEVEL);

		// Store the value in session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$aggregationSession->datasetId = $datasetId;

		if ($aggregationSession->operation == 'RATIO') {
			$this->showVariablesRatioAction();
		} else {
			$this->showVariablesAction();
		}
	}

	/**
	 * Store the selected operation in session.
	 */
	public function validateOperationAction() {
		$this->logger->debug('validateOperationAction');


		// Check the validity of the POST
		if (!$this->getRequest()->isPost()) {
			$this->logger->debug('form is not a POST');
			return $this->_forward('index');
		}

		// Check the validity of the From
		$form = $this->_getOperationForm();
		if (!$form->isValid($_POST)) {
			$this->logger->debug('form is not valid');
			$this->view->form = $form;
			return $this->render('show-operations');
		}

		// Get the selected values
		$values = $form->getValues();
		$operation = $values['OPERATION'];

		// A bit of cleaning
		$this->_resetSession(OPERATION_LEVEL);

		// Store the value in session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$aggregationSession->operation = $operation;


		$this->showDatasetsAction();
	}

	/**
	 * Get variables available for aggregration.
	 */
	private function _getAggregationVariables($datasetId) {

		// List the available tables in the raw_data schema
		// TODO : Calculate the leafTable as the lowest available table
		$ancestors = $this->metadataModel->getTablesTree('SPECIES_DATA', null, 'RAW_DATA');

		// List all the fields available for aggregation (for a given dataset and a list of tables)
		$values = $this->metadataModel->getQuantitativeFields($datasetId, $ancestors, 'RAW_DATA');

		return $values;
	}

	/**
	 * Display the available variables for aggregation.
	 *
	 * @return a View
	 */
	public function showVariablesAction() {
		$this->logger->debug('showVariablesAction');

		// Get the datasetId from session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$datasetId = $aggregationSession->datasetId;
			
		// Get the available variables
		$variables = $this->_getAggregationVariables($datasetId);

		$valuesList = array();
		foreach ($variables as $variable) {
			$valuesList[$variable->format.'__'.$variable->data] = $variable->label;
		}

		// Build the form
		$this->view->form = $this->_getVariablesForm($valuesList);

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		$this->render('show-variables');
	}

	/**
	 * Display the available variables for aggregation.
	 *
	 * @return a View
	 */
	public function showVariablesRatioAction() {
		$this->logger->debug('showVariablesRatioAction');

		// Get the datasetId from session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$datasetId = $aggregationSession->datasetId;
			
		// Get the available variables
		$variables = $this->_getAggregationVariables($datasetId);


		$valuesList = array();
		foreach ($variables as $variable) {
			$valuesList[$variable->format.'__'.$variable->data] = $variable->label;
		}

		// Build the form
		$this->view->form = $this->_getVariablesRatioForm($valuesList);

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		$this->render('show-variables-ratio');
	}

	/**
	 * Get the list of available filters fields.
	 *
	 * @param String $datasetId the dataset identifier
	 */
	private function _getAvailableFilters() {

		// Get back parameters from the session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$datasetId = $aggregationSession->datasetId;

		// Add the calculation variables to the list of already selected data
		$selectedFilters = array();
		$variable = $aggregationSession->variable;
		if ($variable!=null) {
			$selectedFilters[] = $variable->data;
		}
		$numerator = $aggregationSession->numerator;
		if ($numerator!=null) {
			$selectedFilters[] = $numerator->data;
		}
		$denominator = $aggregationSession->denominator;
		if ($denominator!=null) {
			$selectedFilters[] = $denominator->data;
		}

		// Get the filters
		$availableFilters = $this->metadataModel->getQualitativeFields($datasetId, 'RAW_DATA');

		// Eliminate doubles
		foreach ($availableFilters as $key => $filter) {
			if (in_array($filter->data, $selectedFilters)) {
				unset($availableFilters[$key]);
			} else {
				// store the name of the data
				$selectedFilters[] = $filter->data;
			}
		}

		return $availableFilters;

	}

	/**
	 * Display the available filters for aggregation.
	 *
	 * @return a View
	 */
	public function showFiltersAction() {
		$this->logger->debug('showFiltersAction');

		// Get the available filters
		$availableFilters = $this->_getAvailableFilters();

		// Build the form
		$this->view->form = $this->_getFiltersForm($availableFilters);

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		$this->render('show-filters');
	}

	/**
	 * Validate the filter selection.
	 *
	 * @return a View
	 */
	public function validateFiltersAction() {
		$this->logger->debug('validateFiltersAction');

		// Check the validity of the POST
		if (!$this->getRequest()->isPost()) {
			$this->logger->debug('form is not a POST');
			return $this->_forward('index');
		}

		// Get the available filters
		$availableFilters = $this->_getAvailableFilters();

		// Check the validity of the Form
		$form = $this->_getFiltersForm($availableFilters);
		if (!$form->isValid($_POST)) {
			$this->logger->debug('form is not valid');
			$this->view->form = $form;
			return $this->render('show-filters');
		}

		// Get the selected values
		$values = $form->getValues();

		// A bit of cleaning
		$this->_resetSession(FILTER_LEVEL);

		// Store the filters in session
		$selectedFilters = array();
		foreach ($values as $filterName => $value) {
			if ($value == 1) {
				$selectedFilters[] = $filterName;
			}
		}

		// Get back parameters from the session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$aggregationSession->selectedFilters = $selectedFilters;

		$this->showFiltersValuesAction();
	}

	/**
	 * Validate the filter selection.
	 *
	 * @return a View
	 */
	public function showFiltersValuesAction() {
		$this->logger->debug('showFiltersValuesAction');

		// Get back parameters from the session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$selectedFilters = $aggregationSession->selectedFilters;

		// Get the form corresponding to the selected filters
		$this->view->form = $this->_getFilterValuesForm();

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		$this->render('show-filters-values');
	}

	/**
	 * Launch the calculation.
	 *
	 * @return a View
	 */
	public function showLauchCalculationAction() {
		$this->logger->debug('showLauchCalculationAction');

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		$this->render('show-calculation');
	}

	/**
	 * Validate the filter selection.
	 *
	 * @return a View
	 */
	public function validateFiltersValuesAction() {
		$this->logger->debug('validateFiltersValuesAction');

		// Check the validity of the POST
		if (!$this->getRequest()->isPost()) {
			$this->logger->debug('form is not a POST');
			return $this->_forward('index');
		}

		// Check the validity of the Form
		$form = $this->_getFilterValuesForm();
		if (!$form->isValid($_POST)) {
			$this->logger->debug('form is not valid');
			$this->view->form = $form;
			return $this->render('show-filter-values');
		}

		// Get the selected values
		$values = $form->getValues();

		// Store the value in session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$filters = $aggregationSession->filters;
		foreach ($values as $key => $value) {
			$filters[$key] = $value;
		}
		$aggregationSession->filters = $filters;

		return $this->launchCalculationAction();
	}

	/**
	 * Launch the calculation with the variables in session.
	 *
	 * @return a View
	 */
	public function launchCalculationAction() {
		// Aggregate the data and store it in the temporary result table
		$this->_aggregateData();

		return $this->showResultsAction();
	}



	/**
	 * Display the results.
	 *
	 * @return a View
	 */
	public function showResultsAction() {
		$this->logger->debug('showResultsAction');

		// Get the session id
		$sessionId = session_id();

		$request = $this->getRequest();
		$errorType =  $request->errortype;

		// Get the status of the calculation
		$status = $this->calculationServiceModel->getStatus(session_id(), 'CalculationServlet');
		$this->view->status = $status->status;
		$this->view->taskName = $status->taskName;

		$this->logger->debug('status : '.$status->status);

		if ($status->status == "OK") {
				
			// Save the display type for the error
			if ($errorType != null) {
				// save the errorType in the session
				$aggregationSession = new Zend_Session_Namespace('aggregation');
				$aggregationSession->errorType = $errorType;
			}
				
			// Read the results
			$this->view->results = $this->aggregationModel->getAggregatedData($sessionId);
		}

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		$this->render('show-results');
	}


	/**
	 * Store the selected variable in session.
	 */
	public function validateVariableAction() {
		$this->logger->debug('validateVariableAction');


		// Check the validity of the POST
		if (!$this->getRequest()->isPost()) {
			$this->logger->debug('form is not a POST');
			return $this->_forward('index');
		}

		// Get the datasetId from session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$datasetId = $aggregationSession->datasetId;
			
		// Get the available variables
		$variables = $this->_getAggregationVariables($datasetId);

		// Check the validity of the From
		$form = $this->_getVariablesForm($variables);
		if (!$form->isValid($_POST)) {
			$this->logger->debug('form is not valid');
			$this->view->form = $form;
			return $this->render('show-variables');
		}

		// Get the selected variable name
		$values = $form->getValues();
		$variableName = $values['VARIABLE'];

		// Split the variable name and format
		$split = explode("__", $variableName);
		$variableFormat = $split[0];
		$variableData = $split[1];

		// Get the tableField object
		$variable = $this->metadataModel->getTableField($variableFormat, $variableData);
			
		// A bit of cleaning
		$this->_resetSession(VARIABLE_LEVEL);

		// Store the value in session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$aggregationSession->variable = $variable;

		//Get the DOMAIN filter corresponding to the selected value
		$this->_addDomainFilter($variable);

		$this->showCellsAction();
	}

	/**
	 * Add the filter on the domain corresponding to the selected variable.
	 * Store it in session
	 */
	private function _addDomainFilter($variable) {

		if ($variable != null && $variable->data != "1" && $variable->data != "DOMAIN_FOREST" && $variable->data != "DOMAIN_BASAL_AREA") {

			// Get the DOMAIN filter corresponding to the selected value
			$domainfilter = $this->metadataModel->getDomainMapping($variable->format, $variable->data);

			$aggregationSession = new Zend_Session_Namespace('aggregation');
			$filters = $aggregationSession->filters;
			$filters[$domainfilter->format.'__'.$domainfilter->data] = '1';
			$aggregationSession->filters = $filters;
		}

	}

	/**
	 * Store the selected variable in session.
	 */
	public function validateVariablesRatioAction() {
		$this->logger->debug('validateVariablesRatioAction');


		// Check the validity of the POST
		if (!$this->getRequest()->isPost()) {
			$this->logger->debug('form is not a POST');
			return $this->_forward('index');
		}

		// Get the datasetId from session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$datasetId = $aggregationSession->datasetId;
			
		// Get the available variables
		$variables = $this->_getAggregationVariables($datasetId);

		// Check the validity of the From
		$form = $this->_getVariablesRatioForm($variables);
		if (!$form->isValid($_POST)) {
			$this->logger->debug('form is not valid');
			$this->view->form = $form;
			return $this->render('show-variables');
		}

		// Get the selected variable name
		$values = $form->getValues();
		$numeratorName = $values['NUMERATOR'];
		$denominatorName = $values['DENOMINATOR'];

		// Split the variable name and format
		$split = explode("__", $numeratorName);
		$numeratorFormat = $split[0];
		$numeratorData = $split[1];
		$numerator = $this->metadataModel->getTableField($numeratorFormat, $numeratorData);

		if ($denominatorName == "1") {
			// Special case for identity
			$denominatorFormat = "1";
			$denominatorData = "1";
			$denominator = new TableField();
			$denominator->data = "1";
			$denominator->format = "1";
			$denominator->label = "Identity";
			$denominator->definition = "Identity";
			$denominator->unit = "Identity";
			$denominator->type = "NUMERIC";
			$denominator->columnName = "1";
		} else {
			$split = explode("__", $denominatorName);
			$denominatorFormat = $split[0];
			$denominatorData = $split[1];
			$denominator = $this->metadataModel->getTableField($denominatorFormat, $denominatorData);
		}


		// Store the value in session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$aggregationSession->numerator = $numerator;
		$aggregationSession->denominator = $denominator;

		// Get the DOMAIN filter corresponding to the selected values
		$this->_addDomainFilter($numerator);
		$this->_addDomainFilter($denominator);


		$this->showCellsAction();
	}

	/**
	 * Put in session some labels used in the summary.
	 *
	 */
	private function _prepareSummary() {
		$this->logger->debug('_prepareSummary');

		$aggregationSession = new Zend_Session_Namespace('aggregation');

		// operation
		$aggregationSession->operations = $this->operations;

		// dataset label
		$datasets = $this->metadataModel->getDatasets();
		$datasetIds = array();
		foreach ($datasets as $dataset) {
			$datasetIds[$dataset['id']] = $dataset['label'];
		}
		$aggregationSession->datasets = $datasetIds;

		// Cell
		$grids = $this->gridsModel->getGrids();
		$gridsList = array();
		foreach ($grids as $grid) {
			$gridsList[$grid->name] = $grid->label;
		}
		$aggregationSession->cells = $gridsList;

	}

	/**
	 * Display the aggregation cells.
	 *
	 * @return a View
	 */
	public function showCellsAction() {
		$this->logger->debug('showCellsAction');

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		$this->view->form = $this->_getCellsForm();

		$this->render('show-cells');
	}

	/**
	 * Store the selected cell in session.
	 */
	public function validateCellAction() {
		$this->logger->debug('validateCellAction');


		// Check the validity of the POST
		if (!$this->getRequest()->isPost()) {
			$this->logger->debug('form is not a POST');
			return $this->_forward('index');
		}

		// Check the validity of the From
		$form = $this->_getCellsForm();
		if (!$form->isValid($_POST)) {
			$this->logger->debug('form is not valid');
			$this->view->form = $form;
			return $this->render('show-variables');
		}

		// Get the selected variable name
		$values = $form->getValues();
		$cell = $values['CELL'];

		// A bit of cleaning
		$this->_resetSession(CELL_LEVEL);

		// Store the value in session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$aggregationSession->cell = $cell;

		$this->showFiltersAction();

	}

	/**
	 * AJAX function : Get the list of available variables.
	 *
	 * @return JSON The list of forms
	 */
	public function ajaxgetvariablesAction() {

		// Get back info from the user session
		$websiteSession = new Zend_Session_Namespace('website');
		$leafTable = $websiteSession->leafTable;

		// TODO : Remove the hardcoded value of datasetId
		$datasetId = 'WP3_REQUEST';

		// List all the fields available for aggregation (for a given dataset and a list of tables)
		$values = $this->_getAggregationVariables($datasetId);
		$valuesList = array();
		foreach ($values as $value) {
			$valuesList[] = array('name' => $value->format.'__'.$value->data, 'label' => $value->label);
		}

		echo '{'.'metaData:{'.'root:\'rows\','.'fields:['.'\'name\','.'\'label\''.']'.'},'.'rows:'.json_encode($valuesList).'}';

		// No View, we send directly the JSON
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}

	/**
	 * Validate the aggregation variables form.
	 */
	public function ajaxValidateAggregationVariableFormAction() {

		$this->logger->debug('ajaxValidateAggregationVariableFormAction');

		// Get the selected values
		$variable = $this->_getParam('AGGREGATE_VARIABLE');
		$gridName = $this->_getParam('GRID_NAME');

		// Split the variable name and format
		$split = explode("__", $variable);
		$variableFormat = $split[0];
		$variableData = $split[1];

		// Get the tableField object
		$variable = $this->metadataModel->getTableField($variableFormat, $variableData);

		// Get the DOMAIN filter corresponding to the selected value
		$domainfilter = $this->metadataModel->getDomainMapping($variable->format, $variable->data);


		// Store the values in session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$aggregationSession->datasetId = 'WP3_REQUEST'; // hardcoded
		$aggregationSession->variable = $variable;
		$aggregationSession->cell = $gridName;
		$filters = $aggregationSession->filters;
		$filters[$domainfilter->format.'__'.$domainfilter->data] = '1';
		$aggregationSession->filters = $filters;
			
		// Aggregate the data and store it in the temporary result table
		$this->_aggregateData();
			
		// Activate the layer
		$mappingSession = new Zend_Session_Namespace('mapping');
		echo '{success:true, layerName:\''.$mappingSession->aggregatedLayer.'\'}';

		// No View, we send directly the JSON
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}

	/**
	 * Get the parameters from the session and lauch the agregation service.
	 */
	private function _aggregateData() {
		$this->logger->debug('_aggregateData');

		// Get the session id
		$sessionId = session_id();

		// Get back parameters from the session
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$operation = $aggregationSession->operation;
		$datasetId = $aggregationSession->datasetId;
		$variable = $aggregationSession->variable;
		$numerator = $aggregationSession->numerator;
		$denominator = $aggregationSession->denominator;
		$gridName = $aggregationSession->cell;
		$filters = $aggregationSession->filters;

		// Call the calculation module
		if ($operation=='RATIO') {
			$this->calculationServiceModel->aggregateDataRatio($sessionId, $datasetId, $numerator, $denominator, $gridName, $filters);
		} else {
			$this->calculationServiceModel->aggregateData($sessionId, $datasetId, $variable, $gridName, $filters);
		}

		// Get some details about the Grid
		$grid = $this->gridsModel->getGrid($gridName);

		// Register the layer as being active
		$mappingSession = new Zend_Session_Namespace('mapping');
		$mappingSession->activatedLayers[] = $grid->aggregationLayerName;
		$mappingSession->aggregatedLayer = $grid->aggregationLayerName;

		$this->logger->debug('registered : '.$grid->aggregationLayerName);

	}

	/**
	 * AJAX function : Get the list of available grids.
	 *
	 * @return JSON The list of forms
	 */
	public function ajaxgetgridsAction() {

		// Get the list of grids
		$grids = $this->gridsModel->getGrids();

		$gridsList = array();
		foreach ($grids as $grid) {
			$gridsList[] = array('name' => $grid->name, 'label' => $grid->label);
		}

		echo '{'.'metaData:{'.'root:\'rows\','.'fields:['.'\'name\','.'\'label\''.']'.'},'.'rows:'.json_encode($gridsList).'}';

		// No View, we send directly the JSON
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}

	/**
	 * Return the status of the service
	 */
	public function ajaxGetStatusAction(){
		$this->getStatus($this->calculationServiceModel, 'CalculationServlet');
	}

	/**
	 * Convert and display the UTF-8 encoded string to the configured charset
	 * @param String $output The string to encode and to display
	 */
	private function _print($output) {
		$configuration = Zend_Registry::get("configuration");
		echo iconv("UTF-8", $configuration->charset, $output);
	}

	/**
	 * Format a number.
	 *
	 * @param Numeric $input the input number
	 * @param Integer $decimals the number of decimals
	 */
	private function _numberFormat($input, $decimals) {
		return number_format($input, $decimals, '.', '');
	}

	/**
	 * Print the summary of the request in session as CSV.
	 */
	public function _printSummary() {

		// Put in session some labels used in the summary
		$this->_prepareSummary();

		// Summary
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$this->_print('Request Summary'."\n");

		// Operation
		$operation = $aggregationSession->operation;
		if (!empty($operation)) {
			$operationsLabel = $aggregationSession->operations;
			if (!empty($operationsLabel)) {

				$this->_print('Operation;'.$operationsLabel[$operation]."\n");
			} else {
				$this->_print('Operation;'.$operation."\n");
			}
		}

		// Dataset
		$datasetId = $aggregationSession->datasetId;
		if (!empty($datasetId)) {
			$datasetsLabel = $aggregationSession->datasets;
			if (!empty($datasetsLabel)) {
				$this->_print('Dataset;'.$datasetsLabel[$datasetId]."\n");
			} else {
				$this->_print('Dataset;'.$datasetId."\n");
			}
		}

		// Variables
		$variable = $aggregationSession->variable;
		if (!empty($variable)) {
			$this->_print('Variable;'.$variable->label."\n");
		}

		// Numerator
		$numerator = $aggregationSession->numerator;
		if (!empty($numerator)) {
			$this->_print('Numerator;'.$numerator->label."\n");
		}

		// Denominator
		$denominator = $aggregationSession->denominator;
		if (!empty($denominator)) {
			$this->_print('Denominator ;'.$denominator->label."\n");
		}

		// Aggregation cells
		$cell = $aggregationSession->cell;
		if (!empty($cell)) {
			$cellsLabel = $aggregationSession->cells;
			if (!empty($cellsLabel)) {
				$this->_print('Aggregation cells;'.$cellsLabel[$cell]."\n");
			} else {
				$this->_print('Aggregation cells;'.$cell."\n");
			}
		}

		// Filters
		$filters = $aggregationSession->filters;
		if (!empty($filters)) {
			foreach ($filters as $filterName => $filterValue) {
				$this->_print(';'.$filterName.';'.$filterValue."\n");
			}
		}

		$this->_print("\n");

	}

	/**
	 * Format an error.
	 *
	 * @param Numeric $standardDeviation the input error
	 * @param Numeric $baseNumber the base number
	 */
	private function _formatError($standardDeviation, $baseNumber) {
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$errorType = $aggregationSession->errorType;

		if ($baseNumber == 0) {
			return 0;
		}

		if ($errorType == 'IC90') {
			return $this->_numberFormat(100 * 1.645 * $standardDeviation / $baseNumber, 3);
		} else if ($errorType == 'IC95') {
			return $this->_numberFormat(100 * 1.96 * $standardDeviation / $baseNumber, 3);
		} else {
			return $this->_numberFormat($standardDeviation, 3);
		}
	}

	/**
	 * Display the label corresponding to the error type selected.
	 */
	private function _displayErrorTypeLabel() {
		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$errorType = $aggregationSession->errorType;

		if ($errorType == 'IC90') {
			return 'IC 90 (%)';
		} else if ($errorType == 'IC95') {
			return 'IC 95 (%)';
		} else {
			return 'Standard Error';
		}
	}

	/**
	 * Export the calculation result as CSV.
	 */
	public function exportCsvAction() {

		$this->logger->debug('exportCsvAction');

		// Get the session id
		$sessionId = session_id();

		// Define the header of the response
		$this->getResponse()->setHeader('Content-Type', 'text/csv;charset=UTF-8;application/force-download;', true);
		$this->getResponse()->setHeader('Content-disposition', 'attachment; filename=AggregationResult.csv', true);


		// Prepend the Byte Order Mask to inform Excel that the file is in UTF-8
		$configuration = Zend_Registry::get("configuration");
		if ($configuration->charset == 'UTF-8') {
			echo(chr(0xEF));
			echo(chr(0xBB));
			echo(chr(0xBF));
		}

		$aggregationSession = new Zend_Session_Namespace('aggregation');
		$operation = $aggregationSession->operation;
		$var = $aggregationSession->variable;
		$num = $aggregationSession->numerator;
		$errorType = $aggregationSession->errorType;


		// Summary
		$this->_printSummary();

		// Display the default message
		$this->_print('// *************************************************'."\n");
		$this->_print('// Aggregation Results'."\n");
		$this->_print('// *************************************************'."\n\n");

		// Export the column names
		if ($operation=='RATIO') {
			$this->_print('Cell Id;');
			$this->_print('Number of Plots;');
			$this->_print('Surface;');
			$this->_print('Number of Plots in the domain;');
			$this->_print('Domain Size;');
			$this->_print('Domain Size - '.$this->_displayErrorTypeLabel().';');
			if (($var != null && $var->data == 'IS_FOREST_PLOT') || ($num != null && $num->data ==  'IS_FOREST_PLOT')) {
				$this->_print('Mean %;');
			} else {
				$this->_print('Mean;');
			}
			$this->_print('Mean - '.$this->_displayErrorTypeLabel().';');
			$this->_print('Total in domain;');
			$this->_print('Total in domain - '.$this->_displayErrorTypeLabel().';');
			$this->_print("\n");
		} else {
			$this->_print('Cell Id;Number of Plots;Estimated surface;Average Value;Total value;Total (standard error)'."\n");
		}

		// Export the lines of result
		$results = $this->aggregationModel->getAggregatedData($sessionId);

		foreach ($results as $result) {

			$this->_print($result['cell_id'].';');
			$this->_print($this->_numberFormat($result['plot_number'], 0).';');
			$this->_print($this->_numberFormat($result['surface'], 0).';');

			if ($operation=='RATIO') {

				$this->_print($this->_numberFormat($result['dom_nb_plots'], 0).';');
				$this->_print($this->_numberFormat($result['total_estimate_z'], 3).';');
				$this->_print($this->_formatError($result['standard_error_total_z'], $result['total_estimate_z']).';');
				if (($var != null && $var->data == 'IS_FOREST_PLOT') || ($num != null && $num->data ==  'IS_FOREST_PLOT')) {
					$this->_print($this->_numberFormat($result['average_value'] * 100, 3).';');
					if ($errorType == 'IC90' || $errorType == 'IC95') {
						$this->_print($this->_formatError($result['standard_error'], $result['average_value']).';');
					} else {
						$this->_print($this->_formatError($result['standard_error'] * 100, $result['average_value']).';');
					}
				} else {
					$this->_print($this->_numberFormat($result['average_value'], 3).';');
					$this->_print($this->_formatError($result['standard_error'], $result['average_value']).';');
				}
				$this->_print($this->_numberFormat($result['total_estimate_x'], 3).';');
				$this->_print($this->_formatError($result['standard_error_total_x'], $result['total_estimate_x']).';');

			} else {

				$this->_print($this->_numberFormat($result['average_value'], 3).';');
				$this->_print($this->_numberFormat($result['surface'] * $result['average_value'], 3).';');
				$this->_print($this->_formatError($result['standard_error'], $result['average_value']).';');

			}

			$this->_print("\n");
		}

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
}
