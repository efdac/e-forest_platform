/**
 * Licensed under EUPL v1.1 (see http://ec.europa.eu/idabc/eupl).
 * 
 * © European Union, 2008-2012
 *
 * Reuse is authorised, provided the source is acknowledged. The reuse policy of the European Commission is implemented by a Decision of 12 December 2011.
 *
 * The general principle of reuse can be subject to conditions which may be specified in individual copyright notices. 
 * Therefore users are advised to refer to the copyright notices of the individual websites maintained under Europa and of the individual documents. 
 * Reuse is not applicable to documents subject to intellectual property rights of third parties.
 */
package fr.ifn.eforest.interpolation.dabatase.raw_data;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import fr.ifn.eforest.common.database.metadata.TableFieldData;

/**
 * Data Access Object used to extract the raw data needed for interpolation.
 */
public class InterpolationDAO {

	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Get a connexion to the database.
	 * 
	 * @return The <code>Connection</code>
	 * @throws NamingException
	 * @throws SQLException
	 */
	public Connection getConnection() throws NamingException, SQLException {

		Context initContext = new InitialContext();
		DataSource ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/rawdata");
		Connection cx = ds.getConnection();

		return cx;
	}

	/**
	 * Export the data to be interpolated.
	 * 
	 * @param sql
	 *            the SQL query corresponding to the user selection
	 * @param domain
	 *            the domain of validity of the variable
	 * @param variable
	 *            the quantitative variable to aggregate
	 * @param filename
	 *            the name of the file where is exported the data
	 */
	public void exportRawData(String sql, TableFieldData domain, TableFieldData variable, String filename) throws Exception {

		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection con = null;
		FileWriter writer = null;

		try {

			// Preparation of the request
			String request = "SELECT X(ST_transform(the_geom,3035)) as x, Y(ST_transform(the_geom,3035)) as y, coalesce(sum(" + variable.getFormat() + "."
					+ variable.getColumnName() + "),0) as value ";
			request += sql;
			request += " AND " + domain.getTableName() + "." + domain.getColumnName() + " = '1'  "; // Filter the plots corresponding to the domain
			request += " GROUP BY x, y";

			logger.debug("Export Query : " + request);

			// Execution of the request
			con = getConnection();
			ps = con.prepareStatement(request);
			rs = ps.executeQuery();

			// Check if the directory exist and create if otherwise
			File file = new File(filename);
			File dir = file.getParentFile();
			if (!dir.exists()) {
				dir.mkdirs();
			}

			// read the result and export it to a CSV
			writer = new FileWriter(filename);
			writer.append("X;Y;VALUE\n"); // header
			while (rs.next()) {
				writer.append(rs.getString("x"));
				writer.append(";");
				writer.append(rs.getString("y"));
				writer.append(";");
				writer.append(rs.getString("value"));
				writer.append("\n");
			}

		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (Exception e) {
				logger.error("Error while closing file writer : " + e.getMessage());
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing resultset : " + e.getMessage());
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing connexion : " + e.getMessage());
			}
		}
	}
}
