/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.calculation.database.aggregateddata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import fr.ifn.eforest.common.business.UnitTypes;
import fr.ifn.eforest.common.database.mapping.GridData;
import fr.ifn.eforest.common.database.metadata.TableFieldData;

/**
 * Data Access Object used to manage calculations.
 */
public class CalculationDAO {

	private Logger logger = Logger.getLogger(this.getClass());

	private Logger sqllogger = Logger.getLogger("SQLLogger");

	/**
	 * Clean previous results.
	 */
	private static final String CLEAN_RESULTS_STMT = "DELETE FROM aggregated_result WHERE session_id = ? OR ((NOW()-_creationdt)> '5 day') ";

	/**
	 * Get a connexion to the database.
	 * 
	 * @return The <code>Connection</code>
	 * @throws NamingException
	 * @throws SQLException
	 */
	public Connection getConnection() throws NamingException, SQLException {

		Context initContext = new InitialContext();
		DataSource ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/aggregateddata");
		Connection cx = ds.getConnection();

		return cx;
	}

	/**
	 * Aggregate the value per cluster and cell.
	 * 
	 * @param con
	 *            the connexion
	 * @param grid
	 *            the grid where to aggregate the variable
	 * @param variable
	 *            the quantitative variable to aggregate
	 * @param filtersList
	 *            the filters
	 * @param sql
	 *            the SQL query corresponding to the user selection
	 */
	public void meanSelectedPlots(Connection con, GridData grid, TableFieldData variable, List<TableFieldData> filtersList) throws Exception {

		PreparedStatement ps = null;
		try {

			// Build filter SQL
			String filterSQL = "";
			Iterator<TableFieldData> filtersListIter = filtersList.iterator();
			while (filtersListIter.hasNext()) {
				TableFieldData filter = filtersListIter.next();
				
				// Hardcoded value
				if (filter.getFieldName().equals("SPECIES_CLASS")) {
					filterSQL += " AND MAX(" + filter.getFormat() + "." + filter.getColumnName() + ") IN (SELECT src_code FROM group_mode WHERE src_unit = 'SPECIES_CODE' and dst_unit = 'SPECIES_CLASS' and dst_code = '" + filter.getValue() + "') ";
				} else {
					filterSQL += " AND MAX(" + filter.getFormat() + "." + filter.getColumnName() + ") = '" + filter.getValue() + "'";
				}
			}

			//
			String request = "CREATE TEMPORARY TABLE selected_plots ON COMMIT DROP AS ";
			request += " SELECT location_data.country_code, ";
			request += "        location_data.cluster_code, ";
			request += "        plot_data.stratum_code, ";
			request += "        plot_data.plot_code, ";
			// Hardcoded case for NUTS2
			if (grid.getGridName().equalsIgnoreCase("nuts2")) {
				request += "        substr(cell_id_nuts3, 1, 4) as cell_id, ";
			} else {
				request += "        " + grid.getLocationColumn() + " as cell_id, ";
			}
			request += "       COALESCE (";
			if (variable.getType().equals(UnitTypes.BOOLEAN)) {
				request += "(CASE WHEN MAX(" + variable.getColumnName() + ") = '1' " + filterSQL + " THEN 1 ELSE 0 END)::float";
			} else {
				request += "CASE WHEN (1 = 1) " + filterSQL + " THEN SUM(" + variable.getColumnName() + ") ELSE 0 END";
			}
			request += ", 0) AS value, "; // the value to aggregate
			request += "        MAX(statistical_weight) as harmonized_statistical_weigth ";

			request += " FROM LOCATION LOCATION_DATA ";
			request += " JOIN PLOT_DATA PLOT_DATA USING (country_code, plot_code)";
			request += " LEFT JOIN SPECIES_DATA SPECIES_DATA USING (country_code, plot_code, cycle)";
			request += " WHERE (1 = 1) ";

			// Optimisation : specific case for country code, we filter directly the selected plots
			filtersListIter = filtersList.iterator();
			while (filtersListIter.hasNext()) {
				TableFieldData filter = filtersListIter.next();
				if (filter.getFieldName().equalsIgnoreCase("COUNTRY_CODE")) {
					request += " AND " + filter.getFormat() + "." + filter.getColumnName() + " = '" + filter.getValue() + "'";
				}
			}

			request += " AND plot_data.submission_id IN ( ";
			request += "              SELECT DISTINCT submission_id ";
			request += "              FROM submission ";
			request += "              JOIN data_submission using (submission_id) ";
			request += "              WHERE request_id = 'WP3_REQUEST' ";
			request += "              AND status = 'OK' ";
			request += "              AND step = 'VALIDATED') "; // The submission must be validated
			request += " GROUP BY location_data.country_code, location_data.cluster_code,plot_data.stratum_code, plot_data.plot_code, ";
			if (grid.getGridName().equalsIgnoreCase("nuts2")) {
				request += " substr(cell_id_nuts3, 1, 4)";
			} else {
				request += grid.getLocationColumn();
			}

			// Preparation of the request
			ps = con.prepareStatement(request);
			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}

		}
	}

	/**
	 * Select the plot values for the calculation of a ratio.
	 * 
	 * @param con
	 *            the connexion
	 * @param grid
	 *            the grid where to aggregate the variable
	 * @param numerator
	 *            the numerator
	 * @param denominator
	 *            the numerator
	 * @param filtersList
	 *            the filters
	 * @param sql
	 *            the SQL query corresponding to the user selection
	 */
	public void ratioSelectedPlots(Connection con, GridData grid, TableFieldData numerator, TableFieldData denominator, List<TableFieldData> filtersList)
			throws Exception {

		PreparedStatement ps = null;
		try {

			// Build filter SQL
			String filterSQL = "";
			Iterator<TableFieldData> filtersListIter = filtersList.iterator();
			while (filtersListIter.hasNext()) {
				TableFieldData filter = filtersListIter.next();
				
				// Hardcoded value
				if (filter.getFieldName().equals("SPECIES_CLASS")) {
					filterSQL += " AND MAX(" + filter.getFormat() + "." + filter.getColumnName() + ") IN (SELECT src_code FROM group_mode WHERE src_unit = 'SPECIES_CODE' and dst_unit = 'SPECIES_CLASS' and dst_code = '" + filter.getValue() + "') ";
				} else {
					filterSQL += " AND MAX(" + filter.getFormat() + "." + filter.getColumnName() + ") = '" + filter.getValue() + "' ";
				}
			}

			//
			String request = "CREATE TEMPORARY TABLE selected_plots ON COMMIT DROP AS ";
			request += " SELECT location_data.country_code, ";
			request += "        location_data.cluster_code, ";
			request += "        plot_data.stratum_code, ";
			request += "        plot_data.plot_code, ";
			// Hardcoded case for NUTS2
			if (grid.getGridName().equalsIgnoreCase("nuts2")) {
				request += "        substr(cell_id_nuts3, 1, 4) as cell_id, ";
			} else {
				request += "        " + grid.getLocationColumn() + " as cell_id, ";
			}
			request += "       COALESCE (";
			if (numerator.getType().equals(UnitTypes.BOOLEAN) || numerator.getType().equals(UnitTypes.CODE)) { // Cas des domaines
				request += "(CASE WHEN MAX(" + numerator.getColumnName() + ") = '1' " + filterSQL + " THEN 1 ELSE 0 END)::float";
			} else {
				request += "CASE WHEN (1 = 1) " + filterSQL + " THEN SUM(" + numerator.getColumnName() + ") ELSE 0 END";
			}

			request += ", 0) AS valueX, ";
			request += "       COALESCE (";

			if (denominator == null) {
				request += "1"; // identity
			} else if (denominator.getType().equals(UnitTypes.BOOLEAN) || denominator.getType().equals(UnitTypes.CODE)) {
				request += "(CASE WHEN MAX(" + denominator.getColumnName() + ") = '1' THEN 1 ELSE 0 END)::float";
			} else {
				request += "CASE WHEN (1 = 1) THEN SUM(" + denominator.getColumnName() + ") ELSE 0 END";
			}
			request += ", 0) AS valueZ, ";
			request += "        MAX(statistical_weight) as harmonized_statistical_weigth ";
			request += " FROM LOCATION LOCATION_DATA ";
			request += " JOIN PLOT_DATA PLOT_DATA USING (country_code, plot_code)";
			request += " LEFT JOIN SPECIES_DATA SPECIES_DATA on (SPECIES_DATA.SUBMISSION_ID = PLOT_DATA.SUBMISSION_ID ";
			request += "			AND SPECIES_DATA.COUNTRY_CODE = PLOT_DATA.COUNTRY_CODE ";
			request += "			AND SPECIES_DATA.PLOT_CODE = PLOT_DATA.PLOT_CODE ";
			request += "			AND SPECIES_DATA.CYCLE = PLOT_DATA.CYCLE)";
			request += " WHERE (1 = 1) ";

			// Optimisation : specific case for country code, we filter directly the selected plots
			filtersListIter = filtersList.iterator();
			while (filtersListIter.hasNext()) {
				TableFieldData filter = filtersListIter.next();
				if (filter.getFieldName().equalsIgnoreCase("COUNTRY_CODE")) {
					request += " AND " + filter.getFormat() + "." + filter.getColumnName() + " = '" + filter.getValue() + "'";
				}
			}
			
			request += " AND plot_data.submission_id IN ( ";
			request += "              SELECT DISTINCT submission_id ";
			request += "              FROM submission ";
			request += "              JOIN data_submission using (submission_id) ";
			request += "              WHERE request_id = 'WP3_REQUEST' ";
			request += "              AND status = 'OK' ";
			request += "              AND step = 'VALIDATED') "; // The submission must be validated
			request += " GROUP BY location_data.country_code, location_data.cluster_code,plot_data.stratum_code, plot_data.plot_code, ";

			if (grid.getGridName().equalsIgnoreCase("nuts2")) {
				request += " substr(cell_id_nuts3, 1, 4)";
			} else {
				request += grid.getLocationColumn();
			}

			// Preparation of the request
			ps = con.prepareStatement(request);
			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}

		}
	}

	/**
	 * Calculating the cluster weights.
	 * 
	 * @param con
	 *            the connexion
	 */
	public void ratioBjePerCluster(Connection con) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE bje_per_cluster ON COMMIT DROP AS ";
			request += " SELECT country_code, ";
			request += "        cluster_code, ";
			request += "        stratum_code, ";
			request += "        sum(harmonized_statistical_weigth) / count(plot_code) * max(be) AS bje "; // cf page 27 verson 0.50
																										// /count(harmonized_statistical_weigth)
			request += " FROM selected_plots ";
			request += " LEFT JOIN strata_info using (country_code, stratum_code) ";
			request += " GROUP BY country_code, cluster_code, stratum_code";

			// Preparation of the request
			ps = con.prepareStatement(request);
			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}

		}
	}

	/**
	 * Aggregate the value per cluster.
	 * 
	 * @param con
	 *            the connexion
	 */
	public void meanBjePerCluser(Connection con) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE bje_per_cluster ON COMMIT DROP AS ";
			request += " SELECT country_code, ";
			request += "        cluster_code, ";
			request += "        stratum_code, ";
			request += "        sum(harmonized_statistical_weigth) / count(plot_code) * max(be) AS bje "; // poids du cluster
			request += " FROM selected_plots ";
			request += " LEFT JOIN strata_info using (country_code, stratum_code) ";
			request += " GROUP BY country_code, stratum_code, cluster_code";

			// Preparation of the request
			ps = con.prepareStatement(request);
			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}

		}
	}

	/**
	 * Aggregate the values per cluster and cell.
	 * 
	 * @param con
	 *            the connexion
	 */
	public void ratioValuePerCluster(Connection con) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE value_per_cluster ON COMMIT DROP AS ";
			request += " SELECT country_code, ";
			request += "        cluster_code, ";
			request += "        stratum_code, ";
			request += "        cell_id, ";
			request += "        nominal_cluster_size, ";  
			request += "        max(bje) as bje, ";
			request += "        sum(valueX) / count(*) AS value_cluster_x, ";
			request += "        sum(valueZ) / count(*) AS value_cluster_z, ";
			request += "        sum(valueZ) AS domain_nb_plots, ";
			request += "        count(*) AS cluster_nb_plots ";
			request += " FROM ( ";
			request += "        SELECT * ";
			request += "        FROM selected_plots  ";
			request += "        LEFT JOIN bje_per_cluster using (country_code, stratum_code, cluster_code) ";
			request += " ) as fog ";
			request += " LEFT JOIN strata_info using (country_code, stratum_code) ";
			request += " GROUP BY country_code, stratum_code, cluster_code, cell_id, nominal_cluster_size";

			// Preparation of the request
			ps = con.prepareStatement(request);
			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}

		}
	}

	/**
	 * Calculate the number of plots per cluster and cell.
	 * 
	 * @param con
	 *            the connexion
	 */
	public void meanValuePerCluster(Connection con) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE value_per_cluster ON COMMIT DROP AS";
			request += " SELECT 	country_code, ";
			request += "            cluster_code, ";
			request += "            stratum_code, ";
			request += "            cell_id, ";
			request += "        	nominal_cluster_size, ";
			request += "            max(bje) as bje, ";
			request += "            sum(value ) / count(*) AS value_cluster,  "; 
			request += "            count(*) AS cluster_nb_plots "; 
			request += " FROM ( ";
			request += "      SELECT * from selected_plots left join bje_per_cluster using (country_code, stratum_code, cluster_code)  ";
			request += " ) as fog ";
			request += " left join strata_info using (country_code, stratum_code) ";
			request += " GROUP BY country_code, stratum_code, cluster_code, cell_id, nominal_cluster_size";

			// Preparation of the request
			ps = con.prepareStatement(request);
			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Aggregate the value per cell.
	 * 
	 * @param con
	 *            the connexion
	 * @param sessionId
	 *            the identifier of the user session
	 */
	public void meanResult(Connection con, String sessionId) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE result ON COMMIT DROP AS ";
			request += " SELECT  '" + sessionId + "'::text as session_id,";
			request += "        cell_id, ";
			request += "        sum(cluster_nb_plots) as nb_plots,  ";
			request += "        sum(bje * cluster_nb_plots / nominal_cluster_size) as surface,  "; // surface de la cellule
			request += "        (sum(bje * cluster_nb_plots * value_cluster / nominal_cluster_size) /  sum(bje * cluster_nb_plots / nominal_cluster_size)) as mean_estimate, ";
			request += "        sum(bje) * (sum(bje * cluster_nb_plots * value_cluster) /  sum(bje * cluster_nb_plots)) as total_estimate ";
			request += " FROM value_per_cluster ";
			request += " GROUP BY cell_id";
			request += " ORDER BY cell_id";

			// Preparation of the request
			ps = con.prepareStatement(request + ";");

			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Aggregate the value per cell.
	 * 
	 * @param con
	 *            the connexion
	 * @param sessionId
	 *            the identifier of the user session
	 */
	public void ratioResult(Connection con, String sessionId) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE result ON COMMIT DROP AS ";
			request += " SELECT  '" + sessionId + "'::text as session_id,";
			request += "        cell_id, ";
			request += "        sum(cluster_nb_plots) as nb_plots, ";
			request += "        sum(bje * cluster_nb_plots / nominal_cluster_size) as surface, "; // surface de la cellule
			request += "        sum(bje * cluster_nb_plots) / sum(cluster_nb_plots) as mean_cluster_weight, ";
			request += "        sum(bje * cluster_nb_plots * value_cluster_x / nominal_cluster_size) / sum(bje * cluster_nb_plots / nominal_cluster_size) AS mean_estimate_x, ";
			request += "        sum(bje * cluster_nb_plots * value_cluster_z / nominal_cluster_size) / sum(bje * cluster_nb_plots / nominal_cluster_size) AS mean_estimate_z, ";
			request += "        case when sum(value_cluster_z) = 0 then 0 else sum(bje * cluster_nb_plots * value_cluster_x / nominal_cluster_size) / sum (bje * cluster_nb_plots * value_cluster_z / nominal_cluster_size) end as mean_estimate, ";
			request += "        case when sum(value_cluster_z) = 0 then 0 else sum(bje * cluster_nb_plots * value_cluster_z / nominal_cluster_size) end as total_estimate_z, ";
			request += "        case when sum(value_cluster_x) = 0 then 0 else sum(bje * cluster_nb_plots * value_cluster_x / nominal_cluster_size) end as total_estimate_x ";
			request += " FROM value_per_cluster ";
			request += " GROUP BY cell_id";
			request += " ORDER BY cell_id";

			// Preparation of the request
			ps = con.prepareStatement(request + ";");

			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Calculate the sampling info.
	 * 
	 * @param con
	 *            the connexion
	 */
	public void calculateSampling(Connection con) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE sampling ON COMMIT DROP AS ";
			request += " SELECT  cell_id,";
			request += "         sum(bje) ^ 2 / sum(bje ^ 2) as ml,";
			request += "         sum(bje * cluster_nb_plots) / sum(bje) as wl";
			request += " FROM value_per_cluster";
			request += " GROUP BY cell_id";

			// Preparation of the request
			ps = con.prepareStatement(request);
			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Calculate the ecart info.
	 * 
	 * @param con
	 *            the connexion
	 */
	public void ratioEcart(Connection con) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE ecart ON COMMIT DROP AS ";
			request += " SELECT 	cell_id,  ";
			request += " 	stratum_code,  ";
			request += " 	country_code,  ";
			request += " 	cluster_code,  ";
			request += " 	bje, ";
			request += " 	value_cluster_x - (mean_estimate * value_cluster_z) as ecart, ";
			request += " 	value_cluster_x - mean_estimate_x as ecart_x, ";
			request += " 	value_cluster_z - mean_estimate_z as ecart_z ";
			request += " FROM result ";
			request += " LEFT JOIN value_per_cluster using (cell_id);";

			// Preparation of the request
			ps = con.prepareStatement(request);
			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Calculate the moyenne de l'ecart info.
	 * 
	 * @param con
	 *            the connexion
	 */
	public void ratioMoyEcart(Connection con) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE moy_ecart ON COMMIT DROP AS ";
			request += " SELECT ";
			request += "       cell_id, ";
			request += "       sum(bje * ecart) / sum(bje) as moy_ecart ";
			request += " FROM ecart ";
			request += " GROUP BY cell_id;";

			// Preparation of the request
			ps = con.prepareStatement(request);
			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Calculate the standard error.
	 * 
	 * @param con
	 *            the connexion
	 * @param sessionId
	 *            the identifier of the user session
	 */
	public void meanStandardError(Connection con, String sessionId) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE standard_error ON COMMIT DROP AS";
			request += " SELECT  '" + sessionId + "'::text as session_id,";
			request += "        cell_id, ";
			request += "        case when max(ml) = 1 then 0";
			request += "        else max(surface) * sqrt(sum( bje * ((cluster_nb_plots / wl) ^ 2) * (mean_estimate - value_cluster) ^ 2) / ((max(ml) - 1) * sum (bje))) ";
			request += "        end as standard_error ";
			request += " FROM value_per_cluster ";
			request += " LEFT JOIN result using (cell_id) ";
			request += " LEFT JOIN sampling using (cell_id) ";
			request += " GROUP BY cell_id";

			// Preparation of the request
			ps = con.prepareStatement(request + ";");

			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");

			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Calculate the standard error.
	 * 
	 * @param con
	 *            the connexion
	 * @param sessionId
	 *            the identifier of the user session
	 */
	public void ratioStandardError(Connection con, String sessionId) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "CREATE TEMPORARY TABLE standard_error ON COMMIT DROP AS";
			request += " SELECT  '" + sessionId + "'::text as session_id,";
			request += "        cell_id, ";
			request += "        max(result.nb_plots) as nb_plots, ";
			request += "        max(wl) as mean_cluster_size, ";
			request += "        sum(vc.bje) / count(vc.bje) as mean_cluster_weight, ";
			request += "        sum(vc.domain_nb_plots) as dom_nb_plots, ";
			request += "        max(result.surface) as surface, ";
			request += "        max(result.mean_estimate_x) as mean_estimate_x, ";
			request += "        max(result.total_estimate_x) as total_estimate_x, ";
			request += "        max(result.mean_estimate_z) as mean_estimate_z, ";
			request += "        max(result.total_estimate_z) as total_estimate_z, ";
			request += "        max(result.mean_estimate) as mean, ";
			request += "        case ";
			request += "        	when max(ml) = 1 then 0 ";
			request += "        	else (sqrt(sum( vc.bje * ((vc.cluster_nb_plots / wl) ^ 2) * (( ecart_x) ^ 2 )) / ((max(ml) - 1) * sum (vc.bje)))) ";
			request += "        end as standard_error_mean_x, ";
			request += "        case ";
			request += "        when max(ml) = 1 then 0 ";
			request += "        	else max(result.surface)*(sqrt(sum( vc.bje * ((vc.cluster_nb_plots / wl) ^ 2) * (( ecart_x) ^ 2 )) / ((max(ml) - 1) * sum (vc.bje)))) ";
			request += "        end as standard_error_total_x, ";
			request += "        case ";
			request += "        	when max(ml) = 1 then 0 ";
			request += "        	else (sqrt(sum( vc.bje * ((vc.cluster_nb_plots / wl) ^ 2) * (( ecart_z) ^ 2 )) / ((max(ml) - 1) * sum (vc.bje)))) ";
			request += "        end as standard_error_mean_z, ";
			request += "        case ";
			request += "        	when max(ml) = 1 then 0 ";
			request += "        	else max(result.surface) * (sqrt(sum( vc.bje * ((vc.cluster_nb_plots / wl) ^ 2) * (( ecart_z) ^ 2 )) / ((max(ml) - 1) * sum (vc.bje)))) ";
			request += "        end as standard_error_total_z, ";
			request += "        case";
			request += "        	when max(ml) = 1 then 0";
			request += "        	when max(mean_estimate_z) = 0 then 0 ";
			request += "        	else (1 / max(mean_estimate_z)) * sqrt(sum( vc.bje * ((vc.cluster_nb_plots / wl) ^ 2) * (( ecart - moy_ecart) ^ 2 )) / ((max(ml) - 1) * sum (vc.bje))) ";
			request += "        end as standard_error ";
			request += " FROM value_per_cluster vc ";
			request += " LEFT JOIN result USING (cell_id) ";
			request += " LEFT JOIN sampling USING (cell_id) ";
			request += " LEFT JOIN moy_ecart USING (cell_id) ";
			request += " LEFT JOIN ecart USING (cell_id, stratum_code, country_code, cluster_code) ";
			request += " GROUP BY cell_id";

			// Preparation of the request
			ps = con.prepareStatement(request + ";");

			sqllogger.debug(request.replaceAll("ON COMMIT DROP ", "") + ";");

			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Copy the results.
	 * 
	 * @param con
	 *            the connexion
	 * @param sessionId
	 *            the identifier of the user session
	 */
	public void copyResultsMean(Connection con, String sessionId) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "INSERT INTO aggregated_result ";
			request += "(session_id, cell_id, plot_number, surface, average_value, total_value, standard_error) ";
			request += " SELECT  '" + sessionId + "',";
			request += "        cell_id, ";
			request += "        nb_plots, ";
			request += "        surface, ";
			request += "        mean_estimate, ";
			request += "        total_estimate,	";
			request += "        standard_error";
			request += " FROM result ";
			request += " LEFT JOIN standard_error using (session_id, cell_id) ";
			request += " WHERE cell_id IS NOT NULL ";
			request += " ORDER BY cell_id";

			// Preparation of the request
			ps = con.prepareStatement(request + ";");

			sqllogger.debug(request);
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Copy the results.
	 * 
	 * @param con
	 *            the connexion
	 * @param sessionId
	 *            the identifier of the user session
	 */
	public void copyResultsRatio(Connection con, String sessionId) throws Exception {

		PreparedStatement ps = null;
		try {

			String request = "INSERT INTO aggregated_result ";
			request += "(session_id, cell_id, plot_number, surface, dom_nb_plots, total_estimate_z, standard_error_total_z, average_value, standard_error, total_estimate_x, standard_error_total_x) ";
			request += " SELECT  '" + sessionId + "',";
			request += "        cell_id, ";
			request += "        nb_plots, ";
			request += "        surface, ";
			request += "        dom_nb_plots, ";
			request += "        total_estimate_z, ";
			request += "        standard_error_total_z, ";
			request += "        mean, ";
			request += "        standard_error,";
			request += "        total_estimate_x, ";
			request += "        standard_error_total_x ";
			request += " FROM standard_error ";
			request += " WHERE cell_id IS NOT NULL ";
			request += " ORDER BY cell_id";

			// Preparation of the request
			ps = con.prepareStatement(request + ";");

			sqllogger.debug(request);
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

	/**
	 * Clean the previous resultats
	 * 
	 * @param sessionId
	 *            the identifier of the user session
	 */
	public void cleanResults(String sessionId) throws Exception {

		Connection con = null;
		PreparedStatement ps = null;
		try {

			con = getConnection();

			sqllogger.debug("***********************************************************");

			String request = CLEAN_RESULTS_STMT;

			// Preparation of the request
			ps = con.prepareStatement(request);
			ps.setString(1, sessionId);

			sqllogger.debug(request.replaceFirst("\\?", sessionId) + ";");
			ps.execute();

		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}
	}

}
