/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.calculation.business;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import fr.ifn.eforest.calculation.database.aggregateddata.CalculationDAO;
import fr.ifn.eforest.common.business.AbstractService;
import fr.ifn.eforest.common.business.AbstractThread;
import fr.ifn.eforest.common.database.mapping.GridDAO;
import fr.ifn.eforest.common.database.mapping.GridData;
import fr.ifn.eforest.common.database.metadata.MetadataDAO;
import fr.ifn.eforest.common.database.metadata.TableFieldData;

/**
 * Calculation Service.
 */
public class CalculationService extends AbstractService {

	/**
	 * The logger used to log the errors or several information.
	 * 
	 * @see org.apache.log4j.Logger
	 */
	private final transient Logger logger = Logger.getLogger(this.getClass());

	// Les DAOs
	private CalculationDAO calculationDAO = new CalculationDAO();
	private MetadataDAO metadataDAO = new MetadataDAO();
	private GridDAO gridDAO = new GridDAO();

	/**
	 * Constructor.
	 */
	public CalculationService() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param thread
	 *            The thread that launched the service
	 */
	public CalculationService(AbstractThread thread) {
		super(thread);
	}

	/**
	 * Aggregate Data.
	 * 
	 * @param datasetId
	 *            the identifier of the dataset
	 * @param sessionId
	 *            the identifier of the user session (to retrieve the selected plots)
	 * @param variableName
	 *            the quantitative value to aggregate
	 * @param variableFormat
	 *            the logical name of the table where to find the variable
	 * @param gridName
	 *            the logical name of the grid where to aggregate
	 * @param filtersList
	 *            the filters
	 */
	public void aggregateData(String datasetId, String sessionId, String variableName, String variableFormat, String gridName, List<TableFieldData> filtersList) {
		Connection con = null;
		try {

			logger.debug("Aggregate data");

			con = calculationDAO.getConnection();
			con.setAutoCommit(false);

			// Retrieve some information about the quantitative variable
			TableFieldData variable = metadataDAO.getTableField(variableFormat, variableName);

			// Retrieve some information about the grid where to aggregate
			GridData grid = gridDAO.getGrid(gridName);
			if (grid == null) {
				throw new Exception("No grid definition for grid name " + gridName);
			}

			// Clean the result database
			if (thread != null) {
				thread.updateInfo("Clean the result database", 0, 0);
			}
			calculationDAO.cleanResults(sessionId);

			// Simple aggretation
			// aggregationDAO.simpleAggregation(con, sessionId, grid, domain, variable, sql);

			/**
			 * Adrian's algorithm
			 */

			if (thread != null) {
				thread.updateInfo("Identify selected plots", 0, 0);
			}
			calculationDAO.meanSelectedPlots(con, grid, variable, filtersList);

			// Aggregate the value per cluster and cell
			if (thread != null) {
				thread.updateInfo("Calculating the cluster weights", 0, 0);
			}
			calculationDAO.meanBjePerCluser(con);

			// Aggregate the value per strata and cell
			if (thread != null) {
				thread.updateInfo("Aggregate the value per cluster", 0, 0);
			}
			calculationDAO.meanValuePerCluster(con);

			// Aggregate the value per cell
			if (thread != null) {
				thread.updateInfo("Aggregate the value per cell", 0, 0);
			}
			calculationDAO.meanResult(con, sessionId);

			// Calculate sampling info
			if (thread != null) {
				thread.updateInfo("Calculate sampling info", 0, 0);
			}
			calculationDAO.calculateSampling(con);

			// Calculate the standard error
			if (thread != null) {
				thread.updateInfo("Calculate the standard error", 0, 0);
			}
			calculationDAO.meanStandardError(con, sessionId);

			// Copy the results
			if (thread != null) {
				thread.updateInfo("Copy the results", 0, 0);
			}
			calculationDAO.copyResultsMean(con, sessionId);

			con.commit();

			logger.debug("Data aggregated");

		} catch (Exception e) {
			logger.error("Error while aggregating data", e);
		} finally {
			try {
				if (con != null) {
					con.rollback();
					con.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}

	}

	/**
	 * Aggregate Data.
	 * 
	 * @param datasetId
	 *            the identifier of the dataset
	 * @param sessionId
	 *            the identifier of the user session (to retrieve the selected plots)
	 * @param numeratorName
	 *            the quantitative value to aggregate
	 * @param numeratorFormat
	 *            the logical name of the table where to find the variable
	 * @param denominatorName
	 *            the quantitative value to aggregate
	 * @param denominatorFormat
	 *            the logical name of the table where to find the variable * @param gridName the logical name of the grid where to aggregate
	 * @param gridName
	 *            the logical name of the grid where to aggregate
	 * @param filtersList
	 *            the filters
	 */
	public void aggregateRatioData(String datasetId, String sessionId, String numeratorName, String numeratorFormat, String denominatorName,
			String denominatorFormat, String gridName, List<TableFieldData> filtersList) {
		Connection con = null;
		try {

			logger.debug("Aggregate data");

			con = calculationDAO.getConnection();
			con.setAutoCommit(false);

			// Retrieve some information about the quantitative variable
			TableFieldData numerator = metadataDAO.getTableField(numeratorFormat, numeratorName);
			TableFieldData denominator = metadataDAO.getTableField(denominatorFormat, denominatorName);

			// Retrieve some information about the grid where to aggregate
			GridData grid = gridDAO.getGrid(gridName);
			if (grid == null) {
				throw new Exception("No grid definition for grid name " + gridName);
			}

			// Clean the result database
			if (thread != null) {
				thread.updateInfo("Cleaning the result database", 0, 0);
			}
			calculationDAO.cleanResults(sessionId);

			/**
			 * Adrian's algorithm
			 */

			if (thread != null) {
				thread.updateInfo("Identifying selected plots", 0, 0);
			}
			calculationDAO.ratioSelectedPlots(con, grid, numerator, denominator, filtersList);

			// Calculating the cluster weights
			if (thread != null) {
				thread.updateInfo("Calculating the cluster weights", 0, 0);
			}
			calculationDAO.ratioBjePerCluster(con);

			// Aggregate the value per cluster and cell
			if (thread != null) {
				thread.updateInfo("Aggregating the value per cluster", 0, 0);
			}
			calculationDAO.ratioValuePerCluster(con);

		
			// Aggregate the value per cell
			if (thread != null) {
				thread.updateInfo("Aggregating the value per cell", 0, 0);
			}
			calculationDAO.ratioResult(con, sessionId);

			// Calculate sampling info
			if (thread != null) {
				thread.updateInfo("Calculating sampling info", 0, 0);
			}
			calculationDAO.calculateSampling(con);

			// Calculate ecart info
			if (thread != null) {
				thread.updateInfo("Calculating distance info", 0, 0);
			}
			calculationDAO.ratioEcart(con);

			// Calculate moyecart info
			if (thread != null) {
				thread.updateInfo("Calculating mean distance info", 0, 0);
			}
			calculationDAO.ratioMoyEcart(con);

			// Calculate the standard error
			if (thread != null) {
				thread.updateInfo("Calculating the standard error", 0, 0);
			}
			calculationDAO.ratioStandardError(con, sessionId);

			// Copy the results
			if (thread != null) {
				thread.updateInfo("Copying the results", 0, 0);
			}
			calculationDAO.copyResultsRatio(con, sessionId);

			con.commit();

			logger.debug("Data aggregated");

		} catch (Exception e) {
			logger.error("Error while aggregating data", e);
		} finally {
			try {
				if (con != null) {
					con.rollback();
					con.close();
				}
			} catch (SQLException e) {
				logger.error("Error while closing statement : " + e.getMessage());
			}
		}

	}
}
