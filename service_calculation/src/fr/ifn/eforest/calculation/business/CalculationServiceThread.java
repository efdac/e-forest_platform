/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.calculation.business;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import fr.ifn.eforest.common.business.AbstractThread;
import fr.ifn.eforest.common.business.ThreadLock;
import fr.ifn.eforest.common.database.metadata.TableFieldData;

/**
 * Thread running the calculation process.
 */
public class CalculationServiceThread extends AbstractThread {

	// Attributes
	private String datasetId;
	private String sessionId;
	private String variableName;
	private String variableFormat;
	private String gridName;
	private List<TableFieldData> filtersList;

	/**
	 * The logger used to log the errors or several information.
	 * 
	 * @see org.apache.log4j.Logger
	 */
	protected final transient Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Constructor.
	 * 
	 * @param datasetId
	 *            the identifier of the dataset
	 * @param sessionId
	 *            the identifier of the user session (to retrieve the selected plots)
	 * @param variableName
	 *            the quantitative value to aggregate
	 * @param variableFormat
	 *            the logical name of the table where to find the variable
	 * @param gridName
	 *            the logical name of the grid where to aggregate
	 * @param filtersList
	 *            the filters
	 * @throws Exception
	 */
	public CalculationServiceThread(String datasetId, String sessionId, String variableName, String variableFormat, String gridName,
			List<TableFieldData> filtersList) throws Exception {
		this.datasetId = datasetId;
		this.sessionId = sessionId;
		this.variableName = variableName;
		this.variableFormat = variableFormat;
		this.gridName = gridName;
		this.filtersList = filtersList;
	}

	/**
	 * Launch in thread mode the aggregation process.
	 */
	public void run() {

		try {

			Date startDate = new Date();
			logger.debug("Start of the calculation process " + startDate + ".");

			CalculationService calculationService = new CalculationService(this);
			calculationService.aggregateData(datasetId, sessionId, variableName, variableFormat, gridName, filtersList);

			// Log the end the the request
			Date endDate = new Date();
			logger.debug("Calculation process terminated successfully in " + (endDate.getTime() - startDate.getTime()) / 1000.00 + " sec.");

		} finally {
			// Remove itself from the list of running checks
			ThreadLock.getInstance().releaseProcess(sessionId);
		}

	}

}
