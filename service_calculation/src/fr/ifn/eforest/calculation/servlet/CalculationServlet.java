/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.calculation.servlet;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.ifn.eforest.calculation.business.CalculationServiceRatioThread;
import fr.ifn.eforest.calculation.business.CalculationServiceThread;
import fr.ifn.eforest.common.business.AbstractThread;
import fr.ifn.eforest.common.business.ThreadLock;
import fr.ifn.eforest.common.database.metadata.MetadataDAO;
import fr.ifn.eforest.common.database.metadata.TableFieldData;
import fr.ifn.eforest.common.servlet.AbstractServlet;

/**
 * CalculationServlet Servlet. <br>
 */
public class CalculationServlet extends AbstractServlet {

	/**
	 * The logger used to log the errors or several information.
	 * 
	 * @see org.apache.log4j.Logger
	 */
	private final transient Logger logger = Logger.getLogger(this.getClass());

	/**
	 * The serial version ID used to identify the object.
	 */
	private static final long serialVersionUID = -455284792196591246L;

	/**
	 * Input parameters.
	 */
	private static final String ACTION = "action";
	private static final String ACTION_AGGREGATE_DATA = "AggregateData";
	private static final String ACTION_AGGREGATE_RATIO_DATA = "AggregateDataRatio";
	private static final String ACTION_STATUS = "status";

	private static final String DATASET_ID = "DATASET_ID";
	private static final String SESSION_ID = "SESSION_ID";
	private static final String VARIABLE_NAME = "VARIABLE_NAME";
	private static final String VARIABLE_FORMAT = "VARIABLE_FORMAT";
	private static final String NUMERATOR_NAME = "NUMERATOR_NAME";
	private static final String NUMERATOR_FORMAT = "NUMERATOR_FORMAT";
	private static final String DENOMINATOR_NAME = "DENOMINATOR_NAME";
	private static final String DENOMINATOR_FORMAT = "DENOMINATOR_FORMAT";
	private static final String GRID = "GRID";
	private static final String FILTER = "FILTER__";

	/**
	 * Main function of the servlet.
	 * 
	 * @param request
	 *            the request done to the servlet
	 * @param response
	 *            the response sent
	 */
	public void service(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.setContentType("text/xml");
		response.setCharacterEncoding("UTF-8");

		String action = null;
		ServletOutputStream out = response.getOutputStream();

		// logRequestParameters(request);

		try {

			logger.debug("Calculation Servlet called");

			action = request.getParameter(ACTION);
			if (action == null) {
				throw new Exception("The " + ACTION + " parameter is mandatory");
			}

			/*
			 * Get the STATE of the process
			 */
			if (action.equals(ACTION_STATUS)) {

				String sessionId = request.getParameter(SESSION_ID);
				if (sessionId == null) {
					throw new Exception("The " + SESSION_ID + " parameter is mandatory");
				}

				// Try to get the instance of the checkservice for this submissionId
				AbstractThread process = (AbstractThread) ThreadLock.getInstance().getProcess(sessionId);

				if (process != null) {
					// There is a running thread, we get its current status.
					out.print(generateResult("RUNNING", process));
				} else {
					// We try to get the status of the last harmonization
					out.print(generateResult("OK"));
				}

			} else

			/*
			 * Launch the calculation of aggregated values
			 */
			if (action.equals(ACTION_AGGREGATE_DATA)) {

				String sessionId = request.getParameter(SESSION_ID);
				if (sessionId == null) {
					throw new Exception("The " + SESSION_ID + " parameter is mandatory");
				}
				String datasetId = request.getParameter(DATASET_ID);
				if (datasetId == null) {
					throw new Exception("The " + DATASET_ID + " parameter is mandatory");
				}
				String variableName = request.getParameter(VARIABLE_NAME);
				if (variableName == null) {
					throw new Exception("The " + VARIABLE_NAME + " parameter is mandatory");
				}
				String variableFormat = request.getParameter(VARIABLE_FORMAT);
				if (variableFormat == null) {
					throw new Exception("The " + VARIABLE_FORMAT + " parameter is mandatory");
				}
				String grid = request.getParameter(GRID);
				if (grid == null) {
					throw new Exception("The " + GRID + " parameter is mandatory");
				}

				// Get filters
				List<TableFieldData> filtersList = new ArrayList<TableFieldData>();
				Enumeration paramEnum = request.getParameterNames();
				while (paramEnum.hasMoreElements()) {
					String param = (String) paramEnum.nextElement();
					if (param.startsWith(FILTER)) {

						String filterName = param.substring(FILTER.length());
						String format = filterName.substring(0, (filterName.indexOf("__")));
						String data = filterName.substring((filterName.indexOf("__") + 2));

						String value = request.getParameter(param);

						MetadataDAO metadataDAO = new MetadataDAO();
						TableFieldData filter = metadataDAO.getTableField(format, data);
						filter.setValue(value);

						filtersList.add(filter);
					}
				}

				// Check if a thread is already running
				CalculationServiceThread process = (CalculationServiceThread) ThreadLock.getInstance().getProcess(sessionId);
				if (process != null) {
					throw new Exception("A process is already running for this interpolation");
				}

				// Launch the harmonization thread
				process = new CalculationServiceThread(datasetId, sessionId, variableName, variableFormat, grid, filtersList);
				process.start();

				// Register the running thread
				ThreadLock.getInstance().lockProcess(sessionId, process);

				// Output the current status of the check service
				out.print(generateResult("RUNNING", process));

			} else

			/*
			 * Launch the calculation of aggregated values
			 */
			if (action.equals(ACTION_AGGREGATE_RATIO_DATA)) {

				String sessionId = request.getParameter(SESSION_ID);
				if (sessionId == null) {
					throw new Exception("The " + SESSION_ID + " parameter is mandatory");
				}
				String datasetId = request.getParameter(DATASET_ID);
				if (datasetId == null) {
					throw new Exception("The " + DATASET_ID + " parameter is mandatory");
				}
				String numeratorName = request.getParameter(NUMERATOR_NAME);
				if (numeratorName == null) {
					throw new Exception("The " + NUMERATOR_NAME + " parameter is mandatory");
				}
				String numeratorFormat = request.getParameter(NUMERATOR_FORMAT);
				if (numeratorFormat == null) {
					throw new Exception("The " + NUMERATOR_FORMAT + " parameter is mandatory");
				}
				String denominatorName = request.getParameter(DENOMINATOR_NAME);
				if (denominatorName == null) {
					throw new Exception("The " + DENOMINATOR_NAME + " parameter is mandatory");
				}
				String denominatorFormat = request.getParameter(DENOMINATOR_FORMAT);
				if (denominatorFormat == null) {
					throw new Exception("The " + DENOMINATOR_FORMAT + " parameter is mandatory");
				}
				String grid = request.getParameter(GRID);
				if (grid == null) {
					throw new Exception("The " + GRID + " parameter is mandatory");
				}

				// Get filters
				List<TableFieldData> filtersList = new ArrayList<TableFieldData>();
				Enumeration paramEnum = request.getParameterNames();
				while (paramEnum.hasMoreElements()) {
					String param = (String) paramEnum.nextElement();
					if (param.startsWith(FILTER)) {

						String filterName = param.substring(FILTER.length());
						String format = filterName.substring(0, (filterName.indexOf("__")));
						String data = filterName.substring((filterName.indexOf("__") + 2));

						String value = request.getParameter(param);

						// HardCoded
						if (data.equals("SPECIES_CLASS")) {
							TableFieldData filter = new TableFieldData();
							filter.setColumnName("SPECIES_CODE");
							filter.setFormat("SPECIES_DATA");
							filter.setType("CODE");
							filter.setFieldName("SPECIES_CLASS");
							filter.setValue(value);
							filtersList.add(filter);
						} else {
							// Look for the description of the field in the metadata
							MetadataDAO metadataDAO = new MetadataDAO();
							TableFieldData filter = metadataDAO.getTableField(format, data);
							filter.setValue(value);
							filtersList.add(filter);
						}
					}
				}

				// Check if a thread is already running
				CalculationServiceRatioThread process = (CalculationServiceRatioThread) ThreadLock.getInstance().getProcess(sessionId);
				if (process != null) {
					throw new Exception("A process is already running for this interpolation");
				}

				// Launch the harmonization thread
				process = new CalculationServiceRatioThread(datasetId, sessionId, numeratorName, numeratorFormat, denominatorName, denominatorFormat, grid,
						filtersList);
				process.start();

				// Register the running thread
				ThreadLock.getInstance().lockProcess(sessionId, process);

				// Output the current status of the check service
				out.print(generateResult("RUNNING", process));

			} else {
				throw new Exception("The action type is unknown");
			}

		} catch (Exception e) {
			logger.error("Error during data calculation", e);
			out.print(generateErrorMessage(e.getMessage()));
		}
	}
}
