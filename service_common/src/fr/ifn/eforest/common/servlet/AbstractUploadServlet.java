/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.common.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import fr.ifn.eforest.common.servlet.AbstractServlet;
import fr.ifn.eforest.common.database.website.ApplicationParametersDAO;

/**
 * Abstract Upload Servlet.
 */
public abstract class AbstractUploadServlet extends AbstractServlet {

	/**
	 * The serial version ID used to identify the object.
	 */
	protected static final long serialVersionUID = -123484792196121293L;

	/**
	 * High level API for processing file uploads.
	 * 
	 * @see org.apache.commons.fileupload
	 * @see org.apache.commons.fileupload.servlet
	 */
	protected transient ServletFileUpload fileUpload;

	/**
	 * The factory used to store the file uploaded on the disk.
	 * 
	 * @see org.apache.commons.fileupload
	 * @see org.apache.commons.fileupload.disk
	 */
	protected transient DiskFileItemFactory fileItemFactory;

	/**
	 * The system file separator.
	 */
	protected final String separator = System.getProperty("file.separator");

	/**
	 * The upload path.
	 */
	protected String pathFileDirectory = null;

	/**
	 * Initializes servlet.
	 * 
	 * @throws ServletException
	 */
	public void init() throws ServletException {

		logger.debug("Start the Servlet initialisation...");

		try {

			// Read application parameters
			ApplicationParametersDAO parameterDAO = new ApplicationParametersDAO();
			pathFileDirectory = parameterDAO.getApplicationParameter("UploadDirectory");

			// Create a new file upload handler
			this.fileItemFactory = new DiskFileItemFactory();
			this.fileUpload = new ServletFileUpload(fileItemFactory);

			// Set the total max for a request size = 1 Go
			long yourMaxRequestSize = 1024 * 1024 * 100;
			fileUpload.setSizeMax(yourMaxRequestSize);

		} catch (Exception e) {
			logger.error("Error while initialising servlet", e);

			throw new ServletException(e);
		}
		logger.debug("Servlet initialisation... OK");

	}

	/**
	 * Upload a file.
	 * 
	 * @param fileItem
	 *            a file
	 * @param fileName
	 *            the name of the destination file
	 */
	protected void uploadFile(FileItem fileItem, String fileName) throws Exception {
		InputStream sourceFile = null;
		OutputStream destinationFile = null;
		try {
			// GetName is used here for the files
			long sizeInBytes = fileItem.getSize();

			// Check if the file is stored on disk or in memory
			boolean writeToFile = (sizeInBytes > fileItemFactory.getSizeThreshold());

			// Create the file object
			File uploadedFile = new File(fileName);

			// Create the upload directory if needed
			String uploadPath = uploadedFile.getParent();
			File uploadDir = new File(uploadPath);
			if (!uploadDir.exists()) {
				boolean dirWritten = uploadDir.mkdirs();
				if (!dirWritten) {
					throw new Exception("Error while creating the directory " + uploadPath);
				}
			}

			// Write data to disk
			if (writeToFile) {
				// Direct Record
				fileItem.write(uploadedFile);
				logger.debug("File Recorded in Direct Record mode : " + fileName);
			} else {
				// File is in memory, force the write on disk
				sourceFile = fileItem.getInputStream();
				destinationFile = new FileOutputStream(uploadedFile);

				byte[] buffer = new byte[512 * 1024];
				int nbLecture;
				while ((nbLecture = sourceFile.read(buffer)) != -1) {
					destinationFile.write(buffer, 0, nbLecture);
				}
				logger.debug("File Recorded in Streaming Record mode : " + fileName);
			}

		} catch (Exception e) {
			logger.error("Error while writing the file ", e);
			throw new FileUploadException("Error while writing the file to disk : " + e.getMessage());
		} finally {
			// Close the files
			if (sourceFile != null) {
				try {
					sourceFile.close();
				} catch (IOException ignored) {
					logger.error("Error while closing the file ", ignored);
				}
			}

			if (destinationFile != null) {
				try {
					destinationFile.close();
				} catch (IOException ignored) {
					logger.error("Error while closing the file ", ignored);
				}
			}

		}
	}

}
