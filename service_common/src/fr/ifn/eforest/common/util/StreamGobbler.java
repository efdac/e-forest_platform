/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/
package fr.ifn.eforest.common.util;

import java.io.*;

/**
 * Classe permettant de lire en continu la sortie d'un processus.
 * 
 * @author Michael C. Daconta
 * see http://www.javaworld.com/jw-12-2000/jw-1229-traps.html?page=4
 */
public class StreamGobbler extends Thread {

	InputStream is;

	String content = "";

	/**
	 * Constructor
	 * 
	 * @param is
	 *            The input stream
	 */
	public StreamGobbler(InputStream is) {
		this.is = is;
	}

	/**
	 * Read the content of an input stream.
	 */
	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				content += line + "\n";
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Return the read data.
	 * 
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

}