/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.common.business;

import java.util.HashMap;
import java.util.Map;

/**
 * Singleton lock used to store and lock the running process (CheckThread, UploadThread, ...).
 */
public class ThreadLock {

	/**
	 * The single instance of <code>ThreadLock</code>.
	 */
	private static ThreadLock instance = new ThreadLock();

	/**
	 * The <code>Map</code> containing the locks associed with the process Thread.
	 */
	private final Map<String, Thread> submissionMap = new HashMap<String, Thread>();

	/**
	 * Private constructor.
	 * <p>
	 * Used to force the call to the getInstance() method.
	 */
	private ThreadLock() {
		// Do nothing
	}

	/**
	 * Return the single instance of the submissionLock.
	 * 
	 * @return submissionLock
	 */
	public static ThreadLock getInstance() {
		return instance;
	}

	/**
	 * Set a lock for a given submission ID.
	 * 
	 * @param key
	 *            the submission id
	 * @param process
	 *            the Thread we want to lock
	 */
	public void lockProcess(String key, Thread process) {
		submissionMap.put(key, process);
	}

	/**
	 * Release the lock on a given key.
	 * 
	 * @param key
	 *            the key
	 */
	public void releaseProcess(String key) {
		submissionMap.remove(key);
	}

	/**
	 * Check if an submission ID is locked.
	 * 
	 * @param key
	 *            the key
	 * @return the lock value for the specified key
	 */
	public boolean isLocked(String key) {
		return submissionMap.containsKey(key);
	}

	/**
	 * Get a locked process for a given key.
	 * 
	 * @param key
	 *            the key
	 * @return the locked Thread.
	 */
	public Thread getProcess(String key) {
		return submissionMap.get(key);
	}

}
