/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.common.business.checks;

/**
 * List the check codes.
 */
public interface CheckCodes {

	/**
	 * Empty file.
	 */
	Integer EMPTY_FILE = 1000;

	/**
	 * Wrong file number.
	 */
	Integer WRONG_FIELD_NUMBER = 1001;

	/**
	 * Integrity constraint (trying to insert a plot when the location doesn't exist).
	 */
	Integer INTEGRITY_CONSTRAINT = 1002;

	/**
	 * Unexpected SQL error.
	 */
	Integer UNEXPECTED_SQL_ERROR = 1003;

	/**
	 * Duplicate row.
	 */
	Integer DUPLICATE_ROW = 1004;

	/**
	 * Mandatory field missing.
	 */
	Integer MANDATORY_FIELD_MISSING = 1101;

	/**
	 * Invalid format.
	 */
	Integer INVALID_FORMAT = 1102;

	/**
	 * Invalid type field (we cannot cast the value to its type).
	 */
	Integer INVALID_TYPE_FIELD = 1103;

	/**
	 * Invalid date field.
	 */
	Integer INVALID_DATE_FIELD = 1104;

	/**
	 * Invalid code field (the value isn't in the list of codes).
	 */
	Integer INVALID_CODE_FIELD = 1105;

	/**
	 * Invalid range field (the value is outside the range).
	 */
	Integer INVALID_RANGE_FIELD = 1106;

	/**
	 * String too long.
	 */
	Integer STRING_TOO_LONG = 1107;

	/**
	 * Undefined column.
	 */
	Integer UNDEFINED_COLUMN = 1108;

	/**
	 * No mapping.
	 */
	Integer NO_MAPPING = 1109;

	/**
	 * Trigger exception.
	 */
	Integer TRIGGER_EXCEPTION = 1110;

}
