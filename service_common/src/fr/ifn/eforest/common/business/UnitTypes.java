/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.common.business;

/**
 * List the authorised field types.
 */
public interface UnitTypes {

	/**
	 * A string.
	 */
	String STRING = "STRING";

	/**
	 * A code (mapped to java type String).
	 */
	String CODE = "CODE";

	/**
	 * A decimal value comprised in a specified range (mapped to java type BigDecimal).
	 */
	String RANGE = "RANGE";

	/**
	 * A numeric value (mapped to java type BigDecimal).
	 */
	String NUMERIC = "NUMERIC";

	/**
	 * An integer (mapped to java type Integer).
	 */
	String INTEGER = "INTEGER";

	/**
	 * A geographic coordinate (mapped to java type BigDecimal).
	 */
	String COORDINATE = "COORDINATE";

	/**
	 * A date (mapped to java type Date).
	 */
	String DATE = "DATE";

	/**
	 * A boolean (mapped to java type Boolean).
	 */
	String BOOLEAN = "BOOLEAN";

}
