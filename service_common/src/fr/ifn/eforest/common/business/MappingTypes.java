/*
* Copyright 2008-2012 European Union
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
*/

package fr.ifn.eforest.common.business;

/**
 * List the types of mapping.
 */
public interface MappingTypes {

	/**
	 * Map a field from the database to a form.
	 */
	String FORM_MAPPING = "FORM";

	/**
	 * Map a field from a file to the raw database.
	 */
	String FILE_MAPPING = "FILE";

	/**
	 * Map a field from the raw database to the harmonization database.
	 */
	String HARMONIZATION_MAPPING = "HARMONIZE";

	/**
	 * Map a field from the raw database (qualitative value, for exemple basal_area) to the related domain field (domain_basal_area).
	 */
	String DOMAIN_MAPPING = "DOMAIN";

}
